# FLOWDOC

Flowdoc is a universisty project java document manager graphical application.
The unorthodox use of git in this case (having binaries and static docs in the
repo) is caused by the project's constraints.
The normal (source-only) repo would have "FONTS" as root. DOCS and EXE contain
documentation and binaries respectively. Documentation for both the application
and source can be found on the appropiate directories and files.

Designed to work with Java 11, the only real limitation is that building is done with gradle (which at the time did not support Java 18 yet).

