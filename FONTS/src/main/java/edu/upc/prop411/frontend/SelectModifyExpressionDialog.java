package edu.upc.prop411.frontend;

/**
 * Dialog for selecting an expression in the modify expression use case
 *
 * @author Sergi Soler Arrufat
 */
public class SelectModifyExpressionDialog extends SelectExpressionDialog{

    public SelectModifyExpressionDialog(FrontendController frontendController) {
        super(frontendController);
    }

    @Override
    protected boolean executeCommand() {

        int selectedItem = comboBoxSelect.getSelectedIndex();
        if (selectedItem < 0) return false;

        InputExpressionModifyDialog modifyDialog = new InputExpressionModifyDialog(frontendController,
                comboBoxSelect.getItemAt(selectedItem).id, comboBoxSelect.getItemAt(selectedItem).string);

        modifyDialog.pack();
        modifyDialog.setLocationRelativeTo(getContentPane());
        modifyDialog.setVisible(true);

        return true;
    }


}
