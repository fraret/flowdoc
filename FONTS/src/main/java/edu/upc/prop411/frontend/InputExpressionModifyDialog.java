package edu.upc.prop411.frontend;

import java.util.UUID;

/**
 * Dialog for inputting the modified expression in the modify expression use case
 *
 * @author Sergi Soler Arrufat
 */
public class InputExpressionModifyDialog extends SingleTextDialog {
    private UUID id;
    public InputExpressionModifyDialog(FrontendController frontendController, UUID id, String initialText) {
        super(frontendController, "Input expression", "Expression");
        this.id = id;
        textField.setText(initialText);
    }

    @Override
    protected boolean executeCommand() {
        return frontendController.modifyExpr(id, textField.getText());
    }
}
