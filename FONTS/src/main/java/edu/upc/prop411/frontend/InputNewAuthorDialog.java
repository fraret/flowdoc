package edu.upc.prop411.frontend;

import java.util.List;
import java.util.UUID;

/**
 * Dialog for inputting the author in the modify author use case.
 *
 * @author Joan Farres Garcia
 */
public class InputNewAuthorDialog extends SingleTextDialog {
    private List<UUID> docs;
    public InputNewAuthorDialog(FrontendController frontendController, List<UUID> docs) {
        super(frontendController, "Input new author", "New author");
        this.docs = docs;

        if (docs.size() == 1)
            textField.setText(frontendController.getDocAuthor(docs.get(0)));
    }

    @Override
    protected boolean executeCommand() {
        frontendController.modifyAuthor(docs, textField.getText());

        return true;
    }
}