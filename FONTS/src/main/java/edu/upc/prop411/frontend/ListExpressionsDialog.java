package edu.upc.prop411.frontend;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Dialog for showing all the currently saved expressions.
 *
 * @author Joan Farres Garcia
 */
public class ListExpressionsDialog extends ListDialog {
    public ListExpressionsDialog(FrontendController frontendController) {
        super(frontendController, frontendController.listExpr().stream().map(e -> e.string).collect(Collectors.toList()), "Expression list");
    }
}
