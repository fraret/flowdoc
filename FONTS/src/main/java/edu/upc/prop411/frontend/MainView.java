package edu.upc.prop411.frontend;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import edu.upc.prop411.persistence.ExportFormat;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Main program window, containing the menu and the current search results
 */
public class MainView {
    /**
     * FrontendController instance
     */
    private final FrontendController frontendController;
    /**
     * List of the UUID's of the documents that are in the search
     */
    private List<UUID> searchIds;
    /**
     * JFrame instance
     */
    private JFrame frame;
    /**
     * MainView's menuBar
     */
    private JMenuBar menuBar;
    /**
     * Collection containing the JMenuItems that must be enabled only when exactly one item in the search is selected
     */
    private Collection<JMenuItem> requireOneSelectedMenuItems;
    /**
     * Collection containing the JMenuItems that must be enabled only when one or more items in the search are selected
     */
    private Collection<JMenuItem> requireOneOrManySelectedMenuItems;
    /**
     * Collection containing the JMenuItems that must be enabled only when there are one or more expressions registered
     */
    private Collection<JMenuItem> requireOneOrManyExpressions;

    /**
     * JPanel instance that is the frame's content pane
     */
    private JPanel contentPane;
    /**
     * Reset search button
     */
    private JButton resetSearchButton;
    /**
     * Sort criterion combobox
     */
    private JComboBox sortCriterionComboBox;
    /**
     * JList containing the search shown in the application
     */
    private JList search;
    /**
     * JScrollPane containing search
     */
    private JScrollPane searchScrollPane;

    /**
     * FileNameExtensionFilter instances for .txt, .json, and .xml
     */
    private final FileNameExtensionFilter txtFilter, JSONFilter, XMLFilter;


    /**
     * JFileChooser instances for the import and export operations
     */
    private final JFileChooser importChooser, exportChooser;

    /**
     * Class constructor
     *
     * @param frontendControllerInstance reference to the FrontendController instance
     */
    public MainView(FrontendController frontendControllerInstance) {
        frontendController = frontendControllerInstance;

        frame = new JFrame();
        createMenus();
        frame.setJMenuBar(menuBar);
        frame.setContentPane(contentPane);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frontendController.exit();
            }
        });
        frame.setTitle("Document Manager");

        frame.getRootPane().setGlassPane(new JComponent() {
            public void paintComponent(Graphics g) {
                g.setColor(new Color(0, 0, 0, 100));
                g.fillRect(0, 0, getWidth(), getHeight());
                super.paintComponent(g);
            }
        });

        searchScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        searchScrollPane.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                refreshSearch();
            }
        });

        sortCriterionComboBox.addActionListener(e -> changeOrder((String) ((JComboBox) e.getSource()).getSelectedItem()));

        resetSearchButton.addActionListener(e -> resetSearch());

        importChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "DL exported document", "txt", "xml", "json");

        txtFilter = new FileNameExtensionFilter(
                "Text file (txt)", "txt");
        JSONFilter = new FileNameExtensionFilter(
                "JSON File", "json");
        XMLFilter = new FileNameExtensionFilter(
                "XML File", "xml");
        importChooser.setAcceptAllFileFilterUsed(false);
        importChooser.setFileFilter(filter);
        importChooser.setMultiSelectionEnabled(true);
        importChooser.addChoosableFileFilter(txtFilter);
        importChooser.addChoosableFileFilter(JSONFilter);
        importChooser.addChoosableFileFilter(XMLFilter);
        importChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        exportChooser = new JFileChooser();

        exportChooser.addChoosableFileFilter(txtFilter);
        exportChooser.addChoosableFileFilter(JSONFilter);
        exportChooser.addChoosableFileFilter(XMLFilter);
        exportChooser.setAcceptAllFileFilterUsed(false);
        exportChooser.setMultiSelectionEnabled(false);
        exportChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }

    /**
     * Populates the menu bar with its menus and menu items. Also registers each menu items' listeners, and
     * restricts usage of some menu items to when certain amounts of documents are selected or to when expressions
     * are registered in the application
     */
    private void createMenus() {
        menuBar = new JMenuBar();
        requireOneSelectedMenuItems = new ArrayList<>();
        requireOneOrManySelectedMenuItems = new ArrayList<>();
        requireOneOrManyExpressions = new ArrayList<>();

        // File menu
        final JMenu fileMenu = new JMenu("File");

        final JMenuItem importMenuItem = new JMenuItem("Import Document(s)");
        importMenuItem.addActionListener(e -> importDocument());
        fileMenu.add(importMenuItem);

        final JMenuItem exportMenuItem = new JMenuItem("Export Document(s)");
        exportMenuItem.addActionListener(e -> exportDocument());
        requireOneOrManySelectedMenuItems.add(exportMenuItem);
        fileMenu.add(exportMenuItem);

        final JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(e -> exit());
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        // Edit menu
        final JMenu editMenu = new JMenu("Edit");

        final JMenuItem createDocumentMenuItem = new JMenuItem("Create Document");
        createDocumentMenuItem.addActionListener(e -> createDocument());
        editMenu.add(createDocumentMenuItem);

        final JMenuItem deleteDocumentMenuItem = new JMenuItem("Delete Document(s)");
        deleteDocumentMenuItem.addActionListener(e -> deleteDocument());
        requireOneOrManySelectedMenuItems.add(deleteDocumentMenuItem);
        editMenu.add(deleteDocumentMenuItem);

        final JMenuItem modifyTitleMenuItem = new JMenuItem("Modify Title");
        modifyTitleMenuItem.addActionListener(e -> modifyTitle());
        requireOneOrManySelectedMenuItems.add(modifyTitleMenuItem);
        editMenu.add(modifyTitleMenuItem);

        final JMenuItem modifyAuthorMenuItem = new JMenuItem("Modify Author");
        modifyAuthorMenuItem.addActionListener(e -> modifyAuthor());
        requireOneOrManySelectedMenuItems.add(modifyAuthorMenuItem);
        editMenu.add(modifyAuthorMenuItem);

        final JMenuItem modifyContentMenuItem = new JMenuItem("Modify Content");
        modifyContentMenuItem.addActionListener(e -> modifyContent());
        requireOneOrManySelectedMenuItems.add(modifyContentMenuItem);
        editMenu.add(modifyContentMenuItem);

        menuBar.add(editMenu);

        // Search menu
        final JMenu searchMenu = new JMenu("Search");

        final JMenuItem similarToDocumentMenuItem = new JMenuItem("Similar to Document");
        similarToDocumentMenuItem.addActionListener(e -> similarToDocument());
        requireOneSelectedMenuItems.add(similarToDocumentMenuItem);
        searchMenu.add(similarToDocumentMenuItem);

        final JMenuItem similarToWordsMenuItem = new JMenuItem("Similar to Words");
        similarToWordsMenuItem.addActionListener(e -> similarToWords());
        searchMenu.add(similarToWordsMenuItem);

        final JMenuItem searchByExpressionMenuItem = new JMenuItem("By Expression");
        searchByExpressionMenuItem.addActionListener(e -> searchByExpression());
        searchMenu.add(searchByExpressionMenuItem);

        menuBar.add(searchMenu);

        // View menu
        final JMenu viewMenu = new JMenu("View");

        final JMenuItem viewContentMenuItem = new JMenuItem("Content");
        viewContentMenuItem.addActionListener(e -> viewContent());
        requireOneOrManySelectedMenuItems.add(viewContentMenuItem);
        viewMenu.add(viewContentMenuItem);

        final JMenuItem viewFromAuthorTitleMenuItem = new JMenuItem("From Author and Title");
        viewFromAuthorTitleMenuItem.addActionListener(e -> fromAuthorTitle());
        viewMenu.add(viewFromAuthorTitleMenuItem);

        final JMenuItem viewTitlesFromAuthorMenuItem = new JMenuItem("Titles from Author");
        viewTitlesFromAuthorMenuItem.addActionListener(e -> titlesFromAuthor());
        viewMenu.add(viewTitlesFromAuthorMenuItem);

        final JMenuItem authorsFromPrefixMenuItem = new JMenuItem("Authors from prefix");
        authorsFromPrefixMenuItem.addActionListener(e -> authorsFromPrefix());
        viewMenu.add(authorsFromPrefixMenuItem);

        menuBar.add(viewMenu);

        // Expression menu
        final JMenu expressionMenu = new JMenu("Expression");

        final JMenuItem expressionAddMenuItem = new JMenuItem("Add");
        expressionAddMenuItem.addActionListener(e -> expressionAdd());
        expressionMenu.add(expressionAddMenuItem);

        final JMenuItem expressionListMenuItem = new JMenuItem("List");
        expressionListMenuItem.addActionListener(e -> expressionList());
        requireOneOrManyExpressions.add(expressionListMenuItem);
        expressionMenu.add(expressionListMenuItem);

        final JMenuItem expressionModifyMenuItem = new JMenuItem("Modify");
        expressionModifyMenuItem.addActionListener(e -> expressionModify());
        requireOneOrManyExpressions.add(expressionModifyMenuItem);
        expressionMenu.add(expressionModifyMenuItem);

        final JMenuItem expressionRemoveMenuItem = new JMenuItem("Remove");
        expressionRemoveMenuItem.addActionListener(e -> expressionRemove());
        requireOneOrManyExpressions.add(expressionRemoveMenuItem);
        expressionMenu.add(expressionRemoveMenuItem);

        menuBar.add(expressionMenu);

        // Change what menu items are available based on the number of entries selected
        search.addListSelectionListener(e -> {
            int numberSelected = ((JList) e.getSource()).getSelectedValuesList().size();
            for (JMenuItem menuItem : requireOneSelectedMenuItems) {
                menuItem.setEnabled((numberSelected == 1));
            }
            for (JMenuItem menuItem : requireOneOrManySelectedMenuItems) {
                menuItem.setEnabled((numberSelected >= 1));
            }
        });
        // Initially there are no entries selected
        for (JMenuItem menuItem : requireOneSelectedMenuItems) {
            menuItem.setEnabled(false);
        }
        for (JMenuItem menuItem : requireOneOrManySelectedMenuItems) {
            menuItem.setEnabled(false);
        }
    }

    /**
     * Makes the class visible after the necessary preparations
     */
    public void show() {
        frame.pack();
        frame.setSize(600, 480);
        frame.setLocationRelativeTo(null);
        refreshSearch();
        refreshExpressions();
        frame.setVisible(true);
    }

    /**
     * Dispatches a window closing event. Call when exiting the application through means other than closing the
     * OS window to ensure homogenous behaviour in all ways of exiting.
     */
    private void exit() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }

    /**
     * Updates the search to match the one in the Domain layer
     */
    private void refreshSearch() {
        searchIds = frontendController.getSearch();

        DefaultListModel defaultListModel = new DefaultListModel<>();
        Dimension rv = new Dimension();
        searchScrollPane.getViewport().getSize(rv);
        String width = Integer.toString(Math.max((rv.width - 5), 10));
        for (UUID entry : searchIds) {
            String title = frontendController.getDocTitle(entry);
            String author = frontendController.getDocAuthor(entry);
            String text = "<html>" +
                    "<div style=\"color:rgb(0,0,0); align=\"left\"; width=" + width + "\">" + title + "</div>" +
                    "<div style=\"color:rgb(105,105,105); align=\"right\"; width=" + width + "\">" + author + "</div>" +
                    "</html>";
            defaultListModel.addElement(text);
        }
        search.setModel(defaultListModel);
    }

    /**
     * Updates which menu items are available based on the number of registered expressions
     */
    private void refreshExpressions() {
        int numExpr = frontendController.listExpr().size();
        for (JMenuItem menuItem : requireOneOrManyExpressions) {
            menuItem.setEnabled((numExpr >= 1));
        }
    }

    /**
     * Gets the UUID's of the currently selected documents
     *
     * @return a list of the UUID's of the currently selected documents
     */
    private List<UUID> getSelectedDocumentsIds() {
        List<UUID> selectedDocumentsIds = new ArrayList<>();
        for (int index : search.getSelectedIndices()) {
            selectedDocumentsIds.add(searchIds.get(index));
        }
        return selectedDocumentsIds;
    }

    /**
     * Changes the sort criterion
     *
     * @param sortCriterionString string containing the sort criterion to change to
     */
    private void changeOrder(String sortCriterionString) {
        frontendController.changeOrder(sortCriterionString);
        refreshSearch();
    }

    /**
     * Resets the search to the default
     */
    private void resetSearch() {
        frontendController.resetSearch();
        refreshSearch();
    }

    /**
     * Opens a JFileChooser dialog to select the documents to import and imports them
     */
    private void importDocument() {
        frame.getRootPane().getGlassPane().setVisible(true);
        importChooser.showOpenDialog(frame);
        frame.getRootPane().getGlassPane().setVisible(false);
        frontendController.importDocs(Arrays.asList(importChooser.getSelectedFiles()));
        refreshSearch();
    }

    /**
     * Opens a JFileChooser dialog to select the export destination and exports the selected documents
     */
    private void exportDocument() {
        frame.getRootPane().getGlassPane().setVisible(true);
        exportChooser.showSaveDialog(frame);
        frame.getRootPane().getGlassPane().setVisible(false);
        ExportFormat exportFormat;
        if (exportChooser.getFileFilter() == JSONFilter) exportFormat = ExportFormat.JSON;
        else if (exportChooser.getFileFilter() == XMLFilter) exportFormat = ExportFormat.XML;
        else exportFormat = ExportFormat.TXT;
        File file = exportChooser.getSelectedFile();
        if (file != null)
            frontendController.exportDocs(getSelectedDocumentsIds(), file, exportFormat);
    }

    /**
     * Triggers the create document use case
     */
    private void createDocument() {
        frame.getRootPane().getGlassPane().setVisible(true);
        InputAuthorTitleContentDialog inputAuthorTitleContentDialog = new InputAuthorTitleContentDialog(frontendController);
        inputAuthorTitleContentDialog.pack();
        inputAuthorTitleContentDialog.setLocationRelativeTo(frame);
        inputAuthorTitleContentDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
        refreshSearch();
    }

    /**
     * Triggers the delete document use case
     */
    private void deleteDocument() {
        List<UUID> selectedDocumentsIds = getSelectedDocumentsIds();
        frontendController.delDocument(selectedDocumentsIds);
        refreshSearch();
    }

    /**
     * Triggers the modify title use case
     */
    private void modifyTitle() {
        List<UUID> selectedDocumentsIds = getSelectedDocumentsIds();
        frame.getRootPane().getGlassPane().setVisible(true);
        InputNewTitleDialog inputNewTitleDialog = new InputNewTitleDialog(frontendController, selectedDocumentsIds);
        inputNewTitleDialog.pack();
        inputNewTitleDialog.setLocationRelativeTo(frame);
        inputNewTitleDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
        refreshSearch();
    }

    /**
     * Triggers the modify author use case
     */
    private void modifyAuthor() {
        List<UUID> selectedDocumentsIds = getSelectedDocumentsIds();
        frame.getRootPane().getGlassPane().setVisible(true);
        InputNewAuthorDialog inputNewAuthorDialog = new InputNewAuthorDialog(frontendController, selectedDocumentsIds);
        inputNewAuthorDialog.pack();
        inputNewAuthorDialog.setLocationRelativeTo(frame);
        inputNewAuthorDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
        refreshSearch();
    }

    /**
     * Triggers the modify content use case
     */
    private void modifyContent() {
        List<UUID> selectedDocumentsIds = getSelectedDocumentsIds();
        frame.getRootPane().getGlassPane().setVisible(true);
        InputNewContentDialog inputNewContentDialog = new InputNewContentDialog(frontendController, selectedDocumentsIds);
        inputNewContentDialog.pack();
        inputNewContentDialog.setLocationRelativeTo(frame);
        inputNewContentDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
        refreshSearch();
    }

    /**
     * Triggers the search similar to document use case.
     * Searches the documents that are the most similar to the selected (exactly one) document
     */
    private void similarToDocument() {
        List<UUID> selectedDocumentsIds = getSelectedDocumentsIds(); // There is exactly one id

        // Get number of documents to list
        int n;
        do {
            String input = JOptionPane.showInputDialog($$$getRootComponent$$$(), "Enter positive integer: ");
            // If user cancels
            if (input == null) {
                return;
            }
            try {
                n = Integer.parseInt(input);
                if (n > 0) {
                    break;
                }
            } catch (NumberFormatException e) {
            }
            JOptionPane.showMessageDialog($$$getRootComponent$$$(), "Please enter a positive integer", "Warning", JOptionPane.WARNING_MESSAGE);
        } while (true);

        frontendController.searchByDoc(selectedDocumentsIds.get(0), n);
        refreshSearch();
    }

    /**
     * Triggers the search similar to word list use case.
     * Searches the documents that are the most similar to the supplied word list
     */
    private void similarToWords() {
        frame.getRootPane().getGlassPane().setVisible(true);
        InputWordListDialog inputWordListDialog = new InputWordListDialog(frontendController);
        inputWordListDialog.pack();
        inputWordListDialog.setLocationRelativeTo(frame);
        inputWordListDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
        refreshSearch();
    }

    /**
     * Triggers the search by expression use case.
     * Searches the documents that match the given expression
     */
    private void searchByExpression() {
        frame.getRootPane().getGlassPane().setVisible(true);
        SelectInputExpressionDialog selectInputExpressionDialog = new SelectInputExpressionDialog(frontendController);
        selectInputExpressionDialog.pack();
        selectInputExpressionDialog.setLocationRelativeTo(frame);
        selectInputExpressionDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
        refreshSearch();
    }

    /**
     * Triggers the view content use case.
     */
    private void viewContent() {
        List<UUID> selectedDocumentsIds = getSelectedDocumentsIds();
        for (UUID id : selectedDocumentsIds) {
            ViewContentDialog viewContentDialog = new ViewContentDialog(frontendController, frontendController.getDocTitle(id), frontendController.getDocContent(id));
            viewContentDialog.pack();
            viewContentDialog.setLocationRelativeTo(frame);
            viewContentDialog.setVisible(true);
        }
    }

    /**
     * Triggers the view from author title use case.
     * Shows the content of the document with the supplied author and title
     */
    private void fromAuthorTitle() {
        frame.getRootPane().getGlassPane().setVisible(true);
        InputAuthorTitleDialog inputAuthorTitleDialog = new InputAuthorTitleDialog(frontendController);
        inputAuthorTitleDialog.pack();
        inputAuthorTitleDialog.setLocationRelativeTo(frame);
        inputAuthorTitleDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
    }

    /**
     * Triggers the view titles from author use case.
     * Shows the titles belonging to the supplied author
     */
    private void titlesFromAuthor() {
        frame.getRootPane().getGlassPane().setVisible(true);
        InputAuthorNameDialog inputAuthorNameDialog = new InputAuthorNameDialog(frontendController);
        inputAuthorNameDialog.pack();
        inputAuthorNameDialog.setLocationRelativeTo(frame);
        inputAuthorNameDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
    }

    /**
     * Triggers the view author from prefix use case.
     * Shows the authors that match the given prefix
     */
    private void authorsFromPrefix() {
        frame.getRootPane().getGlassPane().setVisible(true);
        InputAuthorPrefixDialog inputAuthorPrefixDialog = new InputAuthorPrefixDialog(frontendController);
        inputAuthorPrefixDialog.pack();
        inputAuthorPrefixDialog.setLocationRelativeTo(frame);
        inputAuthorPrefixDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
    }

    /**
     * Triggers the add expression use case.
     */
    private void expressionAdd() {
        frame.getRootPane().getGlassPane().setVisible(true);
        InputExpressionDialog inputExpressionDialog = new InputExpressionDialog(frontendController);
        inputExpressionDialog.pack();
        inputExpressionDialog.setLocationRelativeTo(frame);
        inputExpressionDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
        refreshExpressions();
    }

    /**
     * Triggers the list expressions use case.
     * Lists all current expressions
     */
    private void expressionList() {
        frame.getRootPane().getGlassPane().setVisible(true);
        ListExpressionsDialog listExpressionsDialog = new ListExpressionsDialog(frontendController);
        listExpressionsDialog.pack();
        listExpressionsDialog.setLocationRelativeTo(frame);
        listExpressionsDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
    }

    /**
     * Triggers the modify expression use case.
     */
    private void expressionModify() {
        frame.getRootPane().getGlassPane().setVisible(true);
        SelectModifyExpressionDialog selectModifyExpressionDialog = new SelectModifyExpressionDialog(frontendController);
        selectModifyExpressionDialog.pack();
        selectModifyExpressionDialog.setLocationRelativeTo(frame);
        selectModifyExpressionDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
    }

    /**
     * Triggers the remove expression use case.
     */
    private void expressionRemove() {
        frame.getRootPane().getGlassPane().setVisible(true);
        SelectDeleteExpressionDialog selectDeleteExpressionDialog = new SelectDeleteExpressionDialog(frontendController);
        selectDeleteExpressionDialog.pack();
        selectDeleteExpressionDialog.setLocationRelativeTo(frame);
        selectDeleteExpressionDialog.setVisible(true);
        frame.getRootPane().getGlassPane().setVisible(false);
        refreshExpressions();
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        resetSearchButton = new JButton();
        resetSearchButton.setText("Reset Search");
        panel1.add(resetSearchButton, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        sortCriterionComboBox = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        defaultComboBoxModel1.addElement("TitleASC");
        defaultComboBoxModel1.addElement("TitleDSC");
        defaultComboBoxModel1.addElement("AuthorASC");
        defaultComboBoxModel1.addElement("AuthorDSC");
        defaultComboBoxModel1.addElement("LengthASC");
        defaultComboBoxModel1.addElement("LengthDSC");
        sortCriterionComboBox.setModel(defaultComboBoxModel1);
        panel1.add(sortCriterionComboBox, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        searchScrollPane = new JScrollPane();
        contentPane.add(searchScrollPane, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        search = new JList();
        final DefaultListModel defaultListModel1 = new DefaultListModel();
        search.setModel(defaultListModel1);
        searchScrollPane.setViewportView(search);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }

}
