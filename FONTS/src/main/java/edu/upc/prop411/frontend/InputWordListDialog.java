package edu.upc.prop411.frontend;

import javax.swing.*;

/**
 * Dialog for inputting the word list in the "list documents similar to word list" use case.
 *
 * @author Joan Farres Garcia
 */
public class InputWordListDialog extends SingleTextDialog {
    public InputWordListDialog(FrontendController frontendController) {
        super(frontendController, "Input word list", "Word list");
    }

    @Override
    protected boolean executeCommand() {
        // Get number of documents to list
        int n;
        do {
            String input = JOptionPane.showInputDialog($$$getRootComponent$$$(), "Enter positive integer: ");
            // If user cancels
            if (input == null) {
                return false;
            }
            try {
                n = Integer.parseInt(input);
                if (n > 0) {
                    break;
                }
            } catch (NumberFormatException e) {
            }
            JOptionPane.showMessageDialog($$$getRootComponent$$$(), "Please enter a positive integer", "Warning", JOptionPane.WARNING_MESSAGE);
        } while (true);

        return frontendController.searchByWordList(textField.getText(), n);
    }
}
