package edu.upc.prop411.frontend;

/**
 * CLI bad range exception
 */
public class RangeException extends FrontendException {

    public RangeException(int value, int size) {
        super(String.format("Document index out of bounds error: in value %d out of [1, %d]", value, size));
    }
}
