package edu.upc.prop411.frontend;

import javax.swing.*;

/**
 * Application entry point
 */
public class Main {
    public static void usage() {
        System.out.println("usage:");
        System.out.println(" -C,--cli   open with command line interface");
    }

    /**
     * Quick and dirty method to try to set the look and field for Windows and Linux
     */
    static private void setLookAndFeel() {
        try {
            String feelName = UIManager.getSystemLookAndFeelClassName();
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            if (feelName.equals("javax.swing.plaf.metal.MetalLookAndFeel")) {
                try {
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                    return;
                } catch (Exception ignored) {};
                try {
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
                    return;
                } catch (Exception ignored) {};
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            javax.swing.SwingUtilities.invokeLater(
                    new Runnable() {
                        @Override
                        public void run() {

                            setLookAndFeel();

                            JFrame.setDefaultLookAndFeelDecorated(true);

                            FrontendController frontendController = new FrontendController();
                            frontendController.run();
                        }
                    }
            );
        } else if (args.length == 1 && (args[0].equals("-C") || args[0].equals("--cli"))) {
            CLI cli = new CLI();
            cli.mainLoop();
        } else {
            usage();
        }
    }
}