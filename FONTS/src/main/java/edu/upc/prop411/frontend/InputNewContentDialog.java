package edu.upc.prop411.frontend;

import java.util.List;
import java.util.UUID;

/**
 * Document content modification dialog.
 *
 * @author Joan Farres Garcia
 */
public class InputNewContentDialog extends LargeContentDialog {
    public InputNewContentDialog(FrontendController frontendController, List<UUID> docs) {
        super(frontendController, "Input new content", "", false);
        this.docs = docs;

        if (docs.size() == 1)
            textArea.setText(frontendController.getDocContent(docs.get(0)));
    }

    @Override
    protected void executeCommand() {
        frontendController.modifyContent(docs, textArea.getText());
    }
}
