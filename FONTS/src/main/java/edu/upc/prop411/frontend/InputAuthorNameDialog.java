package edu.upc.prop411.frontend;

import java.util.List;

/**
 * Input author dialog used in the show author documents use case.
 *
 * @author Joan Farres Garcia
 */
public class InputAuthorNameDialog extends SingleTextDialog {
    public InputAuthorNameDialog(FrontendController frontendController) {
        super(frontendController, "Input author name", "Author name");
    }

    @Override
    protected boolean executeCommand() {
        List<String> titles = frontendController.getAuthorTitles(textField.getText());

        if (titles == null)
            return false;

        ListDialog dialog = new ListDialog(frontendController, titles, "Titles");
        dialog.pack();
        dialog.setLocationRelativeTo(getContentPane());
        dialog.setVisible(true);

        return true;
    }
}
