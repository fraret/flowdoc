package edu.upc.prop411.frontend;

import edu.upc.prop411.domain.DomainController;
import edu.upc.prop411.domain.SortCriterion;
import edu.upc.prop411.domain.exceptions.critical.InvalidStateException;
import edu.upc.prop411.domain.exceptions.recoverable.DomainException;
import edu.upc.prop411.persistence.ExportFormat;

import javax.swing.*;
import java.io.File;
import java.util.*;

/**
 * The FrontendController is the class that connects the interface layer to the domain
 * layer.
 * <p>
 * It also acts as a controller for the other classes in the interface layer, as action
 * listeners in the views and dialogs call this class' methods
 * @author Sergi Soler Arrufat
 */
public class FrontendController {
    /**
     * DomainController instance
     */
    private DomainController domainController;
    /**
     * MainView instance
     */
    private final MainView mainView;

    /*
    enum LoggingLevel {RELEASE, DEBUG};

    LoggingLevel logLevel

            */

    /**
     * Class constructor
     */
    public FrontendController() {
        try {
            domainController = new DomainController();
        } catch (InvalidStateException e) {
            JOptionPane.showMessageDialog(null, "Application state files are corrupted or partially missing. Restore them to a backup or delete them so a new state is generated", "Critical Error", JOptionPane.ERROR_MESSAGE);
            System.err.println("Application state files are corrupted or partially missing. Restore them to a backup or delete them so a new state is generated");
            System.exit(1);
        }
        mainView = new MainView(this);
    }

    /**
     * Start running the application. Currently only makes the mainView visible
     */
    public void run() {
        mainView.show();
    }

    /**
     *
     */
    void changeOrder(String sortCriterionString) {
        SortCriterion sortCriterion;

        switch (sortCriterionString) {
            case "TitleASC":
                sortCriterion = SortCriterion.TitleASC;
                break;
            case "TitleDSC":
                sortCriterion = SortCriterion.TitleDSC;
                break;
            case "AuthorASC":
                sortCriterion = SortCriterion.AuthorASC;
                break;
            case "AuthorDSC":
                sortCriterion = SortCriterion.AuthorDSC;
                break;
            case "LengthASC":
                sortCriterion = SortCriterion.LengthASC;
                break;
            case "LengthDSC":
                sortCriterion = SortCriterion.LengthDSC;
                break;
            default:
                sortCriterion = SortCriterion.TitleASC;
        }

        domainController.changeOrder(sortCriterion);
    }

    /**
     * Creates a new expression
     * @param texpr The text for the new expression
     * @return true if the expression could be created, false otherwise
     */
    boolean addExpr(String texpr) {
        try {
            domainController.createExpr(texpr);
            return true;
        } catch (DomainException e) {
            showDialog("Could not add the expression:", e.getMessage(), MessageType.WARNING);
        }
        return false;
    }

    /**
     * Returns a map containing the expression IDs and their respective texts
     * @return a map from expression IDs to their text
     */
    List<ExpressionUUIDandText> listExpr() {
        ArrayList<ExpressionUUIDandText> expressions = new ArrayList<>();
        for(UUID id : domainController.getExpressions()) {
            expressions.add(new ExpressionUUIDandText(id, domainController.getExprText(id)));
        }
        return expressions;
    }

    /**
     * Modifies a saved expression
     * @param id of the saved expression to modify
     * @param texpr new text for the expression
     * @return true if the expression was modified successfully, false otherwise
     */
    boolean modifyExpr(UUID id, String texpr) {
        try {
            domainController.replaceExpr(texpr, id);
            return true;
        } catch (DomainException e) {
            showDialog("Could not modify the expression:", e.getMessage(), MessageType.WARNING);
        }
        return false;
    }

    /**
     * Removes an expresssion from the internal expresssion library
     * @param id of the expression to delete
     */
    void delExpr(UUID id) {
        domainController.delExpr(id);
    }

    /**
     * Retruns the content of a document identified using an author and a title
     * @param author The author of the document
     * @param title The title of the document
     * @return The content of the document or null if such document does not exist
     */
    String getContentAuthorTitle(String author, String title) {
        UUID id;
        String ans;
        try {
            id = domainController.getDocWithAuthorTitle(author, title);
        } catch (DomainException e) {
            showDialog("Could not find the given author", MessageType.WARNING);
            return null;
        }
        if (id == null) {
            showDialog("The given author does not have a document with the given title", MessageType.WARNING);
            return null;
        }

        ans = domainController.getDocContent(id);
        return ans;
    }

    /**
     * Returns the list of titles for an author, sorted alphabetically
     * @param author the name of the author
     * @return the list if the author exists, null otherwise
     */
    List<String> getAuthorTitles(String author) {
        List<String> ans = new ArrayList<>();
        Collection<UUID> authorDocs;
        try {
            authorDocs = domainController.getAuthorDocs(author);
        } catch (DomainException e) {
            showDialog("Could not find the author", MessageType.WARNING);
            return null;
        }
        for (UUID id : authorDocs) {
            ans.add(domainController.getDocTitle(id));
        }
        Collections.sort(ans);
        return ans;
    }

    /**
     * Returns the list of authors that start with a given prefix, sorted alphabetically
     * @param prefix the prefix
     * @return The list of authors that start with the prefix, will be empty if none do
     */
    List<String> getAuthors(String prefix) {
        List<String> ans = new ArrayList<>(domainController.getAuthors(prefix));
        Collections.sort(ans);
        return ans;
    }

    /**
     * Searches by similarity
     * @param id The document from which to compute similarity
     * @param n the maximum number of results
     */
    void searchByDoc(UUID id, int n) {
        domainController.searchBySimilarDoc(id, n);
        int found = domainController.getSearchSize();
        if (found != n)
            showDialog("Search completed, found " + found + " documents", MessageType.INFO);
    }

    /**
     * Searches by word list
     * @param wordlist a string containing the word list
     * @param n the maximum number of results
     * @return true if the search could be performed, false otehrwise
     */
    boolean searchByWordList(String wordlist, int n) {
        try {
            domainController.searchByWordList(wordlist, n);
            int found = domainController.getSearchSize();
            if (found != n)
                showDialog("Search completed, found " + found + " documents", MessageType.INFO);
            return true;
        } catch (DomainException e) {
            showDialog("Word list is not valid:", e.getMessage(), MessageType.WARNING);
        }
        return false;
    }

    /**
     * Searches by a given saved expression ID
     * @param id the saved expression ID
     */
    void searchByExpr(UUID id) {
        try {
            domainController.searchByExpr(id);
        } catch (DomainException e) {
            showDialog("Internal error during search:", e.getMessage(), MessageType.ERROR);
        }
        int found = domainController.getSearchSize();
        if (found == 0)
            showDialog("Search completed but no documents have been found", MessageType.INFO);
    }

    /**
     * Searches by a volatile (not saved) expression
     * @param texpr the expression text
     * @return true if the search could be performed, false otherwise
     */
    boolean searchByExpr(String texpr) {
        try {
            domainController.searchByExpr(texpr);
            int found = domainController.getSearchSize();
            if (found == 0)
                showDialog("Search completed but no documents have been found", MessageType.INFO);
            return true;
        } catch (DomainException e) {
            showDialog("Invalid expression in search: ", e.getMessage(), MessageType.WARNING);
        }
        return false;
    }

    /**
     * Resets search to the default search.
     */
    void resetSearch() {
        domainController.resetSearch();
    }


    /**
     * Deletes the given documents from the library
     * @param ids list of ids of documents to delete
     */
    void delDocument(List<UUID> ids) {
        for (UUID id : ids) {
            domainController.delDocument(id);
        }
        if (ids.size() > 1)
            showDialog("Successfully deleted " + ids.size() + " documents.", MessageType.INFO);
    }

    /**
     * Sets the title of a given set of documents to the one provided. If the operation cannot be done on a document,
     * it will abort having modified all documents prior to that one and not having modified any documents from the
     * one that failed. It will give a comprehensive error message in that case.
     * @param ids list of ids of the documents to modify
     * @param title the new title for the modified documents
     * @return number of documents successfully modified
     */
    int modifyTitle(List<UUID> ids, String title) {
        int succ = 0;
        for (UUID id : ids) {
            try {
                domainController.modifyDocTitle(id, title);
                succ++;
            } catch (DomainException e) {
                if (ids.size() == 1)
                    showDialog("Could not modify the title:", e.getMessage(), MessageType.WARNING);
                else
                    showDialog("Could not modify the title of a document. " + succ + " documents have had their title modified"
                            + " before a document failed due to:", e.getMessage(), MessageType.WARNING);
                return succ;
            }
        }
        if (succ > 1)
            showDialog("Successfully modified the title of " + succ + " documents.", MessageType.INFO);
        return succ;
    }

    /**
     * Sets the author of a given set of documents to the one provided. If the operation cannot be done on a document,
     * it will abort having modified all documents prior to that one and not having modified any documents from the
     * one that failed. It will give a comprehensive error message in that case.
     * @param ids list of ids of the documents to
     * @param author the new author for the modified couments
     * @return number of documents successfully modified
     */
    int modifyAuthor(List<UUID> ids, String author) {
        int succ = 0;
        for (UUID id : ids) {
            try {
                domainController.modifyDocAuthor(id, author);
                succ++;
            } catch (DomainException e) {
                if (ids.size() == 1)
                    showDialog("Could not modify the author:", e.getMessage(), MessageType.WARNING);
                else
                    showDialog("Could not modify the author of a document. " + succ + " documents have had their author modified"
                            + " before a document failed due to:", e.getMessage(), MessageType.WARNING);
                return succ;
            }
        }
        if (succ > 1)
            showDialog("Successfully modified the author of " + succ + " documents.", MessageType.INFO);
        return succ;
    }

    /**
     * Sets the content of a given set of documents to the one provided. If the operation cannot be done on a document,
     * it will abort having modified all documents prior to that one and not having modified any documents from the
     * one that failed. It will give a comprehensive error message in that case.
     * @param ids list of ids of the documents to modify
     * @param content the new content for the modified documetns
     * @return number of documents successfully modified
     */
    int modifyContent(List<UUID> ids, String content) {
        int succ = 0;
        for (UUID id : ids) {
            try {
                domainController.modifyDocContent(id, content);
                succ++;
            } catch (DomainException e) {
                if (ids.size() == 1)
                    showDialog("Could not modify the content:", e.getMessage(), MessageType.WARNING);
                else
                    showDialog("Could not modify the content of a document. " + succ + " documents have had their content modified"
                            + " before a document failed due to:", e.getMessage(), MessageType.WARNING);
                return succ;
            }
        }
        if (succ > 1)
            showDialog("Successfully modified the content of " + succ + " documents.", MessageType.INFO);
        return succ;
    }


    /**
     * Returns the ordered list of UUIDs for the current search
     */
    List<UUID> getSearch() {
        return domainController.getSearch();
    }

    /**
     * Returns the title of a given document
     * @param doc The id of the document, which must refer to an existing document
     * @return the title of the document identified by that ID
     */
    String getDocTitle(UUID doc) {
        return domainController.getDocTitle(doc);
    }

    /**
     * Returns the author of a given document
     * @param doc The id of the document, which must refer to an existing document
     * @return the author of the document identified by that ID
     */
    String getDocAuthor(UUID doc) {
        return domainController.getDocAuthor(doc);
    }

    /**
     * Returns the content of a given document
     * @param doc The id of the document, which must refer to an existing document
     * @return the content of the document identified by that ID
     */
    String getDocContent(UUID doc) {
        return domainController.getDocContent(doc);
    }

    /**
     * Creates a document with the given parameters
     * @param title The title of the document to be created
     * @param author The author of the document to be created
     * @param content The content of the document to be created
     * @return true if the document could be created, false otherwise
     */
    boolean createDocument(String title, String author, String content) {
        try {
            domainController.createDocument(title, author, content);
            return true;
        } catch (DomainException e) {
            showDialog("Could not create the document:", e.getMessage(), MessageType.WARNING);
        }
        return false;
    }

    /**
     * Saves the current application state and exits
     */
    void exit() {
        domainController.saveState();
    }


    /**
     *
     * @param filesList list of files (can be directories) to import
     * @return -1 if some or all files failed to import, otherwise the number of documents imported
     */
    int importDocs(List<File> filesList) {
        int succ;
        try {
            succ = domainController.importDocuments(filesList);
        } catch (DomainException e) {
            if (filesList.size() == 1)
                showDialog("Could not import the document: ", e.getMessage(), MessageType.WARNING);
            else
                showDialog("Could not import all the documents due to:", e.getMessage(), MessageType.WARNING);
            return -1;
        }
        if (succ > 1)
            showDialog("Successfully imported " + succ + " documents.", MessageType.INFO);
        return succ;
    }

    int exportDocs(List<UUID> ids, File path, ExportFormat format) {
        int succ;
        try {
            succ = domainController.exportDocuments(ids, path, format);
        } catch (DomainException e) {
            if (ids.size() == 1)
                showDialog("Could not export the document: ", e.getMessage(), MessageType.WARNING);
            else
                showDialog("Could not export all the documents due to:", e.getMessage(), MessageType.WARNING);
            return -1;
        }
        if (succ > 1)
            showDialog("Successfully exported " + succ + " documents.", MessageType.INFO);
        return succ;
    }

    /**
     * Internal enum for selecting the type of exception/info dialog to show
     */
    private enum MessageType {INFO, WARNING, ERROR}

    protected void showDialog(String message, MessageType msgType) {
        showDialog(message, "", msgType);
    }

    /**
     * Shows a dialog with the given message (and possibly submessage). Use for exception notification or information
     * messages
     * @param message The main message
     * @param submessage a submessage to also be displayed, usually the exception text
     * @param msgType the type of message/dialog to display
     */
    protected void showDialog(String message, String submessage, MessageType msgType) {
        String title;
        int internalMsgType;
        switch (msgType) {
            case INFO:
                title = "Info";
                internalMsgType = JOptionPane.INFORMATION_MESSAGE;
                break;
            case WARNING:
                title = "Warning";
                internalMsgType = JOptionPane.WARNING_MESSAGE;
                break;
            default: //case ERROR:
                title = "Error";
                internalMsgType = JOptionPane.ERROR_MESSAGE;
                break;
        }
        JOptionPane.showMessageDialog(mainView.$$$getRootComponent$$$(), message + "\n" + submessage, title, internalMsgType);
    }



}
