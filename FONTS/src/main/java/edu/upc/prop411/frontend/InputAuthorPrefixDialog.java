package edu.upc.prop411.frontend;

import java.util.List;

/**
 * Dialog for inputting an author prefix in the get authors use case.
 *
 * @author Joan Farres Garcia
 */
public class InputAuthorPrefixDialog extends SingleTextDialog {
    public InputAuthorPrefixDialog(FrontendController frontendController) {
        super(frontendController, "Input author prefix", "Author prefix");
    }

    @Override
    protected boolean executeCommand() {
        List<String> authors = frontendController.getAuthors(textField.getText());

        ListDialog dialog = new ListDialog(frontendController, authors, "Authors");
        dialog.pack();
        dialog.setLocationRelativeTo(getContentPane());
        dialog.setVisible(true);

        return true;
    }
}
