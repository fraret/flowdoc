package edu.upc.prop411.frontend;

/**
 * Frontend layer exception
 */
public class FrontendException extends Exception {
    public FrontendException() {super();}

    public FrontendException(String str) {
        super(str);
    }
}
