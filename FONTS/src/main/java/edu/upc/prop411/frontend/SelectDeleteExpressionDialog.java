package edu.upc.prop411.frontend;

/**
 * Dialog for selecting a saved expression to be deleted
 *
 * @author Sergi Soler Arrufat
 */
public class SelectDeleteExpressionDialog extends SelectExpressionDialog{

    public SelectDeleteExpressionDialog(FrontendController frontendController) {
        super(frontendController);
    }

    @Override
    protected boolean executeCommand() {

        int selectedItem = comboBoxSelect.getSelectedIndex();
        if (selectedItem < 0) return false;

        frontendController.delExpr(comboBoxSelect.getItemAt(selectedItem).id);
        return true;
    }


}

