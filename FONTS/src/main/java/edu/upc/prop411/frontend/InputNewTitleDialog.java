package edu.upc.prop411.frontend;

import java.util.List;
import java.util.UUID;

/**
 * Dialog for inputting the title in the modify title use case.
 *
 * @author Joan Farres Garcia
 */
public class InputNewTitleDialog extends SingleTextDialog {
    private List<UUID> docs;
    public InputNewTitleDialog(FrontendController frontendController, List<UUID> docs) {
        super(frontendController, "Input new title", "New title");
        this.docs = docs;

        if (docs.size() == 1)
            textField.setText(frontendController.getDocTitle(docs.get(0)));
    }

    @Override
    protected boolean executeCommand() {
        frontendController.modifyTitle(docs, textField.getText());

        return true;
    }
}
