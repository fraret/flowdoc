package edu.upc.prop411.frontend;

/**
 * Dialog for adding a new expression
 *
 * @author Sergi Soler Arrufat
 */
public class InputExpressionDialog extends SingleTextDialog {
    public InputExpressionDialog(FrontendController frontendController) {
        super(frontendController, "Input expression", "Expression");
    }

    @Override
    protected boolean executeCommand() {
        return frontendController.addExpr(textField.getText());
    }
}
