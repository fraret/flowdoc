package edu.upc.prop411.frontend;

/**
 * View document content dialog.
 *
 * @author Joan Farres Garcia
 */
public class ViewContentDialog extends LargeContentDialog {
    public ViewContentDialog(FrontendController frontendController, String title, String content) {
        super(frontendController, title, content, true);
    }
    public ViewContentDialog(FrontendController frontendController, String content) {
        super(frontendController, "View content", content, true);
    }

    @Override
    protected void executeCommand() {}
}