package edu.upc.prop411.frontend;

import edu.upc.prop411.domain.DomainController;
import edu.upc.prop411.domain.exceptions.recoverable.DomainException;
import edu.upc.prop411.domain.SortCriterion;
import edu.upc.prop411.persistence.ExportFormat;

import java.io.File;
import java.util.*;

/**
 * CLI interface for the application.
 *
 * Initially and mostly used as a test driver, it is also
 *
 * @author Joan Farres Garcia
 */
public class CLI {
    /**
     * The domain controller.
     */
    private final DomainController domainController;

    /**
     * The scanner.
     */
    private final Scanner scanner;

    /**
     * Class Constructor.
     */
    public CLI() {
        domainController = new DomainController();
        scanner = new Scanner(System.in);
    }


    /////////////////////
    // Usage functions //
    /////////////////////

    private void printHelp() {
        System.out.println("list of commands:");
        System.out.println("    import-documents       (i) <path>");
        System.out.println("    export-documents       (e) <selection> <path>");

        System.out.println("    create-document        (c)");
        System.out.println("    delete-documents       (d) <selection>");
        System.out.println("    modify-title           (mt) <selection>");
        System.out.println("    modify-author          (ma) <selection>");
        System.out.println("    modify-content         (mc) <selection>");
        System.out.println("    view-content           (vc) <selection>");
        System.out.println("    view-from-author-title (vcat)");

        System.out.println("    show-last-search       (s)");
        System.out.println("    reset-search           (rst)");
        System.out.println("    list-authors           (la) <prefix>");
        System.out.println("    list-author-docs       (lad) <author>");
        System.out.println("    list-similar-to-docs   (lsd) <doc>");
        System.out.println("    list-similar-to-words  (lsw) <word1> <word2> ...");

        System.out.println("    modify-order-criterion (moc) <new criterion>");

        System.out.println("    create-expression      (ce) <expression>");
        System.out.println("    modify-expression      (me)");
        System.out.println("    delete-expression      (de)");
        System.out.println("    list-expressions       (le)");
        System.out.println("    list-docs-expression   (lde) <expression>");
        System.out.println("    list-docs-expression   (lde)");

        System.out.println("    clear                  (clr)");
        System.out.println("    quit-save              (qs)");
        System.out.println("    quit                   (q)");
        System.out.println("    help                   (h)");
    }

    private void helpImportDocuments() {
        System.out.println("USAGE:   import-documents (i) <path>");
        System.out.println("Example: import-documents documents");
    }

    private void helpExportDocuments() {
        System.out.println("USAGE:   export-documents (e) <selection> <path>");
        System.out.println("Example: export-documents 1,4-8,10 exportedDocuments");
    }

    private void helpCreateDocument() {
        System.out.println("USAGE:   create-document (c)");
        System.out.println("Example: create-document");
    }

    private void helpDeleteDocuments() {
        System.out.println("USAGE:   delete-documents (d) <selection>");
        System.out.println("Example: delete-documents 1,4-8,10");
    }

    private void helpModifyTitle() {
        System.out.println("USAGE:   modify-title (mt) <selection>");
        System.out.println("Example: modify-title 2,4");
    }

    private void helpModifyAuthor() {
        System.out.println("USAGE:   modify-author (ma) <selection>");
        System.out.println("Example: modify-author 2,4");
    }

    private void helpModifyContent() {
        System.out.println("USAGE:   modify-content (mc) <selection>");
        System.out.println("Example: modify-content 2,4");
    }

     private void helpViewContent() {
         System.out.println("USAGE:   view-content (vc) <selection>");
         System.out.println("Example: view-content 2,4");
     }

    private void helpViewFromAuthorTitle() {
        System.out.println("USAGE:   view-from-author-title (vcat) <selection>");
        System.out.println("Example: view-from-author-title 2,4");
    }

    private void helpListSearch() {
        System.out.println("USAGE:   show-last-search (s)");
        System.out.println("Example: show-last-search");
    }

    private void helpResetSearch() {
        System.out.println("USAGE:   reset-search (rst)");
        System.out.println("Example: reset-search");
    }

    private void helpListAuthors() {
        System.out.println("USAGE:   list-authors (la) <prefix>");
        System.out.println("Example: list-authors Miguel de");
        System.out.println("Note:    prefix can be empty");
    }

    private void helpListAuthorDocs() {
        System.out.println("USAGE:   list-author-docs (lad) <author>");
        System.out.println("Example: list-author-docs Miguel de Cervantes");
        System.out.println("Note:    author may not exist");
    }

    private void helpListSimilarToDocs() {
        System.out.println("USAGE:   list-similar-to-docs (lad) <doc>");
        System.out.println("Example: list-similar-to-docs 1");
        System.out.println("Note:    author may not exist");
    }

    private void helpListSimilarToWords() {
        System.out.println("USAGE:   list-similar-to-words (lsw) <word1> <word2>");
        System.out.println("Example: list-similar-to-words aventura espada");
        System.out.println("Note:    word list should not be empty");
    }

    private void helpModifyOrderCriterion() {
        System.out.println("USAGE:   modify-order-criterion (moc) <new criterion>");
        System.out.println("Example: modify-order-criterion TitleASC");
    }

    private void helpCreateExpression() {
        System.out.println("USAGE:   create-expression (ce) <expression>");
        System.out.println("Example: create-expression a ");
    }

    private void helpModifyExpression() {
        System.out.println("USAGE:   modify-expression (me)");
        System.out.println("Example: modify-expression");
    }

    private void helpDeleteExpression() {
        System.out.println("USAGE:   delete-expression (de)");
        System.out.println("Example: delete-expression");
    }

    private void helpListExpressions() {
        System.out.println("USAGE:   list-expressions (le)");
        System.out.println("Example: list-expressions");
    }

    private void helpListDocsExpression() {
        System.out.println("USAGE:   list-doc-expression (lde) <expr>");
        System.out.println("Example: list-doc-expression 2");
        System.out.println("USAGE:   list-doc-expression (lde)");
        System.out.println("Example: list-doc-expression");
    }

    //////////////////////
    // Parser functions //
    //////////////////////

    public Set<Integer> parseSelection(String selection, int size) throws SelectionException, RangeException {
        Set<Integer> current_selection = new HashSet<>();
        String[] intervals = selection.split(",");

        if (selection.charAt(selection.length()-1) == ',' || selection.charAt(0) == ',') {
            throw new SelectionException("commas should be between ranges or numbers", selection);
        }

        for (String interval : intervals) {
            String[] pos = interval.split("-");

            if (pos.length == 1) {
                // in this case, we only have 1 value
                if (interval.charAt(interval.length()-1) == '-') {
                    throw new SelectionException("unknown interval right value", selection);
                }

                int number;

                try {
                    number = Integer.parseInt(pos[0]);
                } catch (NumberFormatException ex) {
                    throw new SelectionException("unknown value", selection);
                }

                if (number > size || number <= 0)
                    throw new RangeException(number, size);

                current_selection.add(number);
            } else if (pos.length == 2) {
                // we have 2 values representing interval left and right

                int left, right;
                try {
                    left = Integer.parseInt(pos[0]);
                    right = Integer.parseInt(pos[1]);
                } catch (NumberFormatException ex) {
                    throw new SelectionException("unknown interval values", selection);
                }


                if (right > size || left <= 0) {
                    if (right > size)
                        throw new RangeException(right, size);
                    else
                        throw new RangeException(left, size);
                } else if (left <= right) {
                    for (int i = left; i <= right; i++) {
                        current_selection.add(i);
                    }
                } else {
                    throw new SelectionException("interval left value > interval right value", selection);
                }
            } else {
                throw new SelectionException("interval representation", selection);
            }
        }

        return current_selection;
    }

    private void listEntries() {
        List<UUID> entries = domainController.getSearch();

        int i = 1;
        System.out.println("Obtained " + entries.size() + " entries:");
        for (UUID entry : entries) {
            String title = domainController.getDocTitle(entry);
            String author = domainController.getDocAuthor(entry);

            System.out.println(i + ": " + title + " | " + author);
            i++;
        }
    }

    private void importDocuments(String[] line) {
        if (line.length < 2) {
            helpImportDocuments();
            return;
        }

        List<File> files = new ArrayList<>();

        for (int i = 1; i < line.length; i++) {
            files.add(new File(line[i]));
        }


        try {
            domainController.importDocuments(files);
        } catch (DomainException e) {
            System.out.println(e);
        }
    }

    private void exportDocuments(String[] line) {
        if (line.length != 3) {
            helpExportDocuments();
            return;
        }

        List<UUID> entries = domainController.getSearch();

        Set<Integer> selection;
        try {
            selection = parseSelection(line[1], entries.size());
        }
        catch (FrontendException e) {
            System.out.println(e);
            return;
        }

        List<UUID> docs = new ArrayList<>();

        for (Integer index : selection) {
            docs.add(entries.get(index-1));
        }

        try {
            domainController.exportDocuments(docs, new File(line[2]), ExportFormat.TXT);
        } catch (DomainException e) {
            System.out.println(e);
        }
    }

    private void createDocument(String[] line) {
        if (line.length > 1) {
            helpCreateDocument();
            return;
        }

        System.out.print("Enter title: ");
        String title = scanner.nextLine();

        System.out.print("Enter author: ");
        String author = scanner.nextLine();

        System.out.print("Enter content: ");
        String content = scanner.nextLine();

        try {
            domainController.createDocument(title, author, content);
            System.out.println("Created a new document");
        } catch (DomainException e) {
            System.out.println(e);
        }
    }

    private void deleteDocuments(String[] line) {
        if (line.length != 2) {
            helpDeleteDocuments();
            return;
        }

        List<UUID> entries = domainController.getSearch();

        Set<Integer> selection;
        try {
            selection = parseSelection(line[1], entries.size());
        }
        catch (FrontendException e) {
            System.out.println(e);
            return;
        }

        // We don't have to check if selection is empty
        // because an empty selection would result in
        // fewer parameters, which would trigger error

        for (Integer index : selection) {
            domainController.delDocument(entries.get(index-1));
        }

        domainController.resetSearch();
    }

    private void modifyTitle(String[] line) {
        if (line.length != 2) {
            helpModifyTitle();
            return;
        }

        List<UUID> entries = domainController.getSearch();

        Set<Integer> selection;
        try {
            selection = parseSelection(line[1], entries.size());
        }
        catch (FrontendException e) {
            System.out.println(e);
            return;
        }

        System.out.print("Enter title: ");
        String title = scanner.nextLine();

        for (Integer index : selection) {
            try {
                domainController.modifyDocTitle(entries.get(index-1), title);
            } catch (DomainException e) {
                System.out.println(e);
            }
        }

        System.out.println("Modified documents' titles");
    }

    private void modifyAuthor(String[] line) {
        if (line.length != 2) {
            helpModifyAuthor();
            return;
        }

        List<UUID> entries = domainController.getSearch();

        Set<Integer> selection;
        try {
            selection = parseSelection(line[1], entries.size());
        }
        catch (FrontendException e) {
            System.out.println(e);
            return;
        }

        System.out.print("Enter author: ");
        String author = scanner.nextLine();

        for (Integer index : selection) {
            try {
                domainController.modifyDocAuthor(entries.get(index-1), author);
            } catch (DomainException e) {
                System.out.println(e);
            }
        }

        System.out.println("Modified documents' authors");
    }

    private void modifyContent(String[] line) {
        if (line.length != 2) {
            helpModifyContent();
            return;
        }

        List<UUID> entries = domainController.getSearch();

        Set<Integer> selection;
        try {
            selection = parseSelection(line[1], entries.size());
        }
        catch (FrontendException e) {
            System.out.println(e);
            return;
        }

        System.out.print("Enter content: ");
        String content = scanner.nextLine();

        for (Integer index : selection) {
            try {
                domainController.modifyDocContent(entries.get(index-1), content);
            } catch (DomainException e) {
                System.out.println(e);
            }
        }

        System.out.println("Modified documents' contents");
    }


    private void viewContent(String[] line) {
        if (line.length != 2) {
            helpViewContent();
            return;
        }

        List<UUID> entries = domainController.getSearch();

        Set<Integer> selection;
        try {
            selection = parseSelection(line[1], entries.size());
        }
        catch (FrontendException e) {
            System.out.println(e);
            return;
        }

        for (Integer index : selection) {
            System.out.println();
            System.out.println("Content of document at index " + index + ":");
            String content = domainController.getDocContent(entries.get(index-1));
            System.out.println(content);
        }
    }


    private void viewFromAuthorTitle(String[] line) {
        if (line.length != 1) {
            helpViewFromAuthorTitle();
            return;
        }

        System.out.print("Enter title: ");
        String title = scanner.nextLine();

        System.out.print("Enter author: ");
        String author = scanner.nextLine();

        try {
            UUID id = domainController.getDocWithAuthorTitle(author, title);
            String content = domainController.getDocContent(id);
            System.out.println();
            System.out.println("The content is:");
            System.out.println(content);
        } catch (DomainException e) {
            System.out.println(e);
        }
    }

    private void resetSearch(String[] line) {
        if (line.length > 1) {
            helpResetSearch();
            return;
        }

        domainController.resetSearch();
        System.out.println("Search results have been reset to all documents");
    }

    private void listSearch(String[] line) {
        if (line.length > 1) {
            helpListSearch();
            return;
        }

        listEntries();
    }

    private void listAuthors(String[] line) {
        List<String> prefixWords = Arrays.asList(line).subList(1, line.length);
        String prefix = String.join(" ", prefixWords);
        Collection<String> authors = domainController.getAuthors(prefix);

        for (String author : authors) {
            System.out.println(author);
        }
    }

    private void listAuthorDocs(String[] line) {
        if (line.length == 1) {
            helpListAuthorDocs();
            return;
        }

        List<String> authorWords = Arrays.asList(line).subList(1, line.length);
        String author = String.join(" ", authorWords);

        Collection<UUID> ids;
        try {
            ids = domainController.getAuthorDocs(author);
            System.out.println("Obtained " + ids.size() + " entries:");

            int i = 1;
            for (UUID id : ids) {
                String title = domainController.getDocTitle(id);

                System.out.println(i + ": " + title);
                i++;
            }
        } catch (DomainException e) {
            System.out.println(e);
        }
    }

    private void listSimilarToDocs(String[] line) {
        if (line.length != 2) {
            helpListSimilarToDocs();
            return;
        }

        List<UUID> entries = domainController.getSearch();
        int index;
        try {
            index = Integer.parseInt(line[1]);
        } catch (NumberFormatException e) {
            System.out.println(e);
            return;
        }

        if (index > entries.size() || index <= 0) {
            System.out.println(new RangeException(index, entries.size()));
            return;
        }

        domainController.searchBySimilarDoc(entries.get(index-1), 20);

        listEntries();
    }

    private void listSimilarToWords(String[] line) {
        if (line.length == 1) {
            helpListSimilarToWords();
            return;
        }

        List<String> wordList = Arrays.asList(line).subList(1, line.length);
        String words = String.join(" ", wordList);

        try {
            domainController.searchByWordList(words, 20);
            List<UUID> entries = domainController.getSearch();

            listEntries();
        } catch (DomainException e) {
            System.out.println(e);
        }
    }

    private void modifyOrderCriterion(String[] line) {
        if (line.length != 2) {
            helpModifyOrderCriterion();
            return;
        }

        String newOrder = line[1];

        SortCriterion sortCriterion;

        switch (newOrder) {
            case "TitleASC":
                sortCriterion = SortCriterion.TitleASC;
                break;
            case "TitleDSC":
                sortCriterion = SortCriterion.TitleDSC;
                break;
            case "AuthorASC":
                sortCriterion = SortCriterion.AuthorASC;
                break;
            case "AuthorDSC":
                sortCriterion = SortCriterion.AuthorDSC;
                break;
            case "LengthASC":
                sortCriterion = SortCriterion.LengthASC;
                break;
            case "LengthDSC":
                sortCriterion = SortCriterion.LengthDSC;
                break;
            default:
                sortCriterion = SortCriterion.TitleASC;
        }

        domainController.changeOrder(sortCriterion);
        System.out.println("Changed order criterion");
    }

    private void createExpression(String[] line) {
        if (line.length == 1) {
            helpCreateExpression();
            return;
        }

        List<String> wordList = Arrays.asList(line).subList(1, line.length);
        String expression = String.join(" ", wordList);

        try {
            domainController.createExpr(expression);
            System.out.println("Created new expression");

        } catch (DomainException e) {
            System.out.println(e);
        }
    }

    private void modifyExpression(String[] line) {
        if (line.length != 1) {
            helpModifyExpression();
            return;
        }

        List<UUID> entries = new ArrayList<>(domainController.getExpressions());

        int i = 1;
        System.out.println("Obtained " + entries.size() + " expressions:");
        for (UUID entry : entries) {
            String expression;

            expression = domainController.getExprText(entry);

            System.out.println(i + ": " + expression);
            i++;
        }

        if (entries.size() != 0) {
            System.out.print("Select expression: ");
            String exprIdString = scanner.nextLine().trim().split(("\\s+"))[0];
            int index;
            try {
                index = Integer.parseInt(exprIdString);
            } catch (NumberFormatException ex) {
                System.out.println(ex);
                return;
            }

            System.out.print("Enter new expression: ");
            String expression = scanner.nextLine();

            try {
                domainController.replaceExpr(expression, entries.get(index-1));
            } catch (DomainException e) {
                System.out.println(e);
            }
        }
    }

    private void deleteExpression(String[] line) {
        if (line.length != 1) {
            helpDeleteExpression();
            return;
        }

        List<UUID> entries = new ArrayList<>(domainController.getExpressions());

        int i = 1;
        System.out.println("Obtained " + entries.size() + " expressions:");
        for (UUID entry : entries) {
            String expression;

            expression = domainController.getExprText(entry);

            System.out.println(i + ": " + expression);
            i++;
        }

        if (entries.size() != 0) {
            System.out.print("Select expression: ");
            String exprIdString = scanner.nextLine().trim().split(("\\s+"))[0];
            int index;
            try {
                index = Integer.parseInt(exprIdString);
            } catch (NumberFormatException ex) {
                System.out.println(ex);
                return;
            }

            domainController.delExpr(entries.get(index - 1));
            System.out.println("Deleted expression");
        }
    }

    private void listExpressions(String[] line) {
        if (line.length != 1) {
            helpListExpressions();
            return;
        }

        Collection<UUID> entries = domainController.getExpressions();

        int i = 1;
        System.out.println("Obtained " + entries.size() + " expressions:");
        for (UUID entry : entries) {
            String expression;

            expression = domainController.getExprText(entry);

            System.out.println(i + ": " + expression);
            i++;
        }
    }

    private void listDocsExpression(String[] line) {
        if (line.length == 1) {
            List<UUID> entries = new ArrayList<>(domainController.getExpressions());

            int i = 1;
            System.out.println("Obtained " + entries.size() + " expressions:");
            for (UUID entry : entries) {
                String expression;

                expression = domainController.getExprText(entry);

                System.out.println(i + ": " + expression);
                i++;
            }

            if (entries.size() != 0) {
                System.out.print("Select expression: ");
                String exprIdString = scanner.nextLine().trim().split(("\\s+"))[0];
                int index;
                try {
                    index = Integer.parseInt(exprIdString);
                } catch (NumberFormatException ex) {
                    System.out.println(ex);
                    return;
                }

                try {
                    domainController.searchByExpr(entries.get(index - 1));
                    System.out.println("Search done");
                } catch (DomainException ex) {
                    System.out.println(ex);
                    return;
                }

                listEntries();
            }
        } else if (line.length >= 2) {
            List<String> wordList = Arrays.asList(line).subList(1, line.length);
            String expression = String.join(" ", wordList);

            try {
                domainController.searchByExpr(expression);
                System.out.println("Search done");
            } catch (DomainException ex) {
                System.out.println(ex);
                return;
            }

            listEntries();
        }
    }

    private void saveState(String[] line) {
        domainController.saveState();
    }

    public void mainLoop() {
        loop:
        while (true) {
            System.out.print("Enter command (h for help): ");
            String[] line = scanner.nextLine().trim().split(("\\s+"));
            System.out.println();
            String cmd = line[0];

            switch (cmd) {
                case "i":
                case "import-documents":
                    importDocuments(line);
                    break;
                case "e":
                case "export-documents":
                    exportDocuments(line);
                    break;
                case "c":
                case "create-document":
                    createDocument(line);
                    break;
                case "d":
                case "delete-documents":
                    deleteDocuments(line);
                    break;
                case "mt":
                case "modify-title":
                    modifyTitle(line);
                    break;
                case "ma":
                case "modify-author":
                    modifyAuthor(line);
                    break;
                case "mc":
                case "modify-content":
                    modifyContent(line);
                    break;
                case "vc":
                case "view-content":
                    viewContent(line);
                    break;
                case "vcat":
                case "view-from-author-title":
                    viewFromAuthorTitle(line);
                    break;
                case "s":
                case "show-last-search":
                    listSearch(line);
                    break;
                case "rst":
                case "reset-search":
                    resetSearch(line);
                    break;
                case "la":
                case "list-authors":
                    listAuthors(line);
                    break;
                case "lad":
                case "list-author-docs":
                    listAuthorDocs(line);
                    break;
                case "lsd":
                case "list-similar-to-docs":
                    listSimilarToDocs(line);
                    break;
                case "lsw":
                case "list-similar-to-words":
                    listSimilarToWords(line);
                    break;
                case "moc":
                case "modify-order-cirterion":
                    modifyOrderCriterion(line);
                    break;

                case "ce":
                case "create-expression":
                    createExpression(line);
                    break;
                case "me":
                case "modify-expression":
                    modifyExpression(line);
                    break;
                case "de":
                case "delete-expression":
                    deleteExpression(line);
                    break;
                case "le":
                case "list-expressions":
                    listExpressions(line);
                    break;
                case "lde":
                case "list-docs-expression":
                    listDocsExpression(line);
                    break;

                case "clr":
                case "clear":
                    System.out.print("\033[H\033[2J");
                    System.out.flush();
                    continue;
                case "qs":
                case "quit-save":
                    System.out.println("Saving...");
                    saveState(line);
                    System.out.println("Exiting...");
                    break loop;
                case "q":
                case "quit":
                    System.out.println("Exiting...");
                    break loop;
                case "h":
                case "help":
                    printHelp();
                    break;
                default:
                    System.out.println("Unknown command");
                    printHelp();
            }

            System.out.println();
            System.out.println();
        }
    }
}
