package edu.upc.prop411.frontend;

/**
 * CLI Bad selection exception
 */
public class SelectionException extends FrontendException {
    public SelectionException(String str) {
        super(str);
    }

    public SelectionException(String str, String selection) {
        super(String.format("Selection error:  %s;   in selection == %s ==", str, selection));
    }
}
