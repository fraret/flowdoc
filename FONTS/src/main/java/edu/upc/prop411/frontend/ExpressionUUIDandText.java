package edu.upc.prop411.frontend;

import java.util.UUID;

/**
 * Pair of UUID and text, used in selection comboBoxes
 *
 * @author Sergi Soler Arrufat
 */
class ExpressionUUIDandText {
    /**
     * The UUID id
     */
    public UUID id;
    /**
     * The text
     */
    public String string;

    /**
     * Constructs a new pair of UUID and text
     * @param id the UUID
     * @param string the text
     */
    ExpressionUUIDandText(UUID id, String string) {
        this.id = id;
        this.string = string;
    }

    @Override
    public String toString() {
        return string;
    }
}
