package edu.upc.prop411.domain.exceptions.critical;

/**
 * Exception thrown when we exceed the maximum number of retries for creating a UUID that is not already in use.
 * It should statistically never happen (collision chance is n/2^128 and with a small number of retries the probability
 * of multiple collisions is practically 0) but if for some reason the system's random number generator was not working
 * properly we would not want to corrupt the application's data or stall forever
 */
public class UUIDGenerationException extends RuntimeException{
    public UUIDGenerationException()  {
        super("A new UUID could not be generated");
    }
}
