package edu.upc.prop411.domain;

/**
 * Represents an expression which contains another expression the results of which are negated
 *
 * @author Sergi Soler Arrufat
 */
class ExpNot extends Expression{

    /**
     * The expression being negated
     */
    private final Expression rightExpression;

    /**
     * Constructs a NOT expression
     *
     * @param exp The expression that will be negated. Must be valid, that is, it cannot be null
     * @param text The text from which the current AST node was parsed
     */
    public ExpNot(Expression exp, String text) {
        super(text);

        assert (exp != null) : "Expressions used to create a ExpNot expression must not be null";
        rightExpression = exp;
    }

    /**
     * Returns the Expression that is being negated
     *
     * @return The expression this node negates
     */
    Expression getExpression() {
        return rightExpression;
    }
}
