package edu.upc.prop411.domain.exceptions.critical;

import edu.upc.prop411.domain.exceptions.recoverable.DomainException;

public class TermFrequencyFinderException extends DomainException {

    public TermFrequencyFinderException(String str) {
        super(str);
    }
}
