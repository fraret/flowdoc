package edu.upc.prop411.domain.exceptions.recoverable;

public class ExpressionException extends DomainException{

    public ExpressionException(String str) {
        super(str);
    }
    public ExpressionException(String str, String expression) {
        super(String.format("Expression error:  %s;   in expression == %s ==", str, expression));
    }
}
