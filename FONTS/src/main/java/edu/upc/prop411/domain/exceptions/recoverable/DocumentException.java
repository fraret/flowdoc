package edu.upc.prop411.domain.exceptions.recoverable;

public class DocumentException extends DomainException {
    public DocumentException(String str) {
        super(str);
    }
}
