package edu.upc.prop411.domain;


import edu.upc.prop411.domain.exceptions.recoverable.ExpressionException;

import java.util.ArrayList;
import java.util.List;

/**
 * Token class, for intermediate use during expression parsing
 * <p>
 * This class represents a token in a text representing an expression.
 * A token is defined as a parenthesis or curly brace, a word (a sequence of alphanumeric characters without any other
 * kind of character in it), an operator (characters '!', '|', '&amp;') or a substring (an arbitrary sequence of characters
 * -- except '"' -- delimited by quotation marks, that is,  '"')
 */
class ExpToken {
    /**
     * Text from which the token was created and which represents it
     */
    private final String text;

    /**
     * For use in substring tokens, that would be the text without the quotation marks.
     * Otherwise it should be equal to Text
     */
    private final String innerText;

    /**
     * Possible types of tokens
     */
    public enum TokenType {
        OPENING_CURLY_BRACE,
        CLOSING_CURLY_BRACE,
        WORD,
        OPENING_PARENTHESIS,
        CLOSING_PARENTHESIS,
        SUBSTRING,
        OR_OPERATOR,
        AND_OPERATOR,
        NOT_OPERATOR
    }

    private final TokenType  tokenType;

    /**
     * Primary constructor, needs the type of token and text.
     * It should only be called manually for use in testing, otherwise use the tokenize() method
     * @param tokenType type of the token being created
     * @param text text from which the token was created
     */
    protected ExpToken(TokenType tokenType, String text) {
        this.tokenType = tokenType;
        this.text = text;
        this.innerText = text;
    }

    /**
     * Primary constructor, needs the type of token and text.
     * It should only be called manually for use in testing, otherwise use the tokenize() method
     * @param tokenType type of the token being created
     * @param text text from which the token was created
     * @param innerText for use in substring tokens, that would be the text without the quotation marks
     */
    protected ExpToken(TokenType tokenType, String text, String innerText) {
        this.tokenType = tokenType;
        this.text = text;
        this.innerText = innerText;
    }

    @Override
    public String toString() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExpToken expToken = (ExpToken) o;

        if (!text.equals(expToken.text)) return false;
        if (!innerText.equals(expToken.innerText)) return false;
        return tokenType == expToken.tokenType;
    }

    public String getInnerText() {
        return innerText;
    }

    public String getText() {
        return text;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    /**
     * Tokenizes a text for further parsing
     * <p>
     * Tokenization is intended to convert the sequence of unicode characters into the actual types of objects that make
     * up an expression, such as opening and closing parenthesis, words (not characters), substrings, operators, etc.
     * It also serves to get rid of whitespace and detect some kinds of errors before the actual parsing is done
     *
     * @param expText a text representing an expression
     * @return the list of tokens that make up the expression
     * @throws ExpressionException if the given text cannot be tokenized as it is null, empty or not a well-formed expression
     */
    public static List<ExpToken> tokenize(String expText) throws ExpressionException {
        if (expText == null) {
            throw new ExpressionException("Tried to tokenize null text");
        }
        List<ExpToken> ans = new ArrayList<>();
        int tokBegin=0;
        for (int i = 0; i < expText.length(); ++i) {
            char c = expText.charAt(i);
            if (!TextUtils.isWordElement(c) && tokBegin != i) {
                ans.add(new ExpToken(TokenType.WORD, expText.substring(tokBegin, i).toLowerCase()));
                tokBegin = i;
            }
            switch (c) {
                case '{':
                    ans.add(new ExpToken(TokenType.OPENING_CURLY_BRACE, "{"));
                    ++tokBegin;
                    break;
                case '}':
                    ans.add(new ExpToken(TokenType.CLOSING_CURLY_BRACE, "}"));
                    ++tokBegin;
                    break;
                case '(':
                    ans.add(new ExpToken(TokenType.OPENING_PARENTHESIS, "("));
                    ++tokBegin;
                    break;
                case ')':
                    ans.add(new ExpToken(TokenType.CLOSING_PARENTHESIS, ")"));
                    ++tokBegin;
                    break;
                case '"':
                    ++i;
                    boolean ended = false;
                    while (i < expText.length()) {
                        if (expText.charAt(i) == '"') {
                            ended = true;
                            break;
                        }
                        ++i;
                    }
                    if (!ended) {
                        throw new ExpressionException("Expression ends with incomplete sequence of words", expText);
                    }
                    ans.add(new ExpToken(TokenType.SUBSTRING, expText.substring(tokBegin, i+1), expText.substring(tokBegin+1, i)));
                    tokBegin = i+1;
                    break;
                case '|':
                    ans.add(new ExpToken(TokenType.OR_OPERATOR, "|"));
                    ++tokBegin;
                    break;
                case '&':
                    ans.add(new ExpToken(TokenType.AND_OPERATOR, "&"));
                    ++tokBegin;
                    break;
                case '!':
                    ans.add(new ExpToken(TokenType.NOT_OPERATOR, "!"));
                    ++tokBegin;
                    break;
                case ' ':
                    ++tokBegin;
                    break;
                default:
                    if (!TextUtils.isWordElement(c))
                        throw new ExpressionException(String.format("Unsupported character %c (\\u%h) found while tokenizing", c, c), expText);
                    break;

            }
        }

        if (tokBegin != expText.length())
            ans.add(new ExpToken(TokenType.WORD, expText.substring(tokBegin).toLowerCase()));

        return ans;
    }

}
