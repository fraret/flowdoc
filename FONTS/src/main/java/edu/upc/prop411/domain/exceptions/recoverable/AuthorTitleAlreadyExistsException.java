package edu.upc.prop411.domain.exceptions.recoverable;

public class AuthorTitleAlreadyExistsException extends DomainException{
    public AuthorTitleAlreadyExistsException(String author, String title, String place) {
        super(String.format("The document with author %s and title %s already exists in %s", author, title, place));
    }
}

