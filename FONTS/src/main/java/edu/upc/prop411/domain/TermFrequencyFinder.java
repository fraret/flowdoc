package edu.upc.prop411.domain;

import edu.upc.prop411.domain.exceptions.critical.TermFrequencyFinderException;

import java.util.HashMap;

import static java.lang.Character.*;

/**
 * Class in charge of doing term frequency computations and substring searches on contents
 *
 * @author Eduard de Vidal Flores
 */
public class TermFrequencyFinder {
    /**
     * Returns whether a character can be part of a word, right now equivalent to being alphanumeric
     * @param c the character to
     * @return true if the character is alphanumeric, false otherwise
     */
    static private boolean isWordElement(char c) {
        int charType = Character.getType(c);
        return charType == UPPERCASE_LETTER ||
                charType == LOWERCASE_LETTER ||
                charType == DECIMAL_DIGIT_NUMBER;
    }

    /**
     * Generates a word frequency map for the Finder to use in TF-IDF and other types of searches
     *
     * @param content the content from which to generate the word frequency map
     * @return a map keyed by words that appear in the content and with values the frequency of those words in the content
     * @throws TermFrequencyFinderException if the given text is null
     */
    public static HashMap<String, Integer> termFrequency(String content) throws TermFrequencyFinderException {
        if (content == null) {
            throw new TermFrequencyFinderException("Tried to find the term frequency of null text");
        }

        HashMap<String, Integer> tf = new HashMap<>();
        int begin=0;
        for (int i = 0; i < content.length(); ++i) {
            char c = content.charAt(i);
            if (!isWordElement(c)) {
                if (begin != i){
                    String word = content.substring(begin, i).toLowerCase();
                    Integer count = tf.get(word);
                    tf.put(word, (count == null) ? 1 : count+1);
                }
                begin = i+1;
            }
        }

        if (begin != content.length()) {
            String word = content.substring(begin).toLowerCase();
            Integer count = tf.get(word);
            tf.put(word, (count == null) ? 1 : count+1);
        }

        return tf;
    }

    /**
     * Given a sequence of words checks whether they appear in that order in a given content
     * Be aware that the comparison is performed on a word-by-word basis, see ExpSubstr description for a more detailed
     * explanation on how this is done
     *
     * @param content the content where the sequence of words will be searched
     * @param substr sequence of words to check against
     * @return true if substr appears in the content surrounded by whitespace, punctuation or at the beginning or end of it, false otherwise
     * @throws TermFrequencyFinderException if the given content is null, or if substr is null or an empty String
     */
    public static boolean containsSubstr(String content, String substr) throws TermFrequencyFinderException {
        if (content == null) {
            throw new TermFrequencyFinderException("Tried to find a substring in a null text");
        }
        if (substr == null) {
            throw new TermFrequencyFinderException("Tried to find a null substring in a text");
        }
        if (substr.isEmpty()) {
            throw new TermFrequencyFinderException("Tried to find an empty substring in a text");
        }

        String contentLower = content.toLowerCase();
        String substrLower = substr.toLowerCase();

        boolean found = false;
        for(int i = 0; !found; ++i) {
            i = contentLower.indexOf(substrLower, i);
            if(i == -1) break;

            // Check that the occurrence begins and ends at word boundaries
            boolean beginsAtBoundary = (i == 0) || !isWordElement(contentLower.charAt(i-1));
            boolean endsAtBoundary = ((i + substrLower.length()) == contentLower.length()) || !isWordElement(contentLower.charAt(i+substrLower.length()));
            found = (beginsAtBoundary && endsAtBoundary);
        }

        return found;
    }
}
