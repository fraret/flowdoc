package edu.upc.prop411.domain.exceptions.recoverable;

public class NotExistsException extends DomainException{
    public NotExistsException(String typeName, String searchPlace, String identifier) {
        super(String.format("Could not find %s identified by %s in %s", typeName, identifier, searchPlace));
    }
}

