package edu.upc.prop411.domain;

import edu.upc.prop411.domain.exceptions.recoverable.ExpressionException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Class representing an expression that matches documents which contain a set (or possibly only one) of words
 *
 * @author Sergi Soler Arrufat
 */
class ExpWordSet extends Expression{

    /**
     * List of words (at least one) that have to be in a document for it to match this expression
     */
    private final Set<String> words;

    /**
     * Creates a new word/set of words expression
     *
     * @param words List of words. Must contain
     * @param text the text from which the current AST node was parsed
     * @throws ExpressionException it the wordset is empty
     */
    public ExpWordSet(Set<String> words, String text) throws ExpressionException {
        super(text);
        if (words.size() < 1)
            throw new ExpressionException("List of words must not be empty", text);
        this.words = new HashSet<>(words);
    }

    /**
     * Returns a read-only version of the set of words
     * @return a read-only version of the set of words
     */
    public Set<String> getWords() {
        // We need to use Collections.unmodifiable to avoid allowing changes to the expression through the gotten
        // set
        return Collections.unmodifiableSet(words);
    }

    /**
     * Returns the number of words in the set
     * @return the number of words in the set (&gt;= 1)
     */
    public int getSize() {
        return words.size();
    }

}
