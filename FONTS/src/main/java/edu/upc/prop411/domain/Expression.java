package edu.upc.prop411.domain;


import java.io.Serializable;

/**
 * Abstract class for all kinds of expressions.
 * Expressions are represented as an Abstrac Syntax Tree (AST), with each different kind of node represented by a different
 * subclass.
 *
 * @author Sergi Soler Arrufat
 */
abstract class Expression implements Serializable {
    private String text;

    /**
     * Private constructor for an expression object. It will only be used by the subclassed constructors
     * @param text Text from which the expression has been created
     */
    protected Expression(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
