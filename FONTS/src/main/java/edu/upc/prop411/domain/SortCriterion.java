package edu.upc.prop411.domain;

/**
 * The sort criterion for the search results.
 */
public enum SortCriterion {
    TitleASC,
    TitleDSC,
    AuthorASC,
    AuthorDSC,
    LengthASC,
    LengthDSC
}
