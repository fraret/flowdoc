package edu.upc.prop411.domain;

import java.util.function.Function;

import static java.lang.Character.*;

/**
 * Class for various common text processing utilities (static methods)
 *
 * @author Sergi Soler Arrufat
 */
class TextUtils {


    /**
     * Returns whether a codepoint can be part of a word, right now equivalent to being alphanumeric
     * @param codePoint the codepoint to check
     * @return true if the codepoint is alphanumeric, false otherwise
     */
    static boolean isWordElement(int codePoint) {
        int charType = Character.getType(codePoint);
        return charType == UPPERCASE_LETTER ||
                charType == LOWERCASE_LETTER ||
                charType == DECIMAL_DIGIT_NUMBER;
    }

    /**
     * Returns whether a codepoint is a control character or line/paragraph separator
     * @param codePoint the codepoint to check
     * @return true if the codepoint is a line/paragraph separator or a control character, false otherwise
     */
    static boolean isControlLineSeparator(int codePoint) {
        int charType = Character.getType(codePoint);
        return charType == Character.PARAGRAPH_SEPARATOR ||
                charType == Character.CONTROL ||
                charType == Character.LINE_SEPARATOR;
    }

    /**
     * Given a string and a function that returns a boolean given a unicode code-point (not UTF-16 character, a real
     * Unicode codepoint) returns true if all codepoints evaluate true, false otherwise
     *
     * @param str the string to evaluate
     * @param function an int (codepoint) to boolean function, typically it will check a character type
     * @return true if the function evaluates true for all the codepoints in the string, false otherwise
     */
    static boolean allCodepointsTrue(String str, Function<Integer, Boolean> function) {
        int position = 0;
        int len = str.length();
        while (position < len) {
            // Since we want to iterate over Unicode code points (real characters) and not its UTF-16 "characters"
            // (of which we might need 2 to encode an emoji, for example) we have to use codePointAt
            int curCodePoint = str.codePointAt(position);
            if (!function.apply(curCodePoint)) return false;
            position += Character.charCount(curCodePoint);
        }
        return true;
    }


    /**
     * Given a string and a function that returns a boolean given a unicode code-point (not UTF-16 character, a real
     * Unicode codepoint) returns true if at least one codepoint evaluates true, false otherwise
     *
     * @param str the string to evaluate
     * @param function an int (codepoint) to boolean function, typically it will check a character type
     * @return true if the function evaluates true for at least one codepoint, false otherwise
     */
    static boolean atLeastOneCodepointTrue(String str, Function<Integer, Boolean> function) {
        // We use De Morgan's laws
        return !allCodepointsTrue(str, c -> !function.apply(c));
    }
}
