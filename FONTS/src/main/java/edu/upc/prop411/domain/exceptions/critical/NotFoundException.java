package edu.upc.prop411.domain.exceptions.critical;

import java.util.UUID;

public class NotFoundException extends RuntimeException{
    public NotFoundException(String typeName, String searchPlace, UUID uuid) {
        super(String.format("Could not find %s identified by %s in %s", typeName, uuid.toString(), searchPlace));
    }
}

