package edu.upc.prop411.domain;

import edu.upc.prop411.domain.exceptions.recoverable.DocumentException;

import java.io.Serializable;

/**
 * A document as specified in the given specification.
 * It is composed of a title, an author and content. The title and author must be non-empty and contain no line breaks.
 * The content must be non-empty
 *
 *
 * @author Sergi Soler Arrufat
 */
public class Document implements Serializable {
    /**
     * The title of the Document
     * It should not contain line breaks
     */
    private String title;

    /**
     * The Author of the Document
     * It should not contain line breaks
     */
    private String author;

    /**
     * The content of the Document
     * It will usually contain line breaks
     */
    private String content;

    /**
     * Creates a Document with the given parameters
     *
     * @param title The title of the document to create
     * @param author The author of the document to create
     * @param content The content of the document to create
     * @throws DocumentException if the title, author or content are not valid
     */
    public Document(String title, String author, String content) throws DocumentException {
        if (!validateTitleAuthor(title))
            throw new DocumentException("Invalid title of new Document");
        if (!validateTitleAuthor(author))
            throw new DocumentException("Invalid author of new Document");
        if (content == null || content.length() == 0)
            throw new DocumentException("null or 0-length content provided in new Document constructor");
        this.title = title;
        this.author = author;
        this.content = content;
    }

    /**
     * Returns the title of the Document
     * @return a String containing the title of the document
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the author of the Document
     * @return a String containing the author of the document
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Returns the content of the Document
     * @return a String containing the content of the document
     */
    public String getContent() {
        return content;
    }


    /**
     * A maximum value for characters in the title or author field
     */
    private static final int MAX_CHARS_IN_TITLE_AUTHOR=200;

    /**
     * Validates a String for use in a Title or author field:
     * It disallows paragraph separators, control characters and line breaks. Everything else is allowed,
     * so long as the string is not empty nor it exceeds the maximum number of characters as specified in the
     * relevant constant.
     *
     * @param str The string to validate
     * @return true if the string is valid, false otherwise
     */
    private boolean validateTitleAuthor(String str) {
        if (str == null || str.length() < 1 || str.length() > MAX_CHARS_IN_TITLE_AUTHOR) {
            return false;
        }

        return TextUtils.allCodepointsTrue(str, (c) -> !TextUtils.isControlLineSeparator(c));
    }

    /**
     * Modifies the title of the document
     * @param title the new title for the document
     * @throws DocumentException if the new title is invalid
     */
    public void changeTitle(String title) throws DocumentException {
        if (!validateTitleAuthor(title))
            throw new DocumentException("Invalid new title");
        this.title = title;
    }

    /**
     * Modifies the author of the document
     * @param author the new author for the document
     * @throws DocumentException if the new author is invalid
     */
    public void changeAuthor(String author) throws DocumentException {
        if (!validateTitleAuthor(author))
            throw new DocumentException("Invalid new author");
        this.author = author;
    }

    /**
     * Modifies the content of the document
     * @param content the new content for the document
     * @throws DocumentException if the new content is invalid
     */
    public void changeContent(String content) throws DocumentException {
        if (content == null || content.length() == 0)
            throw new DocumentException("null or 0-length content provided for changing content");
        this.content = content;
    }

    /**
     * Returns the size (in UTF-16 code units) of the content of the document
     * @return the size in UTF-16 code units of the content of the document
     */
    public int getSize() {
        return content.length();
    }
}
