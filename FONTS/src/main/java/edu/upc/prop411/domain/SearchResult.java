package edu.upc.prop411.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Container of ids (result) of a search.
 *
 * @author Ferran Hermida Rivera
 */

class SearchResult {

    private ArrayList<UUID> search;

    private SortCriterion order;

    /**
     * Class constructor.
     */
    public SearchResult(){
        search = new ArrayList<>();
        order = SortCriterion.TitleASC;

    }

    /**
     * Sets the content of search in the SearchResult instance
     *
     * @param ids ArrayList of id to sava in the SearchResult instance.
     */
    public void setSearch(List<UUID> ids){
        search = new ArrayList<>(ids);
    }

    /**
     * Changes the results sorting criteria.
     *
     * @param order The order in which the results must be sorted
     */
    public void changeOrder(SortCriterion order){
        this.order = order;
    }

    /**
     * Return all the ids of the documents which fulfills the search criteria.
     *
     * @return A List with all the ids of the documents which fulfills the search
     *         criteria or null if there is non.
     */
    public List<UUID> getSearch(){
        return  search;
    }

    /**
     * Returns how many documents fulfills the search criteria.
     *
     * @return the number of documents which fulfills the search criteria.
     */
    public Integer getSearchSize(){
        return search.size();
    }

    /**
     * It resets the search result. This means there are no documents which fulfills the search
     * criteria and the search size is 0.
     */
    public void resetSearch(){
        search.clear();
    }

    /**
     * Returns the sort criterion of a search result
     *
     * @return the sort criterion that has to be applied to the search result
     */
    public SortCriterion getSortCriterion(){return order;}


}

