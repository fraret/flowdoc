package edu.upc.prop411.domain;

import edu.upc.prop411.domain.exceptions.recoverable.ExpressionException;

import java.util.HashSet;
import java.util.Set;

import static edu.upc.prop411.domain.TextUtils.*;

/**
 * Represents a substring expression.
 * <p>
 * According to the given specification, this substring will consist of a set of
 * words, that is, of multiple alphanumeric characters separated by whitespace. It is of particular interest that,
 * as interpreted from the given specification, it should not contain a word partially.
 * That means that this kind of expressions performs comparisons on a word-by-word basis, and therefore a document
 * with content "En un lugar de la Mancha" would match ExpSubstr with substr "de la", "un lugar" but not "la Man"
 *
 * @author Sergi Soler Arrufat
 */
class ExpSubstr extends Expression{
    private final String substr;


    /**
     * Validates that the substring is a valid one, that is, is a sequence of words.
     * @param substr The substring to validate
     * @return true if the substring is valid, false otherwise
     */
    private boolean validateSubstring(String substr) {
        assert substr != null;
        return allCodepointsTrue(substr, c -> !isControlLineSeparator(c)) && atLeastOneCodepointTrue(substr, TextUtils::isWordElement);
    }

    /**
     * Creates a new substring expression
     * @param substr The substring, quotation marks excluded
     * @param text The original text, in this case probably the substring but with the original quotation marks included
     * @throws ExpressionException if the substring is invalid
     */
    public ExpSubstr(String substr, String text) throws ExpressionException {
        super(text);
        if (!validateSubstring(substr))
            throw new ExpressionException("Invalid substring", text);
        this.substr = substr;
    }

    /**
     * Returns a Set with all the words appearing in the substring
     * @return a Set with all the words appearing in the substring
     */
    public Set<String> getWords() {
        HashSet<String> ans = new HashSet<>();
        StringBuilder currWord = new StringBuilder();
        for(int i = 0; i < substr.length(); ++i) {
            char c = substr.charAt(i);
            if (TextUtils.isWordElement(c)) {
                currWord.append(c);
            } else {
                if (currWord.length() > 0) ans.add(currWord.toString().toLowerCase());
                currWord.setLength(0); //clear the current word
            }
        }
        if (currWord.length() > 0) ans.add(currWord.toString().toLowerCase());
        return ans;
    }

    /**
     * Returns the substring
     * @return the substring
     */
    public String getSubstr() {
        return substr;
    }
}
