package edu.upc.prop411.domain;

import edu.upc.prop411.domain.exceptions.critical.NotFoundException;
import edu.upc.prop411.domain.exceptions.critical.UUIDGenerationException;
import edu.upc.prop411.domain.exceptions.recoverable.ExpressionException;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Library (collection) of expressions which have been saved for future use.
 * Also in charge of parsing text and converting it into actual Expression objects
 *
 * @author Sergi Soler Arrufat
 *
 */
public class ExpressionLibrary implements Serializable {

    /**
     * Map containing saved expressions by UUID
     */
    private final HashMap<UUID, Expression> expressionHashMap;

    public ExpressionLibrary() {
        expressionHashMap = new HashMap<>();
    }

    /**
     * Parses the given text into an Expression tree
     *
     * @param texpr the text to be parsed
     * @return the Expression which is the root of the AST of the parsed text
     * @throws ExpressionException if the given text does not represent a valid expression
     */
    private static Expression parseText(String texpr) throws ExpressionException {
        ExpressionParser parser = new ExpressionParser();
        List<ExpToken> tokens = ExpToken.tokenize(texpr);
        return parser.parse(tokens);
    }

    final int MAX_UUID_RETRIES = 4;

    /**
     * Creates and adds an expression to the library from a text
     *
     * @param texpr the text to be parsed for the expression to be created
     * @return the id for the new expression
     * @throws UUIDGenerationException if an ID cannot be generated for internal reasons
     * @throws ExpressionException if the given text does not represent a valid expression
     */
    public UUID addExpression(String texpr) throws ExpressionException {
        Expression newExpresion = parseText(texpr);
        UUID newUUID = UUID.randomUUID();
        int retries = 0;
        while (expressionHashMap.containsKey(newUUID) && retries < MAX_UUID_RETRIES) {
            newUUID = UUID.randomUUID();
            ++retries;
        }

        if (retries == MAX_UUID_RETRIES)
            throw new UUIDGenerationException();

        expressionHashMap.put(newUUID, newExpresion);
        return newUUID;
    }

    /**
     * Returns the text for an expression
     *
     * @param id the identifier for the expression
     * @return the String containing the expression in text form
     * @throws NotFoundException if the given id does not identify an expression in the library
     */
    public String getExpressionText(UUID id) {
        if (!expressionHashMap.containsKey(id))
            throw new NotFoundException("Expression", this.getClass().getSimpleName(), id);
        return expressionHashMap.get(id).toString();
    }

    /**
     * Deletes an expression from the library given its identifier
     *
     * @param id the identifier of the expression to be deleted
     * @throws NotFoundException if the given id does not identify an expression in the library
     */
    public void delExpression(UUID id) {
        if (!expressionHashMap.containsKey(id))
            throw new NotFoundException("Expression", this.getClass().getSimpleName(), id);
        expressionHashMap.remove(id);
    }

    /**
     * Returns a collection with the IDs of every stored expression. It might be an empty list
     *
     * @return a collection containing the IDs of every stored expression, which might be empty if no expressions are stored
     */
    public Collection<UUID> getExpressions() {
        return expressionHashMap.keySet();
    }

    /**
     * Retrieves the expression with the given ID
     *
     * @param id the id of the expression to retrieve
     * @return the Expression object stored with the supplied ID
     * @throws NotFoundException if the given id does not identify an expression in the library
     */
    public Expression getExpression(UUID id) {
        if (!expressionHashMap.containsKey(id))
            throw new NotFoundException("Expression", this.getClass().getSimpleName(), id);
        return expressionHashMap.get(id);
    }

    /**
     * Returns a newly created expression from the supplied text.
     * To be used when an expression object is needed and it is not desired to store said expression in the expression
     * library.
     *
     * @param texpr the text to be parsed
     * @return the Expression which is the root of the AST of the parsed text
     * @throws ExpressionException if the given text does not represent a valid expression
     */
    public Expression getVolatileExpression(String texpr) throws ExpressionException {
        return parseText(texpr);
    }
}
