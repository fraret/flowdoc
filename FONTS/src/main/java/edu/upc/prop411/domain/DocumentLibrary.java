package edu.upc.prop411.domain;

//import javax.swing.text.Document;
import edu.upc.prop411.domain.exceptions.critical.NotFoundException;
import edu.upc.prop411.domain.exceptions.recoverable.AuthorTitleAlreadyExistsException;
import edu.upc.prop411.domain.exceptions.recoverable.DocumentException;
import edu.upc.prop411.domain.exceptions.recoverable.NotExistsException;

import java.io.Serializable;
import java.util.*;

/**
 * Library (collection) of documents that contains all documents.
 *
 * @author Ferran Hermida Rivera
 */

public class DocumentLibrary implements Serializable {

    /**
     * Internal storage of documents indexed by the identifier
     */
    private final HashMap <UUID, Document> documents;
    /**
     * Internal association of authors and its documents
     */
    private final HashMap <String, HashMap<String, UUID>> authorTitleIndex;

    private boolean existsAuthorTitle(String author, String title){
        if (authorTitleIndex.containsKey(author)) {
            return authorTitleIndex.get(author).containsKey(title);
        }
        return false;
    }

    /**
     * Class constructor.
     */
    public DocumentLibrary(){
        documents = new HashMap<>();
        authorTitleIndex = new HashMap<>();
    }

    /**
     * Returns a <code>Document</code> that can then be consulted and modified
     *
     * @param id a unique id that identifies a document
     * @return   the document identified by id
     */
    public Document getWithId (UUID id) {
        Document doc = documents.get(id);
        if (doc == null) throw new NotFoundException("Document", "Library", id);
        return doc;
    }

    /**
     * Creates a new  <code>Document</code> instance with the  given <code>title</code>, <code>author</code>
     * and <code>content</code>. The object is added to the library.
     *
     * @param title   the document title.
     * @param author  the document author.
     * @param content the document content.
     * @return        the document identifier
     */
    public UUID createDocument(String title, String author, String content) throws AuthorTitleAlreadyExistsException, DocumentException {
        UUID identifier =  UUID.randomUUID();
        Document doc = new Document(title, author, content);
        while (documents.containsKey(identifier)) identifier = UUID.randomUUID();

        if (existsAuthorTitle(author, title)){
             throw new AuthorTitleAlreadyExistsException(author, title, "Library");
        }
        if (! authorTitleIndex.containsKey(author)) authorTitleIndex.put(author, new HashMap<>());
        authorTitleIndex.get(author).put(title, identifier);
        

        documents.put(identifier, doc);
        return identifier;
    }

    /**
     * Returns a list with the authors whose name starts with the specified prefix.
     *
     * @param prefix the prefix which is being looked for in the author name
     * @return       All the authors which name begins by prefix
     */
    public Collection<String> getAuthors(String prefix){
        Collection<String> authors = new ArrayList<>();
        for (String s: authorTitleIndex.keySet()){
            if (s.startsWith(prefix)) authors.add(s);
        }
        return authors;
    }

    /**
     * Returns the identifier of the document whose author is <code>author</code>
     * and title is <code>title</code>. The instance itself can be obtained with <code>getWithId</code> method.
     *
     * @param author the author of the document
     * @param title  the title of the document
     * @return       the id of the document with title <code>title</code> and author <code>author</code>.
     */
    public UUID getWithAuthorTitle(String author, String title) throws NotExistsException {
        HashMap<String, UUID> authorDocuments = authorTitleIndex.get(author);
        if (authorDocuments == null) throw new NotExistsException("Document", "Library", author+" "+title);
        return authorDocuments.get(title);
    }

    /**
     * Returns a list with the documents identifiers which author is the specified. the <code>Document</code>
     * instance can be obtained with <code>getWithId</code> method.
     *
     * @param author the name of the author
     * @return       the ids of all the documents which author name is <code>author</code>
     */
    public Collection<UUID> getAuthorDocs(String author) throws NotExistsException {
        if (authorTitleIndex.containsKey(author)) return authorTitleIndex.get(author).values();
        else throw new NotExistsException("Author", "Library", author);
    }

    /**
     * Modifies the author name of the document identified by its unique identifier.
     *
     * @param id     unique identifier of the document to be modified
     * @param author the new author name of the <code>Document</code> object identified by <code>id</code>
     */
    public void modifyDocAuthor(UUID id, String author) throws AuthorTitleAlreadyExistsException, DocumentException{
        Document doc = documents.get(id);
        if (doc == null) throw new NotFoundException("Document", "Library", id);

        String title = doc.getTitle();
        String oldAuthor = doc.getAuthor();

        if (author.equals(oldAuthor)) return;

        if (existsAuthorTitle(author, title)){
           throw new AuthorTitleAlreadyExistsException(author, title, "Library");
        }

        doc.changeAuthor(author);
        authorTitleIndex.get(oldAuthor).remove(title);
        if (authorTitleIndex.get(oldAuthor).isEmpty()) authorTitleIndex.remove(oldAuthor);

        HashMap<String, UUID> docUUID = new HashMap<>();
        docUUID.put(title, id);
        HashMap<String, UUID> authorDocs = authorTitleIndex.putIfAbsent(author, docUUID);
        if(authorDocs != null) { authorDocs.put(title, id); }
    }

    /**
     * Modifies the title of a given document identified by its unique identifier.
     *
     * @param id     unique identifier of the document to be modified
     * @param title  the new title of the <code>Document</code> object identified by <code>id</code>
     */
    public void modifyDocTitle(UUID id, String title) throws AuthorTitleAlreadyExistsException, DocumentException{
        Document doc = documents.get(id);
        if (doc == null) throw new NotFoundException("Document", "Library", id);

        String author = doc.getAuthor();
        String oldTitle = doc.getTitle();

        if (title.equals(oldTitle)) return;

        if (existsAuthorTitle(author, title)){
            throw new AuthorTitleAlreadyExistsException(author, title, "Library");
        }

        doc.changeTitle(title);
        HashMap<String, UUID> authorDocs = authorTitleIndex.get(author);
        authorDocs.remove(oldTitle);
        authorDocs.put(title, id);
    }

    /**
     * Modifies the content of a given document identifies by its unique identifier.
     *
     * @param id      unique identifier of the document to be modified
     * @param content the new content of the <code>Document</code> object identified by <code>id</code>
     */
    public void modifyDocContent(UUID id, String content) throws DocumentException {
        Document doc = documents.get(id);
        if (doc == null) throw new NotFoundException("Document", "Library", id);
        doc.changeContent(content);
    }

    /**
     * Deletes the <code>Document</code> object identified by the unique id <code>id</code>.
     *
     * @param id unique identifier of the document to be deleted
     */
    public void delDoc(UUID id) {
        if (!documents.containsKey(id)) throw new NotFoundException("Document", "Library", id);
        Document doc = documents.get(id);
        String title = doc.getTitle();
        String author = doc.getAuthor();
        documents.remove(id);

        if (authorTitleIndex.get(author).size() == 1) authorTitleIndex.remove(author);
        else authorTitleIndex.get(author).remove(title);
    }

    /**
     * Returns a list with all the documents in the library.
     *
     * @return all documents in the library
     */
    public Collection<Document> getDocs(){
        return documents.values();
    }

    /**
     * Returns a list with all the documents identifiers in the library.
     *
     * @return all documents identifiers in the library
     */
    public Collection<UUID> getIDs(){
        return documents.keySet();
    }

}

