package edu.upc.prop411.domain;

import edu.upc.prop411.domain.exceptions.critical.InvalidStateException;
import edu.upc.prop411.domain.exceptions.recoverable.DomainException;
import edu.upc.prop411.persistence.ExportFormat;
import edu.upc.prop411.persistence.PersistenceController;
import edu.upc.prop411.persistence.exceptions.PersistenceException;

import java.io.File;
import java.util.*;

/**
 * The DomainController is the class that connects the domain layer to the interface
 * and persistence layers.
 * <p>
 * It also acts as a controller for the other classes in the domain layer, providing
 * the methods to interact with documents, expressions and the search results.
 * @author Joan Farres Garcia
 */
public class DomainController {

    /**
     * The document library.
     */
    private final DocumentLibrary documentLibrary;

    /**
     * The finder.
     */
    private final Finder finder;

    /**
     * The expression library.
     */
    private final ExpressionLibrary expressionLibrary;

    /**
     * The search result.
     */
    private final SearchResult searchResult;

    /**
     * The persistence controller.
     */
    private final PersistenceController persistenceController;

    /**
     * Class Constructor.
     */
    public DomainController() {
        persistenceController = new PersistenceController();

        Object documentLibraryObject;
        Object indexObject;
        Object expressionLibraryObject;

        try {
            documentLibraryObject = persistenceController.loadDocumentLibrary();
            indexObject = persistenceController.loadIndex();
            expressionLibraryObject = persistenceController.loadExpressionLibrary();
        } catch (PersistenceException e) {
            throw new InvalidStateException(e.toString());
        }

        if (documentLibraryObject != null && indexObject != null && expressionLibraryObject != null) {
            documentLibrary = (DocumentLibrary) documentLibraryObject;
            expressionLibrary = (ExpressionLibrary) expressionLibraryObject;
            finder = new Finder(this, (Index) indexObject);
        } else if (documentLibraryObject == null && indexObject == null && expressionLibraryObject == null) {
            documentLibrary = new DocumentLibrary();
            finder = new Finder(this);
            expressionLibrary = new ExpressionLibrary();
        } else {
            throw new InvalidStateException("Can't find all state files");
        }

        searchResult = new SearchResult();
        resetSearch();
    }

    /**
     * Saves the state of the program.
     *
     * @throws InvalidStateException if some state files are found but not all or none.
     */
    public void saveState() throws InvalidStateException {
        try {
            persistenceController.saveDocumentLibrary(documentLibrary);
            persistenceController.saveIndex(finder.getIndex());
            persistenceController.saveExpressionLibrary(expressionLibrary);
        } catch (PersistenceException e) {
            throw new InvalidStateException(e.toString());
        }
    }

    /**
     * Reads files in paths and creates documents.
     * <p>
     * Recursively adds all files in folders. Resets default
     * search to all documents.
     *
     * @param files the list of files to import
     * @throws DomainException if path doesn't exist, isn't a file or folder, or there is an IO problem.
     */
    public int importDocuments(List<File> files) throws DomainException {
        int num_imported_docs = 0;

        for (File file : files) {
            if (file.isDirectory()) {
                try {
                    File[] subdir_files = file.listFiles();

                    if (subdir_files != null)
                        num_imported_docs += importDocuments(Arrays.asList(subdir_files));
                } catch (Exception e) {
                    resetSearch();
                    throw new DomainException(e.toString());
                }
            } else if (file.isFile()) {
                List<String> doc_fields;

                try {
                    doc_fields = persistenceController.importDocument(file);
                } catch (PersistenceException e) {
                    throw new DomainException(e.toString());
                }

                createDocument(doc_fields.get(0), doc_fields.get(1), doc_fields.get(2));
                num_imported_docs++;
            } else { // should never occur
                throw new DomainException("Path doesn't exist " + file.toString());
            }
            // don't manage special files (like symbolic links)
        }

        resetSearch();

        return num_imported_docs;
    }

    /**
     * Exports documents to the folder pointed by the path.
     *
     * @param docs the list of ids of the documents
     * @param file the destination folder of the exported files
     * @param format extension of the files to be exported
     * @throws DomainException if path isn't a directory
     */
    public int exportDocuments(List<UUID> docs, File file, ExportFormat format) throws DomainException {
        int num_exported_docs = 0;

        if (!file.isDirectory())
            throw new DomainException("Folder doesn't exist " + file.toString());

        for (UUID id : docs) {
            Document doc = documentLibrary.getWithId(id);
            List<String> fields = List.of(doc.getTitle(), doc.getAuthor(), doc.getContent());

            String normalized_title = doc.getTitle().replaceAll("[^\\w\\s]","");
            String normalized_author = doc.getAuthor().replaceAll("[^\\w\\s]","");
            String pathname = file.toString() + "/" + normalized_title + "__" + normalized_author;

            switch (format) {
                case TXT:
                    pathname += ".txt";
                    break;
                case XML:
                    pathname += ".xml";
                    break;
                default:
                    pathname += ".json";
                    break;
            }

            File doc_file = new File(pathname);

            try {
                persistenceController.exportDocument(fields, doc_file, format);
            } catch (PersistenceException e) {
                throw new DomainException(e.toString());
            }
            num_exported_docs++;
        }

        return num_exported_docs;
    }

    /**
     * Creates new document from the title, author and content.
     * <p>
     * Resets default search to all documents.
     *
     * @param title the title of the new document
     * @param author the author of the new document
     * @param content the content of the new document
     * @throws DomainException if parameters for the document are invalid
     */
    public void createDocument(String title, String author, String content) throws DomainException {
        try {
            UUID id = documentLibrary.createDocument(title, author, content);
            finder.addDoc(content, id);
        } finally {
            // We want to always reset the search to the default
            // search (all documents)
            resetSearch();
        }
    }

    /**
     * Returns the document given its id.
     *
     * @param id the id of the document
     * @return the document object
     */
    protected Document getDocumentWithId(UUID id) {
        return documentLibrary.getWithId(id);
    }

    /**
     * Modifies the author of a document given its id.
     *
     * @param id the id of the document
     * @param author the new author
     * @throws DomainException if new author is invalid
     */
    public void modifyDocAuthor(UUID id, String author) throws DomainException {
        try {
            documentLibrary.modifyDocAuthor(id, author);
        } finally {
            // We want to always reset the search to the default
            // search (all documents)
            resetSearch();
        }
    }

    /**
     *  Modifies the title of a document given its id.
     * <p>
     * Resets default search to all documents.
     *
     * @param id the id of the document
     * @param title the new title
     * @throws DomainException if new title is invalid
     */
    public void modifyDocTitle(UUID id, String title) throws DomainException {
        try {
            documentLibrary.modifyDocTitle(id, title);
        } finally {
            // We want to always reset the search to the default
            // search (all documents)
            resetSearch();
        }
    }

    /**
     * Modifies the content of a document given its id.
     * <p>
     * Resets default search to all documents.
     *
     * @param id the id of the document
     * @param content the new content
     * @throws DomainException if new content is invalid
     */
    public void modifyDocContent(UUID id, String content) throws DomainException {
        try {
            documentLibrary.modifyDocContent(id, content);
            finder.modifyDoc(content, id);
        } finally {
            // Modifying the content can change order of results
            // if sorted by length
            resetSearch();
        }
    }

    /**
     * Deletes a document given its id.
     * <p>
     * Resets default search to all documents.
     *
     * @param doc the id of the document
     */
    public void delDocument(UUID doc) {
        documentLibrary.delDoc(doc);
        finder.delDoc(doc);
        resetSearch();
    }

    /**
     * Gives the content of a document given its id.
     *
     * @param doc the id of the document
     * @return the content of the document
     */
    public String getDocContent(UUID doc) {
        return getDocumentWithId(doc).getContent();
    }

    /**
     * Returns the id of a document given its title and author.
     * <p>
     * Returns null if the author-title combination doesn't exist.
     *
     * @param author the author of the document
     * @param title the title of the document
     * @return the id of the corresponding document
     * @throws DomainException if the author doesn't exist
     */
    public UUID getDocWithAuthorTitle(String author, String title) throws DomainException {
        return documentLibrary.getWithAuthorTitle(author, title);
    }

    /**
     * Returns the author of a document given its id.
     *
     * @param doc the id of the document
     * @return the author of the document
     */
    public String getDocAuthor(UUID doc) {
        return getDocumentWithId(doc).getAuthor();
    }

    /**
     * Returns the title of a document given its id.
     *
     * @param doc the id of the document
     * @return the title of the document
     */
    public String getDocTitle(UUID doc) {
        return getDocumentWithId(doc).getTitle();
    }

    /**
     * Returns the list of ids of all the documents of an author.
     *
     * @param author the author
     * @return the list of ids of the documents
     * @throws DomainException if author doesn't exist
     */
    public Collection<UUID> getAuthorDocs(String author) throws DomainException {
        return documentLibrary.getAuthorDocs(author);
    }


    /**
     * Returns a list of all the authors whose names start with a prefix.
     *
     * @param prefix the string containing the prefix
     * @return the list of authors that start with the prefix
     */
    public Collection<String> getAuthors(String prefix) {
        return documentLibrary.getAuthors(prefix);
    }

    /**
     * Updates the search result with the most similar documents with respect
     * to a reference document.
     *
     * @param doc the id of the document to use as a reference
     * @param n the allowed maximum number of entries in the search result
     */
    public void searchBySimilarDoc(UUID doc, int n) {
        searchResult.setSearch(finder.getSimilarDocs(doc, n));
    }

    /**
     * Searches the most similar documents to a list of words.
     *
     * @param words the list of words
     * @param n the allowed maximum number of entries in the search result
     * @throws DomainException if word list is empty
     */
    public void searchByWordList(String words, int n) throws DomainException {
        searchResult.setSearch(finder.getDocsByWordList(words, n));
    }

    /**
     * Updates the search result with the documents that comply with an expression,
     * given the expression id.
     *
     * @param expr the id of the expression
     * @throws DomainException if expression is invalid
     */
    public void searchByExpr(UUID expr) throws DomainException {
        Expression expression = expressionLibrary.getExpression(expr);
        Set<UUID> set = finder.getDocsExpression(expression);
        sortSearch(new ArrayList<>(set));
    }

    /**
     * Updates the search result with the documents that comply with an expression,
     * given the text of the expression.
     *
     * @param texpr the id of the expression
     * @throws DomainException if expression is invalid
     */
    public void searchByExpr(String texpr) throws DomainException {
        Expression expression = expressionLibrary.getVolatileExpression(texpr);
        Set<UUID> set = finder.getDocsExpression(expression);
        sortSearch(new ArrayList<>(set));
    }

    /**
     * Resets search to all documents.
     */
    public void resetSearch() {
        List<UUID> list = new ArrayList<>(documentLibrary.getIDs());
        sortSearch(list);
    }

    /**
     * Sorts the input list and updates the search.
     * <p>
     * The current criterion will be used for the sort.
     *
     * @param docs the list of ids
     */
    private void sortSearch(List<UUID> docs) {
        SortCriterion criterion = searchResult.getSortCriterion();
        Comparator<UUID> comp;

        switch (criterion) {
            case TitleASC:
                comp = (lhs, rhs) -> {
                    Document left = getDocumentWithId(lhs);
                    Document right = getDocumentWithId(rhs);

                    return Integer.signum(left.getTitle().toLowerCase().compareTo(right.getTitle().toLowerCase()));
                };
                break;
            case TitleDSC:
                comp = (lhs, rhs) -> {
                    Document left, right;
                    left = getDocumentWithId(lhs);
                    right = getDocumentWithId(rhs);

                    return Integer.signum(right.getTitle().toLowerCase().compareTo(left.getTitle().toLowerCase()));
                };
                break;
            case AuthorASC:
                comp = (lhs, rhs) -> {
                    Document left = getDocumentWithId(lhs);
                    Document right = getDocumentWithId(rhs);

                    return Integer.signum(left.getAuthor().toLowerCase().compareTo(right.getAuthor().toLowerCase()));
                };
                break;
            case AuthorDSC:
                comp = (lhs, rhs) -> {
                    Document left = getDocumentWithId(lhs);
                    Document right = getDocumentWithId(rhs);

                    return Integer.signum(right.getAuthor().toLowerCase().compareTo(left.getAuthor().toLowerCase()));
                };
                break;
            case LengthASC:
                comp = (lhs, rhs) -> {
                    Document left = getDocumentWithId(lhs);
                    Document right = getDocumentWithId(rhs);

                    return (left.getSize() < right.getSize()) ? -1 : 1;
                };
                break;
            case LengthDSC:
            default:
                comp = (lhs, rhs) -> {
                    Document left = getDocumentWithId(lhs);
                    Document right = getDocumentWithId(rhs);

                    return (left.getSize() >= right.getSize()) ? -1 : 1;
                };
                break;
        }

        docs.sort(comp);
        searchResult.setSearch(docs);
    }

    /**
     * Changes the criterion of the search results.
     *
     * @param order the new criterion
     */
    public void changeOrder(SortCriterion order) {
        searchResult.changeOrder(order);
        sortSearch(searchResult.getSearch());
    }

    /**
     * Returns the last search.
     * <p>
     * If no previous search has been done, it returns
     * the default search (all documents).
     *
     * @return the list of UUIDs
     */
    public List<UUID> getSearch() {
        return searchResult.getSearch();
    }

    /**
     * Returns the number of entries in the last search.
     *
     * @return the number of entries
     */
    public int getSearchSize() {
        return searchResult.getSearchSize();
    }

    /**
     * Creates a new expression from the text.
     *
     * @param texpr the text of the expression
     * @throws DomainException if expression is invalid
     */
    public void createExpr(String texpr) throws DomainException {
        expressionLibrary.addExpression(texpr);
    }

    /**
     * Deletes an expression given its id and creates a new
     * expression from the text string.
     * <p>
     * It resets the search to the default search (all documents).
     *
     * @param texpr the text of the new expression
     * @param id the id of the old expression
     * @throws DomainException if expression is invalid
     */
    public void replaceExpr(String texpr, UUID id) throws DomainException {
        try {
            expressionLibrary.addExpression(texpr);
            expressionLibrary.delExpression(id);
        } finally {
            // reset search in case our last search
            // was with replaced expression
            resetSearch();
        }
    }

    /**
     * Deletes an expression given its id.
     * <p>
     * It resets the search to the default search (all documents).
     *
     * @param expr the id of the expression
     */
    public void delExpr(UUID expr) {
        expressionLibrary.delExpression(expr);
        resetSearch();
    }

    /**
     * Returns the text of an expression given its id.
     *
     * @param expr the id of the expression
     * @return the text of the expression
     */
    public String getExprText(UUID expr) {
        return expressionLibrary.getExpressionText(expr);
    }

    /**
     * Returns the ids of the expressions.
     *
     * @return the collection of uuids
     */
    public Collection<UUID> getExpressions() {
        return expressionLibrary.getExpressions();
    }
}