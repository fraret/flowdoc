package edu.upc.prop411.domain;

import edu.upc.prop411.domain.exceptions.critical.NotFoundException;

import java.io.Serializable;
import java.util.*;

import static java.lang.Math.log;

/**
 * Data cache for tfidf algorithms.
 *
 * @author Ferran Hermida Rivera
 */

public class Index implements Serializable {
    private final HashMap<UUID, HashMap<String, Integer>> tfid;
    private final HashMap<String, Set<UUID>> dwt;

    /**
     * Class constructor.
     */
    public Index(){
        tfid = new HashMap<UUID, HashMap<String, Integer>>();
        dwt = new HashMap<String, Set<UUID>>();

    }

    /**
     * Returns a list with the unique identifiers of all the <code>Document</code> objects which contents
     * contains the word <code>word</code>.
     *
     * @param word the word to look for in the documents content
     * @return     the identifiers of all documents which content contains <code>word</code>
     */
    public Set<UUID> docsWithWord(String word) {
        if (!dwt.containsKey(word)) return new HashSet<>();
        HashSet<UUID> docs = new HashSet<UUID>(dwt.get(word));
        return docs;
    }

    /**
     * Returns a list with the unique identifiers of all the <code>Document</code> objects which contents
     * contains all the words in <code>words</code>.
     *
     * @param words <code>Iterable</code> with all the words to look for in the documents contents
     * @return      a list with the unique identifiers of the documents which content contains all the
     *              words in <code>words</code>
     */
    public Set<UUID> docsWithWords(Iterable<String> words){
        HashSet<UUID> docs = new HashSet<>(tfid.keySet());
        for (String s: words){
            if (!dwt.containsKey(s)) return new HashSet<>();
            docs.retainAll(dwt.get(s));
        }
        return docs;
    }

    /**
     * Adds a TF (term frequency) of a word in a given <code>Document</code> object.
     * The elements in <code>TF</code> are respectively the word and its frequency in the
     * content of the <code>Document</code> object identified by <code>id</code>.
     *
     * @param TF HasMap which contains all (<code>word</code>,<code>frequency</code>) pairs for the document
     *           identified by <code>id</code>. T
     * @param id Identifier of the document where the word appearances have been counted
     */
    public void addTF(HashMap<String, Integer> TF, UUID id){
        assert !tfid.containsKey(id): "Index already has a TF for the document identified by "+id.toString()+".";
        for (String s: TF.keySet()){
            if (! dwt.containsKey(s)) {dwt.put(s, new HashSet<>());}
            dwt.get(s).add(id);
        }
        tfid.put(id, new HashMap<>(TF));
    }

    /**
     * Modifies the frequency appearance of a word in a document. <code>newTF</code> contains the
     * words of as key and its frequencies as the content. The words appear in the document identified by
     * <code>id</code> and the frequencies count how many times do thew words appear in that same document.
     * <p>
     * This method can be called once the document and its TF already exists, but its term frequency has been
     * modified, i.e, by the <code>modifyDocContent</code> method.
     *
     * @param newTF HasMap which contains all (<code>word</code>,<code>frequency</code>) pairs for the document
     *              identified by <code>id</code>
     * @param id    unique identifier of the <code>Document</code> object where the words have been counted
     */
    public void modifyTF(HashMap<String, Integer> newTF,  UUID id) {
        if (! tfid.containsKey(id)) {
            throw new NotFoundException("Document TF information", "Index", id);
        }
        delTF(id);
        addTF(newTF, id);
    }

    /**
     * Deletes the TF of an specified document.
     *
     * @param oldid Unique identifier of the document which TF has to be deleted.
     */
    public void delTF(UUID oldid) {
        if (! tfid.containsKey(oldid)) {
            throw new NotFoundException("Document TF information", "Index", oldid);
        }
        for (String s: tfid.get(oldid).keySet()){
            dwt.get(s).remove(oldid);
            if (dwt.get(s).isEmpty()) dwt.remove(s);
        }
        tfid.remove(oldid);
    }

    /**
     * Returns a map with the term frequency of all the words in the document identified by id. it returns
     * (word, frequency) pairs for every word in the document.
     *
     * @param id document identifier.
     * @return   a map with all the (word, frequency) pairs for the document specified.
     */
    public HashMap<String, Integer> getTF(UUID id) {
        if (! tfid.containsKey(id)) {
            throw new NotFoundException("Document TF information", "Index", id);
        }
        return new HashMap<String, Integer>(tfid.get(id));
    }

    /**
     * Returns a map with the inverse document frequency of each word appearing at least once in one or more
     * documents.
     *
     * @return   a map with all the (word, frequency) pairs for each word.
     */
    public HashMap<String, Double> getIDF(){
        HashMap<String, Double> IDF = new HashMap<>();
        Integer docsQuantity =  tfid.size();
        for (String s: dwt.keySet()) {
            IDF.put(s, log(docsQuantity.doubleValue()/ (double) dwt.get(s).size()));
        }
        return IDF;
    }

    /**
     * Returns the id of all the documents whose term frequency is computed
     *
     * @return   a set with the id of all documents whose term frequency is stored in index or null if no document
     *           has its term frequency computed.
     */
    public Set<UUID> getIDs(){
        return Collections.unmodifiableSet(tfid.keySet());
    }
}
