package edu.upc.prop411.domain;

import edu.upc.prop411.domain.exceptions.recoverable.ExpressionException;

import java.util.*;


/**
 * Class for parsing expressions
 * <p>
 * An instance is needed for parsing as some internal information is stored during the parsing process. You can reuse an
 * instance after it has finished parsing a supplied expression (successfully or unsuccessfully) without any explicit
 * cleanup. You should NOT use the same instance simultaneously in different threads.
 * <p>
 * An EBNF specification for the syntax it parses could be the following::
 * <pre>
 * expression = simple-expression
 *            | simple-expression, '&amp;', simple-expression
 *            | simple-expression, '|', simple-expression
 *            ;
 *
 * simple_expression = '(', expression, ')'
 *                   | '!', simple-expression
 *                   | WORD
 *                   | SUBSTRING
 *                   | word-list
 *                   ;
 *
 * word-list = '{' word, {word} '}' ;
 * </pre>
 *
 * WORD and SUBSTRING, as well as the terminal characters are all tokens defined and separated in the tokenizer.
 * <code>&amp;</code> has precedence over <code>|</code>
 */
class ExpressionParser {
    private List<ExpToken> tokenList;

    /**
     * Current parsing position.
     * Incremented each time a token is consumed.
     */
    private int parserPosition;

    public ExpressionParser() {}

    /**
     * Consumes tokens starting at the current position trying to parse a word-list.
     * The last token consumed should have been a '{' and the current position should be the token immediately after that
     * It will advance the parser position and leave it on the first token after the closing '}' if it succeeds, otherwise
     * it will throw an exception
     *
     * @return An ExpWordSet object representing the word-list
     * @throws ExpressionException if the expression ends before the word-list does, the word-list is empty or the word-list contains tokens that are not a WORD or '}'
     */
    private Expression parseWordList() throws ExpressionException {
        if (parserPosition == tokenList.size())
            throw new ExpressionException("Expression ended when expecting a word list element");

        Set<String> words = new HashSet<>();
        StringBuilder text = new StringBuilder("{");
        while (parserPosition < tokenList.size()) {
            ExpToken currTok = tokenList.get(parserPosition);
            if (currTok.getTokenType() == ExpToken.TokenType.CLOSING_CURLY_BRACE) {
                if (words.isEmpty())
                    throw new ExpressionException("Empty word list");
                text.append(" }");
                ++parserPosition;
                return new ExpWordSet(words, text.toString());
            } else if (currTok.getTokenType() == ExpToken.TokenType.WORD) {
                words.add(currTok.getInnerText());
                text.append(" ").append(currTok.getInnerText());
            } else {
                throw new ExpressionException(String.format("Unsupported token %s in word list", currTok));
            }
            ++parserPosition;
        }
        throw new ExpressionException("Expression ended in the middle of a word list");
    }

    /**
     * Consumes tokens starting at the current position trying to parse a simple-expression.
     * It will advance the parser position to the first token after the simple-expression if it succeeds in parsing,
     * it will throw an exception otherwise
     *
     * @return An Expression object representing the simple-expression
     * @throws ExpressionException if there are no more tokens or they cannot be parsed as a simple expression
     */
    private Expression parseSimpleExpression() throws ExpressionException {
        if (parserPosition == tokenList.size())
            throw new ExpressionException("Expression ended when expecting a boolean expression");
        ExpToken currTok = tokenList.get(parserPosition);
        ++parserPosition;
        switch (currTok.getTokenType()) {
            case WORD:
                return new ExpWordSet(Collections.singleton(currTok.getInnerText()), currTok.getText());
            case OPENING_CURLY_BRACE:
                return parseWordList();
            case SUBSTRING:
                return new ExpSubstr(currTok.getInnerText(), currTok.getText());
            case NOT_OPERATOR:
                Expression rhs = parseSimpleExpression();
                return new ExpNot(rhs, "!" + rhs);
            case OPENING_PARENTHESIS:
                Expression exp = parseExpression(false);
                String oldText = exp.toString();
                exp.setText("(" + oldText + ")");
                return exp;
            default:
                throw new ExpressionException(String.format("Unexpected token %s in boolean expression", currTok.getText()));
        }
    }

    /**
     * Given a deque containing expressions that are to be OR-ed, that is,
     * exp1 | exp2 | ... | expN, constructs an OR tree from said deque
     *
     * @param orDeque the deque with the expressions to construct the OR tree from. It will be destroyed.
     * @return a balanced OR tree that performs the OR of all the expressions given
     */
    private Expression constructOrTree(ArrayDeque<Expression> orDeque) {
        ArrayDeque<Expression> reducedDeque = new ArrayDeque<>();

        while (orDeque.size() > 1) {
            while (orDeque.size() > 1) {
                Expression rhs = orDeque.removeLast();
                Expression lhs = orDeque.removeLast();

                ExpBinOp joined = new ExpBinOp(ExpBinOp.BinaryOperationType.OR, lhs, rhs,
                        lhs + " | " + rhs);
                reducedDeque.addFirst(joined);
            }

            if (orDeque.size() > 0)
                reducedDeque.addFirst(orDeque.removeLast());

            assert orDeque.isEmpty();

            //Swap the dequeues
            ArrayDeque<Expression> aux;
            aux = orDeque;
            orDeque = reducedDeque;
            reducedDeque = aux;
        }
        return orDeque.getLast();
    }

    /**
     * Consumes tokens starting at the current position trying to parse multiple expressions until the end or a parenthesis
     * is found.
     * <p>
     * Depending on the value of topLevel it will try to parse an expression that starts and ends with a parenthesis
     * or not:
     * <p>
     * If topLevel is set to true it will keep consuming until the end of the token list, throwing an exception if a ')'
     * is found. Otherwise, it will consume until it finds the closing ')', leaving the parser position just after it.
     * <p>
     * This function handles the operator precedence using a stack for storing the expressions to be OR-ed at the end
     * and combining the top expression with the next one when an AND is found. Due to the way this is implemented,
     * this will mean that the resulting tree will be relatively balanced when multiple ORs are found but will be very
     * unbalanced when multiple ANDs are found.
     *
     * @return An Expression object representing the expression
     * @param topLevel whether the expression parsed should end with the end of the list or a parenthesis
     * @throws ExpressionException if there are no more tokens and we are not topLevel or they cannot be parsed as
     *                             an expression
     */
    private Expression parseExpression(boolean topLevel) throws ExpressionException {
        ArrayDeque<Expression> orDeque = new ArrayDeque<>();

        orDeque.addLast(parseSimpleExpression());
        while (parserPosition != tokenList.size()) {
            ExpToken currToken = tokenList.get(parserPosition);
            if (currToken.getTokenType() == ExpToken.TokenType.OR_OPERATOR) {
                ++parserPosition;
                orDeque.addLast(parseSimpleExpression());
            } else if (currToken.getTokenType() == ExpToken.TokenType.AND_OPERATOR) {
                Expression lhsExpresion = orDeque.removeLast();
                ++parserPosition;
                Expression rhsExpression = parseSimpleExpression();
                orDeque.addLast(new ExpBinOp(ExpBinOp.BinaryOperationType.AND, lhsExpresion, rhsExpression,
                        lhsExpresion + " & " + rhsExpression));
            } else if (currToken.getTokenType() == ExpToken.TokenType.CLOSING_PARENTHESIS) {
                if (topLevel) {
                    throw new ExpressionException("Found extra closing parenthesis in top-level expression");
                } else {
                    ++parserPosition;
                    if (orDeque.isEmpty())
                        throw new ExpressionException("Expression contains empty parentheses");
                    return constructOrTree(orDeque);
                }
            } else {
                throw new ExpressionException("Expression has boolean expressions not joined by an & or |");
            }
        }
        if (!topLevel)
            throw new ExpressionException("Expression ended before closing parentheses");
        return constructOrTree(orDeque);

    }

    /**
     * Parses a given tokenList into an AST
     *
     * @param tokenList the list of tokens to be parsed
     * @return The Expression object at the top (root) of the AST
     * @throws ExpressionException if the given tokenList is not a valid expression
     */
    public Expression parse(List<ExpToken> tokenList) throws ExpressionException {
        this.tokenList = tokenList;
        parserPosition = 0;
        return parseExpression(true);
    }
}
