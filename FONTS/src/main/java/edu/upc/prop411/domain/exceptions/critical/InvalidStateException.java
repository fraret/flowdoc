package edu.upc.prop411.domain.exceptions.critical;

public class InvalidStateException extends RuntimeException {
    public InvalidStateException(String str)  {
        super("Error while reading application state: " + str);
    }
}
