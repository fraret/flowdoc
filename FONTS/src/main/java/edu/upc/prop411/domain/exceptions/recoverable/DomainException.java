package edu.upc.prop411.domain.exceptions.recoverable;

public class DomainException extends Exception{
    public DomainException() {
        super();
    }

    public DomainException(String str) {
        super(str);
    }
}
