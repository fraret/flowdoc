package edu.upc.prop411.domain;

/**
 * Represents an operation with two operands which are themselves expressions
 *
 * @author Sergi Soler Arrufat
 */
class ExpBinOp extends Expression{

    /**
     * Binary operation type
     */
    public enum BinaryOperationType {
      AND,
      OR
    };

    /**
     * The binary operation type,
     */
    private final BinaryOperationType type;

    /**
     * The expression on the left side of the binary operator
     */
    private final Expression leftExpression;

    /**
     * The expression on the right side of the binary operator
     */
    private final Expression rightExpression;

    /**
     * Constructs a binary operation (and, or) expression
     *
     * @param type the expression type
     * @param lexp the expression on the left side of the binary operator
     * @param rexp the expression on the right side of the binary operator
     * @param text the text from which the current AST node was parsed
     */
    public ExpBinOp(BinaryOperationType type, Expression lexp, Expression rexp, String text) {
        super(text);

        assert (lexp != null && rexp != null) : "Expressions used to create a ExpNot expression must not be null";

        leftExpression = lexp;
        rightExpression = rexp;
        this.type = type;
    }

    /**
     * Returns the type of binary operation
     *
     * @return the type of binary operation
     */
    public BinaryOperationType getType() {
        return type;
    }

    /**
     * Returns the expression on the left side of the binary operator
     * @return the expression on the left side of the binary operator
     */
    public Expression getLeftExpression() {
        return leftExpression;
    }


    /**
     * Returns the expression on the right side of the binary operator
     * @return the expression on the right side of the binary operator
     */
    public Expression getRightExpression() {
        return rightExpression;
    }
}
