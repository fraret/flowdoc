package edu.upc.prop411.persistence;

import java.util.ArrayList;
import java.util.Scanner;

import edu.upc.prop411.persistence.exceptions.XMLFormatNotCorrectException;

class XMLParser {
    private final Scanner sc;

    /**
     * default constructor with the given scanner
     * @param scanner scanner used to by the new parser
     */
    public XMLParser(Scanner scanner){
        sc = scanner;
        sc.useDelimiter("");
    }

    /**
     * Consumes white spaces before another character. It includes spaces, tabs, new line or carriage return, next page and next line.
     * @return Returns first character different to a whitespace.
     * @throws XMLFormatNotCorrectException if there are no character different to whitespaces until the end of the file.
     */
    private String consumeWhitespace() throws XMLFormatNotCorrectException {
        String c;
        while (sc.hasNext()) {
            c = sc.next();
            if (!c.isBlank()) return c;
        }
        throw new XMLFormatNotCorrectException("Check expected input format.");
    }

    /**
     * Returns next XML label found. It assumes that all previous XML labels have been parsed passed the closing label.
     * @return next valid XML label's value.
     * @throws XMLFormatNotCorrectException if no valid label is found.
     */
    private String parseLabel() throws XMLFormatNotCorrectException{
        StringBuilder label = new StringBuilder();
        String c = consumeWhitespace();
        if (!c.equals("<")) throw new XMLFormatNotCorrectException("Expecting one of this three labels: <title>, <author> & <content>.");
        while(sc.hasNext() & !c.equals(">")){
            c = sc.next();
            if(!c.equals(">")) label.append(c);

        }
        if (!c.equals(">")) throw new XMLFormatNotCorrectException("Expecting one of this three labels: <title>, <author> & <content>.");
        return label.toString();
    }

    /**
     * Converts escaped XML characters to its String representation.
     * @param scapeChar string with an XML escaped character.
     * @return String representation of an XML escaped character.
     * @throws XMLFormatNotCorrectException if parameter is not an XML escaped character.
     */
    private String convertEscapeChar(String scapeChar) throws XMLFormatNotCorrectException {
        switch (scapeChar) {
            case "&lt;":
            case "&#60;":
                return "<";
            case "&gt;":
            case "&#62;":
                return ">";
            case "&quot;":
            case "&#34;":
                return "\"";
            case "&apos;":
            case "&#39;":
                return  "'";
            case "&amp;":
            case "&#38;":
                return "&";
            default:
                throw new XMLFormatNotCorrectException("& character is used to scape characters or has to be scaped.");
        }
    }

    /**
     * Returns true if the input parameter is a character that should be escaped in XML, false otherwise.
     * @param c input character to check
     * @return true if the input parameter is a character that should be escaped, false otherwise.
     */
    private boolean isSpecialChar(String c){
        return (c.equals(">") | c.equals("<") | c.equals("&") | c.equals("\"") | c.equals("'") );
    }

    /**
     * Parses XML escaped characters. It assumes that the & has already been parsed and that next characters are the ones remaining to complete the escaped character.
     * @param c Escaping xml character, '&'.
     * @return the text representation of the escaped character.
     * @throws XMLFormatNotCorrectException if the string found does not represent any escaped character in XMÑ.
     */
    private String parseEscapeChar(String c) throws XMLFormatNotCorrectException {
        StringBuilder scapeChar = new StringBuilder(c);
        boolean endScapeChar = false;
        while (sc.hasNext() & ! endScapeChar){
            scapeChar.append(sc.next());
            if (scapeChar.toString().endsWith(";")) endScapeChar = true;
        }
        return convertEscapeChar(scapeChar.toString());
    }

    /**
     * Parses the content it finds until the closing of label. It expects the content to be correctly formatted text
     * in XML syntax.
     * @param label Value of the label whose value is being parsed.
     * @return The content parsed.
     * @throws XMLFormatNotCorrectException If content's format is not as expected.
     */
    private String parseUntilLabel(String label) throws XMLFormatNotCorrectException{
        StringBuilder content = new StringBuilder();
        String c = "";
        boolean closing = false;
        while(sc.hasNext() & !closing){
            c = sc.next();
            if (c.equals("<")) closing = true;
            else if (c.equals("&")) content.append(parseEscapeChar(c));
            else if (isSpecialChar(c)) throw new XMLFormatNotCorrectException("Special characters in xml must be e  scaped.");
            else content.append(c);
        }
        if (closing){
            StringBuilder closeLabel = new StringBuilder();
            while(sc.hasNext() & !c.equals(">")){
                c = sc.next();
                if(!c.equals(">")) closeLabel.append(c);
            }
            if (!c.equals(">")) throw new XMLFormatNotCorrectException("label "+label+" must be closed. Check expected format.");
            else if (!closeLabel.toString().equals("/"+label)) throw new XMLFormatNotCorrectException("Close label "+label+" before opening or closing a new one. Check expected format.");
        }
        return content.toString();
    }

    /**
     * Parse an XML file in a specific format.
     * @return A list with the three parameters that conform the XML format; title, author and content (in this same order).
     * @throws XMLFormatNotCorrectException if there is some syntax or format error in the XML file being parsed.
     */
    public ArrayList<String> parse() throws XMLFormatNotCorrectException {
        ArrayList<String> doc = new ArrayList<>();
        String header;
        String root;
        String label;
        String author = "";
        String title = "";
        String content = "";

        //Parse xml header, which is required in the format specified.
        header = parseLabel();
        if (!header.matches("^\\?xml version=\"[+-]?([0-9]*[.])?[0-9]+\" encoding=\"UTF-8\"\\?$")) {
            throw new XMLFormatNotCorrectException("Expecting a valid xml header using encoding UTF-8.");
        }

        //Parse xml root
        root = parseLabel();

        //Parse the expected xml fields, title, author and content
        for (int i = 0; i < 3; i++) {
            label = parseLabel();
            if (label.equals("title") & title.isEmpty()) {
                title = parseUntilLabel(label);
            } else if (label.equals("author") & author.isEmpty()) author = parseUntilLabel(label);
            else if (label.equals("content") & content.isEmpty()) content = parseUntilLabel(label);
            else throw new XMLFormatNotCorrectException("Use only this three labels: <title>, <author> and <content>.");
        }

        //Parse close root
        if (!((parseLabel()).equals("/"+root))) throw new XMLFormatNotCorrectException("Use only this three labels: <title>, <author> and <content>. root label must be closed.");
        sc.close();
        doc.add(title);
        doc.add(author);
        doc.add(content);
        return doc;
    }
}

