package edu.upc.prop411.persistence.exceptions;

public class JSONFormatNotCorrectException extends PersistenceException{

    public JSONFormatNotCorrectException (String s){
        super(String.format(".json format not correct. %s", s));
    }
}
