package edu.upc.prop411.persistence.exceptions;

public class TXTFormatNotCorrectException extends PersistenceException{
    public TXTFormatNotCorrectException(){
        super(String.format("xml format was not correct. Check expected format."));
    }
}
