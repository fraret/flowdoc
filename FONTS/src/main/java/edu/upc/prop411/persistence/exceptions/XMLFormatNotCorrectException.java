package edu.upc.prop411.persistence.exceptions;

public class XMLFormatNotCorrectException extends PersistenceException{
    public XMLFormatNotCorrectException(String s){
        super(String.format("xml format was not correct. %s", s));
    }
}
