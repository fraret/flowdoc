package edu.upc.prop411.persistence.exceptions;

public class NotAccessibleStateException extends PersistenceException{
    public NotAccessibleStateException(String object) {
        super(String.format("Could not load %s state", object));
    }
}
