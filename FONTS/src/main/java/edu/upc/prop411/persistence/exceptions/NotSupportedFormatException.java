package edu.upc.prop411.persistence.exceptions;

public class NotSupportedFormatException extends PersistenceException{
    public NotSupportedFormatException(String format) {
        super(String.format("Format %s not supported. Please, use TXT, XML or JSON to export.", format));
    }
    public NotSupportedFormatException() {
        super(String.format("Format not supported. Please, use TXT, XML or JSON to import."));
    }
}