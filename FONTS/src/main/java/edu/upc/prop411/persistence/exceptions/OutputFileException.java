package edu.upc.prop411.persistence.exceptions;

public class OutputFileException extends PersistenceException{
    public OutputFileException(String file) {
        super(String.format("Could not open or write %s file", file));
    }
}