package edu.upc.prop411.persistence.exceptions;


public class PersistenceException extends Exception{
    public PersistenceException(String str) {
        super(str);
    }
}
