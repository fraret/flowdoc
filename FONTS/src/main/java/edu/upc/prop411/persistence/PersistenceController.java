package edu.upc.prop411.persistence;

import edu.upc.prop411.domain.DocumentLibrary;
import edu.upc.prop411.domain.ExpressionLibrary;
import edu.upc.prop411.domain.Index;
import edu.upc.prop411.persistence.exceptions.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;


/**
 * Persistence layer controller
 */
public class PersistenceController {

    /**
     * Returns the three fields, title, author and content, parsed from a txt file.
     * @param sc Scanner of the txt file to be parsed.
     * @return three fields, title, author and content (in this same order)
     * @throws PersistenceException if the txt file it is not in the correct format.
     */
    private ArrayList<String> importTxt(Scanner sc) throws PersistenceException{
        ArrayList<String> doc = new ArrayList<>();
        StringBuilder content;
        try{
            doc.add(sc.nextLine());
            doc.add(sc.nextLine());
            content = new StringBuilder(sc.nextLine());
        } catch (NoSuchElementException e){
            throw new TXTFormatNotCorrectException();
        }
        while (sc.hasNext()){
            content.append("\n").append(sc.nextLine());
        }
        doc.add(content.toString());
        return doc;
    }

    /**
     * Imports the content of a JSON file. It expects an specific format.
     * @param sc Scanner of the JSON file to be parsed.
     * @return Three fields (title, author and content, in this same order) parsed from the JSON file.
     * @throws JSONFormatNotCorrectException if JSON format or syntax is not correct.
     */
    private ArrayList<String> importJSON(Scanner sc) throws JSONFormatNotCorrectException {
        JSONParser parser = new JSONParser(sc);
        return parser.parse();
    }

    /**
     * Imports the content of a XML file. It expects an specific format.
     * @param sc Scanner of the XML file to be parsed.
     * @return Three fields (title, author and content, in this same order) parsed from the JSON file.
     * @throws XMLFormatNotCorrectException if XML format or syntax is not correct.
     */
    private ArrayList<String> importXML(Scanner sc) throws XMLFormatNotCorrectException {
        XMLParser parser = new XMLParser(sc);
        return parser.parse();
    }

    /**
     * Returns three strings, which are title, author and content (in this same order) found in the file passed to the function.
     * @param file path specix
     * @return A list containing the title, author and content (in this order) found in file.
     * @throws PersistenceException if file format is not the one expected.
     */
    public List<String> importDocument(File file) throws PersistenceException {
        Scanner sc;
        try {
            Path path = file.toPath();
            ArrayList<String> doc;
            String stringPath = path.toString();
            sc = new Scanner(file, StandardCharsets.UTF_8);

            if (stringPath.endsWith(".txt")) doc = importTxt(sc);
            else if (stringPath.endsWith(".xml")) doc = importXML(sc);
            else if (stringPath.endsWith(".json")) doc = importJSON(sc);
            else throw new NotSupportedFormatException();
            sc.close();
            return doc;
        } catch (FileNotFoundException e){
            throw new PersistenceException("File not found.");
        } catch (IOException e) {
            throw new PersistenceException("Permission denied.");
        }
    }

    /**
     * Returns the escaped JSON representation of the character parameter.
     * @param c Character to escape in JSON.
     * @return The escaped representation of the character passed by parameter. If it is not an Escapable character in JSON, it returns the same character.
     */
    private String formatSpecialCharJSON(char c){
        switch (c) {
            case '\"':
                return "\\\"";
            case '\\':
                return "\\\\";
            case '/':
                return "\\/";
            case '\b':
                return "\\b";
            case '\f':
                return "\\f";
            case '\n':
                return "\\n";
            case '\r':
                return "\\r";
            case '\t':
                return "\\t";
            default:
                return String.valueOf(c);
        }
    }

    /**
     * Returns the escaped representation of an String.
     * @param content String to represent with JSON escapement.
     * @return the escaped representation of an String.
     */
    private String escapeJSONSpecialChars(String content){
        StringBuilder escapedContent = new StringBuilder();
        for (int i = 0; i < content.length(); i++){
            escapedContent.append(formatSpecialCharJSON(content.charAt(i)));
        }
        return escapedContent.toString();
    }

    /**
     * Returns the escaped XML representation of the character parameter.
     * @param c Character to escape in XML.
     * @return The escaped representation of the character passed by parameter. If it is not an Escapable character in XML, it returns the same character.
     */
    private String formatSpecialCharXML(char c){
        switch (c) {
            case '<':
                return "&lt;";
            case '>':
                return "&gt;";
            case '&':
                return "&amp;";
            case '\'':
                return "&apos;";
            case '\"':
                return "&quot;";
            default:
                return String.valueOf(c);
        }
    }

    /**
     * Returns the escaped representation of an String.
     * @param content String to represent with XML escapement.
     * @return the escaped representation of an String.
     */
    private String escapeXMLSpecialChars(String content){
        StringBuilder escapedContent = new StringBuilder();
        for (int i = 0; i < content.length(); i++){
            escapedContent.append(formatSpecialCharXML(content.charAt(i)));
        }
        return escapedContent.toString();
    }

    /**
     * Exports the content in doc to disk in a specific format, correspondent to the extension specified in format. doc's
     * fields are interpreted as title, author and content, in this same order. If doc hs more than three fields, the ones
     * remaining are ignored.
     * @param doc contains tha title, author and content to export.
     * @param file path to the file where doc has to be exported.
     * @param format extension (which has a correspondent format) of exporting.
     * @throws PersistenceException If extension specified is not supported.
     */
    public void exportDocument(List<String> doc, File file, ExportFormat format) throws PersistenceException{
        try (FileWriter fileWriter = new FileWriter(file, StandardCharsets.UTF_8);
             BufferedWriter buffWriter = new BufferedWriter(fileWriter)) {
            if (format == ExportFormat.TXT){
                buffWriter.write("");
                buffWriter.append(doc.get(0)).append("\n");
                buffWriter.append(doc.get(1)).append("\n");
                buffWriter.append(doc.get(2));
            }
            else if (format == ExportFormat.XML){
                buffWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                buffWriter.write("<document>\n");
                buffWriter.append("\t" + "<title>").append(escapeXMLSpecialChars(doc.get(0))).append("</title>\n"); //Title and author can not contain special chars.
                buffWriter.append("\t" + "<author>").append(escapeXMLSpecialChars(doc.get(1))).append("</author>\n");
                buffWriter.append("\t" + "<content>").append(escapeXMLSpecialChars(doc.get(2))).append("</content>\n");
                buffWriter.write("</document>");
            }
            else { // IS JSON format
                buffWriter.write("{\n");
                buffWriter.append("\"title\": " + "\"").append(escapeJSONSpecialChars(doc.get(0))).append("\",\n");
                buffWriter.append("\"author\": " + "\"").append(escapeJSONSpecialChars(doc.get(1))).append("\",\n");
                buffWriter.append("\"content\": " + "\"").append(escapeJSONSpecialChars(doc.get(2))).append("\"\n");
                buffWriter.write("}");
            }
        }
        catch (IOException e) {
            throw new OutputFileException(file.toString());}


    }

    /**
     * Serializes an Index object. It saves the serialization in the Index.jsef file.
     *
     * @param index object to serialize and save to disk.
     */
    public void saveIndex(Serializable index) throws OutputFileException{
        try {
            FileOutputStream fos = new FileOutputStream("index.jsef");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(index);
            oos.close();
        } catch (IOException e) {
            throw new OutputFileException("index.jsef");}
    }

    /**
     * Serializes a DocumentLibrary object. It saves the serialization in the documentLibrary.jsef file.
     *
     * @param documentLibrary object to serialize and save to disk.
     */
    public void saveDocumentLibrary(Serializable documentLibrary) throws OutputFileException{
        try {
            FileOutputStream fos = new FileOutputStream("documentLibrary.jsef");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(documentLibrary);
            oos.close();
        } catch (IOException e) {
            throw new OutputFileException("documentLibrary.jsef");}
    }

    /**
     * Serializes an ExpressionLibrary object. It saves the serialization in the expressionLibrary.jsef file.
     *
     * @param el object to serialize and save to disk.
     */
    public void saveExpressionLibrary(Serializable el) throws OutputFileException{
        try {
            FileOutputStream fos = new FileOutputStream("expressionLibrary.jsef");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(el);
            oos.close();
        } catch (IOException e) {
            throw new OutputFileException("expressionLibrary.jsef");}
    }

    /**
     * Returns the <code>Index</code> that was stored serialized in the index.jsef file.
     *
     * @return   the Index stored in index.jsef file or null if the file does not exist
     */
    public Index loadIndex() throws NotAccessibleStateException {
        try {
            FileInputStream fis = new FileInputStream("index.jsef");
            ObjectInputStream ois = new ObjectInputStream(fis);
            return (Index) ois.readObject();
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            throw new NotAccessibleStateException("Index");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the <code>DocumentLibrary</code> that was stored serialized in the documentLibrary.jsef file.
     *
     * @return   the DocumentLibrary stored in documentLibrary.jsef file or null if the file does not exist
     */
    public DocumentLibrary loadDocumentLibrary() throws NotAccessibleStateException {
        try {
            FileInputStream fis = new FileInputStream("documentLibrary.jsef");
            ObjectInputStream ois = new ObjectInputStream(fis);
            return (DocumentLibrary) ois.readObject();
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            throw new NotAccessibleStateException("Document Library");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the <code>ExpressionLibrary</code> that was stored serialized in the expressionLibrary.jsef file.
     *
     * @return   the ExpressionLibrary stored in expressionLibrary.jsef file, or null if the file does not exist
     */
    public ExpressionLibrary loadExpressionLibrary() throws NotAccessibleStateException {
        try {
            FileInputStream fis = new FileInputStream("expressionLibrary.jsef");
            ObjectInputStream ois = new ObjectInputStream(fis);
            return (ExpressionLibrary) ois.readObject();
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            throw new NotAccessibleStateException("Expresssion Library");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
