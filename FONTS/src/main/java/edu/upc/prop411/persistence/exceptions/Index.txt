El directori conte el codi de les excepcions de la capa de persistencia
-JSONFormatNotCorrectException: excepcio de format incorrecte
-NotAccessibleStateException: excepcio d'estat inaccessible
-NotSupportedFormatException: excepcio de format no suportat
-OutputFileException: excepcio de problema en el fitxer de sortida
-PersistenceException: excepcio de la capa de persistencia
-TXTFormatNotCorrectException: excepcio de format de .txt incorrecte
-XMLFormatNotCorrectException: excepcio de format de .xml incorrecte

