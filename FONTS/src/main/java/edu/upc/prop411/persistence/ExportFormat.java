package edu.upc.prop411.persistence;

/**
 * Supported file formats
 */
public enum ExportFormat {
    TXT,
    XML,
    JSON
}
