package edu.upc.prop411.persistence;

import edu.upc.prop411.persistence.exceptions.JSONFormatNotCorrectException;

import java.util.ArrayList;
import java.util.Scanner;


public class JSONParser {

    private final Scanner sc;

    /**
     * default constructor with the given scanner
     * @param sc scanner used to by the new parser
     */
    public JSONParser(Scanner sc){
        this.sc = sc;
        this.sc.useDelimiter("");
    }

    /**
     * Returns if the character passed by parameter is a special character (has to be escaped) in JSON syntax or not.
     * @param c one character string with the character to check
     * @return true if c is a special character in JSON.
     */
    private boolean isControlChar(String c){
        return (c.equals("/") | c.equals("\b") | c.equals("\f") | c.equals("\n") | c.equals("\t") | c.equals("\r"));
    }

    /**
     * Returns String representation of next JSON escaped char. It assumes tht next character to be scanned is the beginning of an escaped JSON character.
     * @return String representation of next JSON escaped character.
     * @throws JSONFormatNotCorrectException if no JSON escaped character is found.
     */
    private String nextEscapeChar() throws JSONFormatNotCorrectException {
        if (!sc.hasNext()) throw new JSONFormatNotCorrectException("Check expected input format.");
        String c = sc.next();
        switch (c) {
            case "\"":
                return "\"";
            case "\\":
                return "\\";
            case "/":
                return "/";
            case "b":
                return "\b";
            case "f":
                return "\f";
            case "n":
                return "\n";
            case "t":
                return "\t";
            case "r":
                return "\r";
            default:
                throw new JSONFormatNotCorrectException("Make sure to escape \\ character.");
        }
    }

    /**
     * Consumes all whitespace in the stream and returns the first non-whitespace character
     * @return A string containing the first non-whitespace character found
     * @throws JSONFormatNotCorrectException If the stream ends before whitespace does
     */
    private String consumeWhitespace() throws JSONFormatNotCorrectException {
        String c;
        while (sc.hasNext()) {
            c = sc.next();
            if (!c.isBlank()) return c;
        }
        throw new JSONFormatNotCorrectException("Check expected input format.");
    }

    /**
     * Parse next String value, which has to be quoted.
     * @return Next String value.
     * @throws JSONFormatNotCorrectException if no correct String value is found.
     */
    private String parseString() throws JSONFormatNotCorrectException {
        StringBuilder value = new StringBuilder();
        String c = consumeWhitespace();
        if (!c.equals("\"")) throw new JSONFormatNotCorrectException("Check expected input format.");
        while (sc.hasNext() & !(c = sc.next()).equals("\"")){
            if (c.equals("\\")){
                value.append(nextEscapeChar());
            }
            else if (isControlChar(c))throw new JSONFormatNotCorrectException("\\, \" and control characters must be escaped.");
            else value.append(c);
        }
        if (!c.equals("\"")) throw new JSONFormatNotCorrectException("Check expected input format.");
        return value.toString();
    }

    /**
     * Returns next JSON name/value pair name.
     * @return next JSON name.
     * @throws JSONFormatNotCorrectException if name/value pair name format is not correct.
     */
    private String parseTag() throws JSONFormatNotCorrectException {
        return parseString();
    }

    /**
     * Returns a JSON name/value pair string value. It assumes that the name in name/value has just been read correctly.
     * @return string value of a JSON object.
     * @throws JSONFormatNotCorrectException if value string format is not correct.
     */
    public String parseValue() throws JSONFormatNotCorrectException {
        String c = consumeWhitespace();
        if (!c.equals(":")) throw new JSONFormatNotCorrectException("Value and name have to be separated by \\s:\\s.");
        return parseString();
    }

    /**
     * Parses a JSON object's opening key.
     * @throws JSONFormatNotCorrectException if the object's opening key is found.
     */
    private void parseOpening() throws JSONFormatNotCorrectException {
        String c = consumeWhitespace();
        if (!c.equals("{")) throw new JSONFormatNotCorrectException("Objects must be opened with {.");
    }

    /**
     * Parses the separator used to separate name form value in a name/value JSON pair.
     * @throws JSONFormatNotCorrectException if separator is not found or its format is incorrect.
     */
    private void parseSeparator() throws JSONFormatNotCorrectException {
        String c = consumeWhitespace();
        if (!c.equals(",")) throw new JSONFormatNotCorrectException("Name/value pairs have to be separated by \\s,\\s.");
    }

    /**
     * Parses a JSON object's closing key.
     * @throws JSONFormatNotCorrectException if the object's closing key is found.
     */
    private void parseClosing() throws JSONFormatNotCorrectException {
        String c = consumeWhitespace();
        if (!c.equals("}")) throw new JSONFormatNotCorrectException("Objects must be closed with }.");
    }

    /**
     * Parses a JSON file in an specific format.
     * @return A list with the three parameters that conform the JSON format; title, author and content (in this same order).
     * @throws JSONFormatNotCorrectException f there is some syntax or format error in the JSON file being parsed.
     */
    public ArrayList<String> parse() throws JSONFormatNotCorrectException {
        ArrayList<String> doc = new ArrayList<>();
        String tag;
        String title = "";
        String author = "";
        String content = "";
        parseOpening();
        for (int i = 0; i < 3; i++){
            tag = parseTag();
            if (tag.equals("title") & title.isEmpty()) title = parseValue();
            else if (tag.equals("author") & author.isEmpty()) author = parseValue();
            else if (tag.equals("content") & content.isEmpty()) content = parseValue();
            else throw new JSONFormatNotCorrectException("Check expected input format.");
            if (i < 2) parseSeparator();
            else parseClosing();
        }

        sc.close();
        doc.add(title);
        doc.add(author);
        doc.add(content);
        return doc;
    }
}
