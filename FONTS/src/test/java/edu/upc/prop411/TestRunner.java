package edu.upc.prop411;

import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

import java.io.PrintWriter;

import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;

public class TestRunner {

    public static void runClassTests(Class<?> clazz) {
        SummaryGeneratingListener listener;
        listener = new SummaryGeneratingListener();
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(selectClass(clazz))
                .build();
        Launcher launcher = LauncherFactory.create();
        launcher.registerTestExecutionListeners(listener);
        launcher.execute(request);
        TestExecutionSummary summary = listener.getSummary();
        PrintWriter output = new PrintWriter(System.out);
        if (summary.getTotalFailureCount() > 0) {
            System.out.println("There have been some failures in the tests:");
            summary.printFailuresTo(output);
        }
        System.out.println("Executed " + summary.getTestsStartedCount() + " tests");
        System.out.printf("%4d PASSED\n", summary.getTestsSucceededCount());
        System.out.printf("%4d FAILED\n", summary.getTotalFailureCount());

        if (summary.getTotalFailureCount() > 0) {
            System.out.println("There were some failures, see above for more details");
        } else {
            System.out.println("All the tests succeeded");
        }

    }
}