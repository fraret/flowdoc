package edu.upc.prop411.domain;

import edu.upc.prop411.TestRunner;
import edu.upc.prop411.domain.exceptions.recoverable.ExpressionException;
import org.junit.jupiter.api.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class ExpTokenTests {



    static ExpToken simpTok(String str) {
        if (str.equals("{"))
            return new ExpToken(ExpToken.TokenType.OPENING_CURLY_BRACE, str);
        else if (str.equals("}"))
            return new ExpToken(ExpToken.TokenType.CLOSING_CURLY_BRACE, str);
        else if (str.equals("("))
            return new ExpToken(ExpToken.TokenType.OPENING_PARENTHESIS, str);
        else if (str.equals(")"))
            return new ExpToken(ExpToken.TokenType.CLOSING_PARENTHESIS, str);
        else if (str.equals("&"))
            return new ExpToken(ExpToken.TokenType.AND_OPERATOR, str);
        else if (str.equals("|"))
            return new ExpToken(ExpToken.TokenType.OR_OPERATOR, str);
        else if (str.equals("!"))
            return new ExpToken(ExpToken.TokenType.NOT_OPERATOR, str);
        else if (str.charAt(0) == '"')
            return new ExpToken(ExpToken.TokenType.SUBSTRING, str, str.substring(1, str.length()-1));
        else
            return new ExpToken(ExpToken.TokenType.WORD, str);
    }

    private void testTokenizer(String str, Iterable<ExpToken> tokenList) throws ExpressionException {
        List<ExpToken> obtainedList = ExpToken.tokenize(str);
        assertIterableEquals(tokenList, obtainedList);
    }

    @Test
    void singleCharTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("c").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("c");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void multiCharTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("casa").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("casa");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void orTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("c", "|", "da").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("c|da");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void andTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("c", "&", "da").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("c&da");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void notTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("!", "da").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("!da");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void parTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("(", "da", ")").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("(da)");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void curBracesTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("{", "da", "}").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("{da}");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void oneSpaceTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("a", "b").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("a b");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void multSpaceTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("a", "b").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("a        b");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void unssuportedSpaceTest(){
        assertThrows(ExpressionException.class, () ->  {ExpToken.tokenize("a    \t   b");});
    }

    @Test
    void unssuportedPunctuationTest() {
        assertThrows(ExpressionException.class, () ->  {ExpToken.tokenize("a,b");});
    }

    @Test
    void substrTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("\"abc\"").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("\"abc\"");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void substrAndSpacesTest() throws ExpressionException {
        Iterable<ExpToken> expected = Stream.of("\"abc \"", "\"cd?e\"", "|", "dfg", "\"f\"", "\"\"").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        List<ExpToken> obtainedList = ExpToken.tokenize("\"abc \"\"cd?e\"  | dfg\"f\" \"\"");
        assertIterableEquals(expected, obtainedList);
    }

    @Test
    void unclosedSubstrTest() {
        //List<ExpToken> obtainedList = ExpToken.tokenize("\"");
        assertThrows(ExpressionException.class, () ->  {ExpToken.tokenize("\"");});
    }

    public static void main(String[] args) {
        TestRunner.runClassTests(ExpTokenTests.class);
    }
}
