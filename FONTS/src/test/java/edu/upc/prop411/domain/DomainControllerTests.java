package edu.upc.prop411.domain;

import edu.upc.prop411.TestRunner;
import edu.upc.prop411.domain.exceptions.critical.NotFoundException;
import edu.upc.prop411.domain.exceptions.critical.TermFrequencyFinderException;
import edu.upc.prop411.domain.exceptions.critical.UUIDGenerationException;
import edu.upc.prop411.domain.exceptions.recoverable.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class DomainControllerTests {
    DomainController domainController;
    Finder finderMock = mock(Finder.class);
    DocumentLibrary documentLibraryMock = mock(DocumentLibrary.class);
    SearchResult searchResultMock = mock(SearchResult.class);
    ExpressionLibrary expressionLibraryMock = mock(ExpressionLibrary.class);

    @BeforeEach
    void setUp() throws DomainException {
        domainController = new DomainController();
        try {
            // assign classes to mocks
            Field finderField = DomainController.class.getDeclaredField("finder");
            Field documentLibraryField = DomainController.class.getDeclaredField("documentLibrary");
            Field searchResultField = DomainController.class.getDeclaredField("searchResult");
            Field expressionLibraryField = DomainController.class.getDeclaredField("expressionLibrary");

            finderField.setAccessible(true);
            finderField.set(domainController, finderMock);

            documentLibraryField.setAccessible(true);
            documentLibraryField.set(domainController, documentLibraryMock);

            searchResultField.setAccessible(true);
            searchResultField.set(domainController, searchResultMock);

            expressionLibraryField.setAccessible(true);
            expressionLibraryField.set(domainController, expressionLibraryMock);
        } catch (Exception e) {
            fail("Can't find DomainController field");
        }
    }

    @Test
    void testCreateInvalidDocument() throws DocumentException, AuthorTitleAlreadyExistsException, TermFrequencyFinderException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();
        when(documentLibraryMock.createDocument("", "", "")).thenThrow(DocumentException.class);
        when(documentLibraryMock.createDocument("a", "b", "")).thenThrow(DocumentException.class);
        doThrow(TermFrequencyFinderException.class).when(finderMock).addDoc("", id);

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        assertThrows(DocumentException.class, () -> domainController.createDocument("", "", ""));
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());

        assertThrows(DocumentException.class, () -> domainController.createDocument("a", "b", ""));
        verify(searchResultMock, times(2)).setSearch(new ArrayList<>());
    }

    @Test
    void testCreateValidDocument() throws DomainException {
        // setup mocks for current test
        UUID expectedId = UUID.randomUUID();
        when(documentLibraryMock.createDocument("a", "b", "c")).thenReturn(expectedId);

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        domainController.createDocument("a", "b", "c");
        verify(finderMock, times(1)).addDoc("c", expectedId);
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());
    }

    @Test
    void testGetValidDocumentWithId() throws DocumentException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();
        Document doc = new Document("a", "b", "c");

        when(documentLibraryMock.getWithId(id)).thenReturn(doc);
        assertEquals(doc, domainController.getDocumentWithId(id));
    }

    @Test
    void testModifyInvalidDocAuthor() throws AuthorTitleAlreadyExistsException, DocumentException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();
        doThrow(DocumentException.class).when(documentLibraryMock).modifyDocAuthor(id, "");
        doThrow(AuthorTitleAlreadyExistsException.class).when(documentLibraryMock).modifyDocAuthor(id, "a");

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        assertThrows(DocumentException.class, () -> domainController.modifyDocAuthor(id, ""));
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());

        assertThrows(AuthorTitleAlreadyExistsException.class, () -> domainController.modifyDocAuthor(id, "a"));
        verify(searchResultMock, times(2)).setSearch(new ArrayList<>());
    }

    @Test
    void testModifyValidDocAuthor() throws DomainException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        domainController.modifyDocAuthor(id, "b");
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());
    }

    @Test
    void testModifyInvalidDocTitle() throws AuthorTitleAlreadyExistsException, DocumentException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();
        doThrow(DocumentException.class).when(documentLibraryMock).modifyDocTitle(id, "");
        doThrow(AuthorTitleAlreadyExistsException.class).when(documentLibraryMock).modifyDocTitle(id, "a");

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        assertThrows(DocumentException.class, () -> domainController.modifyDocTitle(id, ""));
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());

        assertThrows(AuthorTitleAlreadyExistsException.class, () -> domainController.modifyDocTitle(id, "a"));
        verify(searchResultMock, times(2)).setSearch(new ArrayList<>());
    }

    @Test
    void testModifyValidDocTitle() throws DomainException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        domainController.modifyDocTitle(id, "b");
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());
    }

    @Test
    void testModifyInvalidDocContent() throws TermFrequencyFinderException, DocumentException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();
        doThrow(DocumentException.class).when(documentLibraryMock).modifyDocContent(id, "");
        doThrow(TermFrequencyFinderException.class).when(finderMock).modifyDoc("", id);

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        assertThrows(DocumentException.class, () -> domainController.modifyDocContent(id, ""));
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());
    }

    @Test
    void testModifyValidDocContent() throws DomainException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        domainController.modifyDocContent(id, "c");
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());
        verify(finderMock, times(1)).modifyDoc("c", id);
    }

    @Test
    void testDelDocument() {
        // setup mocks for current test
        UUID id = UUID.randomUUID();

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        domainController.delDocument(id);
//        verify(documentLibraryMock, times(1)).delDoc(id);
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());
        verify(finderMock, times(1)).delDoc(id);
    }

    @Test
    void testGetDocContent() throws DocumentException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();

        when(documentLibraryMock.getWithId(id)).thenReturn(new Document("a", "b", "c"));
        assertEquals("c", domainController.getDocContent(id));
    }

    @Test
    void testGetDocAuthor() throws DocumentException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();

        when(documentLibraryMock.getWithId(id)).thenReturn(new Document("a", "b", "c"));
        assertEquals("b", domainController.getDocAuthor(id));
    }

    @Test
    void testGetDocTitle() throws DocumentException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();

        when(documentLibraryMock.getWithId(id)).thenReturn(new Document("a", "b", "c"));
        assertEquals("a", domainController.getDocTitle(id));
    }

    @Test
    void testChangeOrder() {
        // setup mocks for current test
        doCallRealMethod().when(searchResultMock).changeOrder(SortCriterion.AuthorASC);
        doCallRealMethod().when(searchResultMock).changeOrder(SortCriterion.TitleDSC);
        doCallRealMethod().when(searchResultMock).getSortCriterion();

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());


        domainController.changeOrder(SortCriterion.AuthorASC);
        assertEquals(SortCriterion.AuthorASC, searchResultMock.getSortCriterion());
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());

        domainController.changeOrder(SortCriterion.TitleDSC);
        assertEquals(SortCriterion.TitleDSC, searchResultMock.getSortCriterion());
        verify(searchResultMock, times(2)).setSearch(new ArrayList<>());
    }


    @Test
    void testReplaceInvalidExpr() throws DomainException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();
        when(expressionLibraryMock.addExpression("")).thenThrow(ExpressionException.class);

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        assertThrows(ExpressionException.class, () -> domainController.replaceExpr("", id));
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());
        verify(expressionLibraryMock, times(1)).delExpression(id);
    }

    @Test
    void testReplaceValidExpr() throws DomainException {
        // setup mocks for current test
        UUID id = UUID.randomUUID();

        // resetSearch() calls these instructions, but we will test this apart
        when(documentLibraryMock.getIDs()).thenReturn(new ArrayList<UUID>());
        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);

        domainController.replaceExpr("a", id);
        verify(searchResultMock, times(1)).setSearch(new ArrayList<>());
        verify(expressionLibraryMock, times(1)).delExpression(id);
    }

    @Test
    void testResetSearch() throws DocumentException {
        doCallRealMethod().when(searchResultMock).setSearch(anyList());
        doCallRealMethod().when(searchResultMock).getSearch();

        UUID id0 = UUID.randomUUID();
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        UUID id3 = UUID.randomUUID();

        Document docId0 = new Document("Hotel California", "The Eagles", "Welcome to the hotel california");
        Document docId1 = new Document("21 Guns", "Green Day", "One, twenty one guns");
        Document docId2 = new Document("Tunnel of Love", "Dire Straits", "Tunnel of Love");
        Document docId3 = new Document("Still Loving You", "The Scorpions", "I'm still loving you");

        when(documentLibraryMock.getWithId(id0)).thenReturn(docId0);
        when(documentLibraryMock.getWithId(id1)).thenReturn(docId1);
        when(documentLibraryMock.getWithId(id2)).thenReturn(docId2);
        when(documentLibraryMock.getWithId(id3)).thenReturn(docId3);

        List<UUID> unsortedList = new ArrayList<UUID>();
        unsortedList.add(id0);
        unsortedList.add(id1);
        unsortedList.add(id2);
        unsortedList.add(id3);

        when(documentLibraryMock.getIDs()).thenReturn(unsortedList);

        List<UUID> sortedAuthorASC = new ArrayList<>();
        sortedAuthorASC.add(id2);
        sortedAuthorASC.add(id1);
        sortedAuthorASC.add(id0);
        sortedAuthorASC.add(id3);

        List<UUID> sortedTitleASC = new ArrayList<>();
        sortedTitleASC.add(id1);
        sortedTitleASC.add(id0);
        sortedTitleASC.add(id3);
        sortedTitleASC.add(id2);

        List<UUID> sortedLengthASC = new ArrayList<>();
        sortedLengthASC.add(id2);
        sortedLengthASC.add(id1);
        sortedLengthASC.add(id3);
        sortedLengthASC.add(id0);

        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.AuthorASC);
        domainController.resetSearch();
        assertEquals(sortedAuthorASC, searchResultMock.getSearch());

        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.AuthorDSC);
        domainController.resetSearch();
        Collections.reverse(sortedAuthorASC); // reverse order
        assertEquals(sortedAuthorASC, searchResultMock.getSearch());

        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleASC);
        domainController.resetSearch();
        assertEquals(sortedTitleASC, searchResultMock.getSearch());

        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.TitleDSC);
        domainController.resetSearch();
        Collections.reverse(sortedTitleASC); // reverse order
        assertEquals(sortedTitleASC, searchResultMock.getSearch());

        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.LengthASC);
        domainController.resetSearch();
        assertEquals(sortedLengthASC, searchResultMock.getSearch());

        when(searchResultMock.getSortCriterion()).thenReturn(SortCriterion.LengthDSC);
        domainController.resetSearch();
        Collections.reverse(sortedLengthASC); // reverse order
        assertEquals(sortedLengthASC, searchResultMock.getSearch());
    }


    public static void main(String[] args) {
        TestRunner.runClassTests(DomainControllerTests.class);
    }
}