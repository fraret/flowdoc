package edu.upc.prop411.domain;

import edu.upc.prop411.TestRunner;
import edu.upc.prop411.domain.exceptions.critical.TermFrequencyFinderException;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class TermFrequencyFinderTests {

    @Test
    void termFrequencySingleWord() throws TermFrequencyFinderException {
        String content = "hola";
        // Expected result
        HashMap<String, Integer> expected = new HashMap<>();
        expected.put("hola", 1);
        // Actual result
        HashMap<String, Integer> actual = TermFrequencyFinder.termFrequency(content);
        // Test
        assertEquals(expected, actual);
    }

    @Test
    void termFrequencyMultipleWord() throws TermFrequencyFinderException {
        String content = "I'm barely there\nI'm everywhere\nHeavy thin air\nSahara mascara\nsahara!Mascara\n";
        // Expected result
        HashMap<String, Integer> expected = new HashMap<>();
        expected.put("i", 2);
        expected.put("m", 2);
        expected.put("barely", 1);
        expected.put("there", 1);
        expected.put("everywhere", 1);
        expected.put("heavy", 1);
        expected.put("thin", 1);
        expected.put("air", 1);
        expected.put("sahara", 2);
        expected.put("mascara", 2);
        // Actual result
        HashMap<String, Integer> actual = TermFrequencyFinder.termFrequency(content);
        // Test
        assertEquals(expected, actual);
    }

    @Test
    void termFrequencyMultipleWordCatalaCastella() throws TermFrequencyFinderException {
        String content = "\u00CDndia, \u00C8xode, ra\u00CFm, ca\u00C7a, \u00D1u, raIm";
        // Expected result
        HashMap<String, Integer> expected = new HashMap<>();
        expected.put("\u00EDndia", 1);
        expected.put("\u00E8xode", 1);
        expected.put("ra\u00EFm", 1);
        expected.put("ca\u00E7a", 1);
        expected.put("\u00F1u", 1);
        expected.put("raim", 1);
//        for (Map.Entry<String, Integer> entry : expected.entrySet())
//        {
//            System.out.println("key: " + entry.getKey() + "; value: " + entry.getValue());
//        }
//        System.out.println("\n");
        // Actual result
        HashMap<String, Integer> actual = TermFrequencyFinder.termFrequency(content);
//        for (Map.Entry<String, Integer> entry : actual.entrySet())
//        {
//            System.out.println("key: " + entry.getKey() + "; value: " + entry.getValue());
//        }
        // Test
        assertEquals(expected, actual);
    }

    @Test
    void termFrequencyEmpty() throws TermFrequencyFinderException {
        String content = "";
        // Expected result
        HashMap<String, Integer> expected = new HashMap<>();
        // Actual result
        HashMap<String, Integer> actual = TermFrequencyFinder.termFrequency(content);
        // Test
        assertEquals(expected, actual);
    }

    @Test
    void termFrequencyEmptySymbols() throws TermFrequencyFinderException {
        String content = "\u00BF!&-/~| \\__\u00A8%$ \n\u20AC. "; // unicode characters are: Inverted Question Mark, Diaeresis, and Euro Sign
        // Expected result
        HashMap<String, Integer> expected = new HashMap<>();
        // Actual result
        HashMap<String, Integer> actual = TermFrequencyFinder.termFrequency(content);
//        for (Map.Entry<String, Integer> entry : actual.entrySet())
//        {
//            System.out.println("key: " + String.format("%x", entry.getKey().codePointAt(0)) + "; value: " + entry.getValue());
//        }
        // Test
        assertEquals(expected, actual);
    }

    @Test
    void containsSubstrSingleWord() throws TermFrequencyFinderException {
        String content = "You make my life and times a book of bluesy Saturdays";
        String substr = "saturdays";
        // Test
        assertTrue(TermFrequencyFinder.containsSubstr(content, substr));
    }

    @Test
    void containsSubstrNonWordBoundary() throws TermFrequencyFinderException {
        String content = "You make my life and times a book of bluesy Saturdays";
        String substr = "days";
        // Test
        assertFalse(TermFrequencyFinder.containsSubstr(content, substr));
    }

    @Test
    void containsSubstrNonExact() throws TermFrequencyFinderException {
        String content = "You make my life and times a book of bluesy Saturdays";
        String substr = "book, of";
        // Test
        assertFalse(TermFrequencyFinder.containsSubstr(content, substr));
    }

    @Test
    void containsSubstrExactCatalaCastella() throws TermFrequencyFinderException {
        String content = "\u00CDndia, \u00C8xode, ra\u00CFm, ca\u00C7a, \u00D1u, raIm";
        String substr = "\u00D1u, raIm";
        // Test
        assertTrue(TermFrequencyFinder.containsSubstr(content, substr));
    }

    @Test
    void containsSubstrEmpty() {
        String content = "You make my life and times a book of bluesy Saturdays";
        String substr = "";
        // Test
        assertThrowsExactly(TermFrequencyFinderException.class, () -> TermFrequencyFinder.containsSubstr(content, substr));
    }

    public static void main(String[] args) {
        TestRunner.runClassTests(TermFrequencyFinderTests.class);
    }

}
