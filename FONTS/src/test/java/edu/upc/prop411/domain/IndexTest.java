package edu.upc.prop411.domain;

import edu.upc.prop411.TestRunner;
import edu.upc.prop411.domain.exceptions.critical.NotFoundException;
import org.junit.jupiter.api.*;

import java.util.*;

import static java.lang.Math.log;
import static org.junit.jupiter.api.Assertions.*;
public class IndexTest {

  /*  private HashMap<String, Integer> randomTF(){

    }*/

    private HashMap<Integer, UUID> addTFs(Index index){
        HashMap<String, Integer> TF1 = new HashMap<String, Integer>();
        HashMap<String, Integer> TF2 = new HashMap<String, Integer>();
        HashMap<String, Integer> TF3 = new HashMap<String, Integer>();

        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        UUID id3 = UUID.randomUUID();
        HashMap<Integer, UUID> docs = new HashMap<>();
        docs.put(1,id1);
        docs.put(2,id2);
        docs.put(3,id3);

        TF1.put("barquito", 4);
        TF1.put("vela", 2);
        TF1.put("marinero", 6);
        TF1.put("traje", 9);
        TF1.put("viento", 1);
        TF1.put("mar", 3);
        TF2.put("vela", 2);
        TF2.put("viento", 5);
        TF2.put("oleaje", 8);
        TF2.put("tormenta", 9);
        TF2.put("naufragio", 2);
        TF3.put("bandera", 4);
        TF3.put("vela", 3);
        TF3.put("puerto", 1);
        TF3.put("traje", 9);
        TF3.put("muelle", 3);

        index.addTF(TF1, id1);
        index.addTF(TF2, id2);
        index.addTF(TF3, id3);

        return docs;
    }

    @Test
    public void addTFTest() {
        HashMap<String, Integer> TF1exp = new HashMap<String, Integer>();
        HashMap<String, Integer> TF2exp = new HashMap<String, Integer>();
        HashMap<String, Integer> TF3exp = new HashMap<String, Integer>();
        TF1exp.put("barquito", 4);
        TF1exp.put("vela", 2);
        TF1exp.put("marinero", 6);
        TF1exp.put("traje", 9);
        TF1exp.put("viento", 1);
        TF1exp.put("mar", 3);
        TF2exp.put("vela", 2);
        TF2exp.put("viento", 5);
        TF2exp.put("oleaje", 8);
        TF2exp.put("tormenta", 9);
        TF2exp.put("naufragio", 2);
        TF3exp.put("bandera", 4);
        TF3exp.put("vela", 3);
        TF3exp.put("puerto", 1);
        TF3exp.put("traje", 9);
        TF3exp.put("muelle", 3);

        Index index = new Index();
        HashMap<Integer, UUID> docs = new HashMap<Integer, UUID>(addTFs(index));

        assertEquals(TF1exp, index.getTF(docs.get(1)));
        assertEquals(TF2exp, index.getTF(docs.get(2)));
        assertEquals(TF3exp, index.getTF(docs.get(3)));

    }

    @Test
    public void docsWithWordTest(){
        Index index = new Index();
        HashMap<Integer, UUID> docs = new HashMap<Integer, UUID>(addTFs(index));
        Set<UUID> docsWithBarquito = index.docsWithWord("barquito");
        Set<UUID> docsWithCarro = index.docsWithWord("Carro");
        Set<UUID> docsWithTraje = index.docsWithWord("traje");
        Set<UUID> docsWithVela = index.docsWithWord("vela");


        assertTrue(docsWithBarquito.contains((docs.get(1))) && docsWithBarquito.size() == 1);
        assertTrue(docsWithCarro.isEmpty());
        assertTrue(docsWithTraje.contains(docs.get(1)) && docsWithTraje.contains(docs.get(3)) && docsWithTraje.size() == 2);
        assertTrue(docsWithVela.size() == 3);
    }

    @Test
    public void docsWithWordsTest(){
        Index index = new Index();
        HashMap<Integer, UUID> docs = new HashMap<Integer, UUID>(addTFs(index));

        Collection<String> words1 = new ArrayList<>();
        words1.add("barquito");
        words1.add("traje");

        Collection<String> words2 = new ArrayList<>();
        words2.add("barquito");
        words2.add("carro");

        Set<UUID> docsWithWords1 = index.docsWithWords(words1);
        Set<UUID> docsWithWords2 = index.docsWithWords(words2);

        assertTrue(docsWithWords1.contains(docs.get(1)) && docsWithWords1.size() == 1);
        assertTrue(docsWithWords2.isEmpty());
    }

    @Test
    public void delTFTest() {
        Index index = new Index();
        HashMap<Integer, UUID> docs = new HashMap<Integer, UUID>(addTFs(index));

        index.delTF(docs.get(1));
        assertThrows(NotFoundException.class, () -> {
            HashMap<String, Integer> TF1exp = index.getTF(docs.get(1));
        });


        UUID randID = UUID.randomUUID();
        assertThrows(NotFoundException.class, () -> {
            HashMap<String, Integer> doc2 = index.getTF(randID);
        });
    }

    @Test
    public void modifyTFTest() {
        Index index = new Index();
        HashMap<Integer, UUID> docs = new HashMap<Integer, UUID>(addTFs(index));

        HashMap<String, Integer> TF1 = new HashMap<String, Integer>();
        TF1.put("coche", 4);
        TF1.put("motor", 2);
        TF1.put("conductor", 6);
        TF1.put("mono", 9);
        TF1.put("gasolina", 1);
        TF1.put("asfalto", 3);
        TF1.put("ruedas", 7);

        index.modifyTF(TF1, docs.get(1));
        HashMap<String, Integer> TF1mod = index.getTF(docs.get(1));
        //assertTrue(TF1.equals(TF1mod));
    }

    @Test
    public void getIDFTest() {
        Index index = new Index();

        HashMap<String, Integer> TF1 = new HashMap<String, Integer>();
        TF1.put("coche", 3);
        TF1.put("motor", 1);
        TF1.put("conductor", 2);
        UUID id1 = UUID.randomUUID();
        index.addTF(TF1, id1);

        HashMap<String, Integer> TF2 = new HashMap<String, Integer>();
        TF2.put("coche", 1);
        TF2.put("motor", 1);
        TF2.put("ruedas", 2);
        UUID id2 = UUID.randomUUID();
        index.addTF(TF2, id2);

        HashMap<String, Integer> TF3 = new HashMap<String, Integer>();
        TF3.put("piloto", 3);
        UUID id3 = UUID.randomUUID();
        index.addTF(TF3, id3);

        HashMap<String, Double> IDFexp = new HashMap<>();
        IDFexp.put("coche", log(((double) 3) /2));
        IDFexp.put("motor", log(((double) 3) /2));
        IDFexp.put("conductor", log(3));
        IDFexp.put("ruedas", log(3));
        IDFexp.put("piloto", log(3));
        HashMap<String, Double> IDF = index.getIDF();

        assertTrue(IDFexp.equals(IDF));

        TF2.clear();
        TF2.put("piloto", 4);
        TF2.put("ruedas", 3);
        index.modifyTF(TF2, id2);
        HashMap<String, Double> IDFmod = index.getIDF();
        IDFexp.replace("piloto", log(((double) 3) /2));
        IDFexp.replace("coche", log(3));
        IDFexp.replace("motor", log(3));
        assertTrue(IDFexp.equals(IDFmod));
    }

    @Test
    void getIDFEmptyIndex(){
        Index index = new Index();
        HashMap<String, Double> IDF = index.getIDF();
        assertTrue(IDF.isEmpty());
    }

    public static void main(String[] args) {
        TestRunner.runClassTests(IndexTest.class);
    }
}
