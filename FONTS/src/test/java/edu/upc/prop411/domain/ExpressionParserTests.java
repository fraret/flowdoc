package edu.upc.prop411.domain;

import edu.upc.prop411.TestRunner;
import edu.upc.prop411.domain.exceptions.recoverable.ExpressionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class ExpressionParserTests {

    ExpressionParser parser;

    @BeforeEach
    void createParser() {
        parser = new ExpressionParser();
    }

    private String serializeExpression(Expression expression) {
        if (expression instanceof ExpNot) {
            ExpNot expNot = (ExpNot) expression;
            return "NOT("+ serializeExpression(expNot.getExpression()) + ")";
        } else if (expression instanceof ExpBinOp) {
            ExpBinOp expBinOp = (ExpBinOp) expression;
            return (expBinOp.getType() == ExpBinOp.BinaryOperationType.AND ? "AND" : "OR") +
                    "(" + serializeExpression(expBinOp.getLeftExpression())+ ", " + serializeExpression(expBinOp.getRightExpression())+ ")";
        } else if (expression instanceof ExpSubstr) {
            ExpSubstr expSubstr = (ExpSubstr) expression;
            return "SUBSTR(\"" + expSubstr.getSubstr() + "\")";
        } else if (expression instanceof ExpWordSet) {
            ExpWordSet expWordSet = (ExpWordSet) expression;
            StringBuilder texts = new StringBuilder("WORDS(\"");
            boolean first = true;
            for(String word: expWordSet.getWords()) {
                if (!first) texts.append("\", \"");
                else first = false;
                texts.append(word);
            }
            texts.append("\")");
            return texts.toString();
        }
        throw new RuntimeException("Error in test");
    }

    @Test
    void parseEmpty() {
        ExpressionException exception = assertThrows(ExpressionException.class, () -> parser.parse(new ArrayList<>()));
        assertEquals("Expression ended when expecting a boolean expression", exception.getMessage());
    }

    @Test
    void parseWord() throws ExpressionException {
        List<ExpToken> tokens = Stream.of("casa").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        Expression exp = parser.parse(tokens);
        String resultingString = serializeExpression(exp);
        String expectedString = "WORDS(\"casa\")";
        assertEquals(expectedString, resultingString);
    }

    @Test
    void parseWordList() throws ExpressionException {
        List<ExpToken> tokens = Stream.of("{", "casa", "cotxe", "tomaquet", "}").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        Expression exp = parser.parse(tokens);
        String resultingString = serializeExpression(exp);
        String expectedString = "WORDS(\"casa\", \"cotxe\", \"tomaquet\")";
        //Warning: order is not preserved, be aware!
        assertEquals(expectedString, resultingString);
    }

    @Test
    void parseOr() throws ExpressionException {
        List<ExpToken> tokens = Stream.of( "casa", "|", "tomaquet" ).map(ExpTokenTests::simpTok).collect(Collectors.toList());
        Expression exp = parser.parse(tokens);
        String resultingString = serializeExpression(exp);
        String expectedString = "OR(WORDS(\"casa\"), WORDS(\"tomaquet\"))";
        assertEquals(expectedString, resultingString);
    }

    @Test
    void parseAnd() throws ExpressionException {
        List<ExpToken> tokens = Stream.of( "casa", "&", "tomaquet" ).map(ExpTokenTests::simpTok).collect(Collectors.toList());
        Expression exp = parser.parse(tokens);
        String resultingString = serializeExpression(exp);
        String expectedString = "AND(WORDS(\"casa\"), WORDS(\"tomaquet\"))";
        assertEquals(expectedString, resultingString);
    }

    @Test
    void parseNot() throws ExpressionException {
        List<ExpToken> tokens = Stream.of( "!", "tomaquet" ).map(ExpTokenTests::simpTok).collect(Collectors.toList());
        Expression exp = parser.parse(tokens);
        String resultingString = serializeExpression(exp);
        String expectedString = "NOT(WORDS(\"tomaquet\"))";
        assertEquals(expectedString, resultingString);
    }

    @Test
    void parseSubstr() throws ExpressionException {
        List<ExpToken> tokens = Stream.of( "\"substring que;:!??-ññ\"" ).map(ExpTokenTests::simpTok).collect(Collectors.toList());
        Expression exp = parser.parse(tokens);
        String resultingString = serializeExpression(exp);
        String expectedString = "SUBSTR(\"substring que;:!??-ññ\")";
        assertEquals(expectedString, resultingString);
    }

    @Test
    void parseAndOr() throws ExpressionException {
        List<ExpToken> tokens = Stream.of( "pera", "|", "casa", "&", "tomaquet" ).map(ExpTokenTests::simpTok).collect(Collectors.toList());
        Expression exp = parser.parse(tokens);
        String resultingString = serializeExpression(exp);
        String expectedString = "OR(WORDS(\"pera\"), AND(WORDS(\"casa\"), WORDS(\"tomaquet\")))";
        assertEquals(expectedString, resultingString);
    }

    @Test
    void parseAndOrParentheses() throws ExpressionException {
        List<ExpToken> tokens = Stream.of( "(", "pera", "|", "casa", ")", "&", "tomaquet" ).map(ExpTokenTests::simpTok).collect(Collectors.toList());
        Expression exp = parser.parse(tokens);
        String resultingString = serializeExpression(exp);
        String expectedString = "AND(OR(WORDS(\"pera\"), WORDS(\"casa\")), WORDS(\"tomaquet\"))";
        assertEquals(expectedString, resultingString);
    }

    @Test
    void parseComplex() throws ExpressionException {
        List<ExpToken> tokens = Stream.of("a", "|", "b", "&", "c", "&", "!", "(", "d", "|", "e", ")", "|", "!", "f", "|", "{", "a", "}", "&", "\"potato and\"").map(ExpTokenTests::simpTok).collect(Collectors.toList());
        Expression exp = parser.parse(tokens);
        String resultingString = serializeExpression(exp);
        String expectedString = "OR(OR(WORDS(\"a\"), AND(AND(WORDS(\"b\"), WORDS(\"c\")), NOT(OR(WORDS(\"d\"), WORDS(\"e\"))))), OR(NOT(WORDS(\"f\")), AND(WORDS(\"a\"), SUBSTR(\"potato and\"))))";
        assertEquals(expectedString, resultingString);
    }

    public static void main(String[] args) {
        TestRunner.runClassTests(ExpressionParserTests.class);
    }

}
