package edu.upc.prop411.domain;

import java.util.*;

import edu.upc.prop411.TestRunner;
import edu.upc.prop411.domain.exceptions.recoverable.DomainException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static java.lang.Math.log;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FinderTests {

    Finder finder;
    Index indexMock = mock(Index.class);
    DomainController domainControllerMock = mock(DomainController.class);

    @BeforeEach
    void setUp() {
        finder = new Finder(domainControllerMock);
        try {
            // assign classes to mocks
            Field indexField = Finder.class.getDeclaredField("index");

            indexField.setAccessible(true);
            indexField.set(finder, indexMock);
        } catch (Exception e) {
            fail("Can't find Finder field");
        }
    }

    @Test
    void getDocsExpressionSimple() throws DomainException {
        String expressio = "hola | Ad\u00E9u";
        Set<String> lset = new HashSet<>(); lset.add("hola"); ExpWordSet lexp = new ExpWordSet(lset, "hola");
        Set<String> rset = new HashSet<>(); rset.add("Ad\u00E9u"); ExpWordSet rexp = new ExpWordSet(rset, "Ad\u00E9u");
        ExpBinOp exp = new ExpBinOp(ExpBinOp.BinaryOperationType.OR, lexp, rexp, "hola | Ad\u00E9u");

        // id1 has content "hola farola"
        // id2 has content "ad\u00E9u farola"
        // id3 has content "hola ad\u00E9u"
        // id4 has content "farola farola"
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        UUID id3 = UUID.randomUUID();
        UUID id4 = UUID.randomUUID();
        Set<UUID> s1 = new HashSet<>(); s1.add(id1); s1.add(id3);
        Set<UUID> s2 = new HashSet<>(); s2.add(id2); s2.add(id3);
        when(indexMock.docsWithWords(lset)).thenReturn(s1);
        when(indexMock.docsWithWords(rset)).thenReturn(s2);

        // Expected result
        Set<UUID> expected = new HashSet<>(); expected.add(id1); expected.add(id2); expected.add(id3);
        // Actual result
        Set<UUID> actual = finder.getDocsExpression(exp);
        // Test
        assertEquals(expected, actual);
    }

    @Test
    void getDocsExpressionSubstr() throws DomainException {
        String expressio = "salutacions | \"hola Ad\u00E9u\"";

        Set<String> lset = new HashSet<>(); lset.add("salutacions"); ExpWordSet lexp = new ExpWordSet(lset, "salutacions");
        Set<String> rset = new HashSet<>(); rset.add("hola"); rset.add("Ad\u00E9u"); ExpSubstr rexp = new ExpSubstr("hola Ad\u00E9u", "\"hola Ad\u00E9u\"");
        ExpBinOp exp = new ExpBinOp(ExpBinOp.BinaryOperationType.OR, lexp, rexp, "salutacions | \"hola Ad\u00E9u\"");

        // id1 has content "hola farola"
        // id2 has content "ad\u00E9u farola"
        // id3 has content "hola hola ad\u00E9u farola"
        // id4 has content "farola farola"
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        UUID id3 = UUID.randomUUID();
        UUID id4 = UUID.randomUUID();
        Set<UUID> s1 = new HashSet<>();
        Set<UUID> s2 = new HashSet<>(); s2.add(id3);
        when(indexMock.docsWithWords(lset)).thenReturn(s1);
        // when(indexMock.docsWithWords(rset)).thenReturn(s2);
        Set<String> rset2 = new HashSet<>(); rset2.add("hola"); rset2.add("ad\u00E9u"); // rexp will report its words to be all lowercase
        when(indexMock.docsWithWords(rset2)).thenReturn(s2);

        when(domainControllerMock.getDocContent(id3)).thenReturn(" hola hola ad\u00E9u farola");

        // Expected result
        Set<UUID> expected = new HashSet<>(); expected.add(id3);
        // Actual result
        Set<UUID> actual = finder.getDocsExpression(exp);
        // Test
        assertIterableEquals(expected, actual);
    }

    @Test
    void getDocsByWordListSimpleQuery() throws DomainException {
        String query = "alpha beta iota";
//        String content1 = "alpha";
//        String content2 = "alpha beta";
//        String content3 = "beta gamma";
//        String content4 = "alpha";
//        String content5 = "zeta";

//        HashMap<String, Integer> docTF = new HashMap<>();
//        docTF.put("alpha", 1);
//        docTF.put("beta", 1);
//        docTF.put("iota", 1);
//        when(TermFrequencyFinder.termFrequency(query)).thenReturn(docTF);
//
        HashMap<String, Double> IDF = new HashMap<>();
        IDF.put("alpha", log(5./3.));
        IDF.put("beta", log(5./2.));
        IDF.put("gamma", log(5./1.));
        IDF.put("zeta", log(5./1.));
        when(indexMock.getIDF()).thenReturn(IDF);

        Set<UUID> ids = new HashSet<>();
        UUID id1 = UUID.randomUUID(); ids.add(id1);
        UUID id2 = UUID.randomUUID(); ids.add(id2);
        UUID id3 = UUID.randomUUID(); ids.add(id3);
        UUID id4 = UUID.randomUUID(); ids.add(id4);
        UUID id5 = UUID.randomUUID(); ids.add(id5);
        when(indexMock.getIDs()).thenReturn(ids);

        HashMap<String, Integer> TF1 = new HashMap<>();
        TF1.put("alpha", 1);
        when(indexMock.getTF(id1)).thenReturn(TF1);
        HashMap<String, Integer> TF2 = new HashMap<>();
        TF2.put("alpha", 1);
        TF2.put("beta", 1);
        when(indexMock.getTF(id2)).thenReturn(TF2);
        HashMap<String, Integer> TF3 = new HashMap<>();
        TF3.put("beta", 1);
        TF3.put("gamma", 1);
        when(indexMock.getTF(id3)).thenReturn(TF3);
        HashMap<String, Integer> TF4 = new HashMap<>();
        TF4.put("alpha", 1);
        when(indexMock.getTF(id4)).thenReturn(TF4);
        HashMap<String, Integer> TF5 = new HashMap<>();
        TF5.put("zeta", 1);
        when(indexMock.getTF(id5)).thenReturn(TF5);

        // Expected result
        // Note that id1 and id4 could be in reverse order too
        List<UUID> expected = new ArrayList<>(); expected.add(id2); expected.add(id1); expected.add(id4); expected.add(id3); expected.add(id5);
        // Actual result
        List<UUID> actual = finder.getDocsByWordList(query, 5);
        // If actual has id1 and id4 in reverse order, modify expected accordingly
        if((actual.contains(id1)) && (actual.contains(id4)) && (actual.indexOf(id1) > actual.indexOf(id4))){
            expected.set(1, id4);
            expected.set(2, id1);
        }
        // Test
        assertIterableEquals(expected, actual);
    }

    @Test
    void getDocsByWordListBadEmptyQuery() throws DomainException {
        String query = "&& ";
//        String content1 = "alpha";
//        String content2 = "alpha beta";
//        String content3 = "beta gamma";
//        String content4 = "alpha";
//        String content5 = "zeta";

//        HashMap<String, Integer> docTF = new HashMap<>();
//        when(TermFrequencyFinder.termFrequency(query)).thenReturn(docTF);
//
        HashMap<String, Double> IDF = new HashMap<>();
        IDF.put("alpha", log(5./3.));
        IDF.put("beta", log(5./2.));
        IDF.put("gamma", log(5./1.));
        IDF.put("zeta", log(5./1.));
        when(indexMock.getIDF()).thenReturn(IDF);

        Set<UUID> ids = new HashSet<>();
        UUID id1 = UUID.randomUUID(); ids.add(id1);
        UUID id2 = UUID.randomUUID(); ids.add(id2);
        UUID id3 = UUID.randomUUID(); ids.add(id3);
        UUID id4 = UUID.randomUUID(); ids.add(id4);
        UUID id5 = UUID.randomUUID(); ids.add(id5);
        when(indexMock.getIDs()).thenReturn(ids);

        HashMap<String, Integer> TF1 = new HashMap<>();
        TF1.put("alpha", 1);
        when(indexMock.getTF(id1)).thenReturn(TF1);
        HashMap<String, Integer> TF2 = new HashMap<>();
        TF2.put("alpha", 1);
        TF2.put("beta", 1);
        when(indexMock.getTF(id2)).thenReturn(TF2);
        HashMap<String, Integer> TF3 = new HashMap<>();
        TF3.put("beta", 1);
        TF3.put("gamma", 1);
        when(indexMock.getTF(id3)).thenReturn(TF3);
        HashMap<String, Integer> TF4 = new HashMap<>();
        TF4.put("alpha", 1);
        when(indexMock.getTF(id4)).thenReturn(TF4);
        HashMap<String, Integer> TF5 = new HashMap<>();
        TF5.put("zeta", 1);
        when(indexMock.getTF(id5)).thenReturn(TF5);

        // Expected result
        // ids in any order, as none match
        // Actual result
        List<UUID> actual = finder.getDocsByWordList(query, 5);
        // Test
        assertEquals(5, actual.size());
    }

    @Test
    void getDocsByWordListBadQuery() throws DomainException {
        String query = "&& alpha beta";
//        String content1 = "alpha";
//        String content2 = "alpha beta";
//        String content3 = "beta gamma";
//        String content4 = "alpha";
//        String content5 = "zeta";

//        HashMap<String, Integer> docTF = new HashMap<>();
//        docTF.put("alpha", 1);
//        docTF.put("beta", 1);
//        when(TermFrequencyFinder.termFrequency(query)).thenReturn(docTF);
//
        HashMap<String, Double> IDF = new HashMap<>();
        IDF.put("alpha", log(5./3.));
        IDF.put("beta", log(5./2.));
        IDF.put("gamma", log(5./1.));
        IDF.put("zeta", log(5./1.));
        when(indexMock.getIDF()).thenReturn(IDF);

        Set<UUID> ids = new HashSet<>();
        UUID id1 = UUID.randomUUID(); ids.add(id1);
        UUID id2 = UUID.randomUUID(); ids.add(id2);
        UUID id3 = UUID.randomUUID(); ids.add(id3);
        UUID id4 = UUID.randomUUID(); ids.add(id4);
        UUID id5 = UUID.randomUUID(); ids.add(id5);
        when(indexMock.getIDs()).thenReturn(ids);

        HashMap<String, Integer> TF1 = new HashMap<>();
        TF1.put("alpha", 1);
        when(indexMock.getTF(id1)).thenReturn(TF1);
        HashMap<String, Integer> TF2 = new HashMap<>();
        TF2.put("alpha", 1);
        TF2.put("beta", 1);
        when(indexMock.getTF(id2)).thenReturn(TF2);
        HashMap<String, Integer> TF3 = new HashMap<>();
        TF3.put("beta", 1);
        TF3.put("gamma", 1);
        when(indexMock.getTF(id3)).thenReturn(TF3);
        HashMap<String, Integer> TF4 = new HashMap<>();
        TF4.put("alpha", 1);
        when(indexMock.getTF(id4)).thenReturn(TF4);
        HashMap<String, Integer> TF5 = new HashMap<>();
        TF5.put("zeta", 1);
        when(indexMock.getTF(id5)).thenReturn(TF5);

        // Expected result
        // Note that id1 and id4 could be in reverse order too
        List<UUID> expected = new ArrayList<>(); expected.add(id2); expected.add(id1); expected.add(id4); expected.add(id3); expected.add(id5);
        // Actual result
        List<UUID> actual = finder.getDocsByWordList(query, 5);
        // If actual has id1 and id4 in reverse order, modify expected accordingly
        if((actual.contains(id1)) && (actual.contains(id4)) && (actual.indexOf(id1) > actual.indexOf(id4))){
            expected.set(1, id4);
            expected.set(2, id1);
        }
        // Test
        assertIterableEquals(expected, actual);
    }

    @Test
    void getSimilarDocsSimple(){
//        Search by id4
//        String content1 = "alpha";
//        String content2 = "alpha beta";
//        String content3 = "beta gamma";
//        String content4 = "alpha";
//        String content5 = "zeta";

        HashMap<String, Double> IDF = new HashMap<>();
        IDF.put("alpha", log(5./3.));
        IDF.put("beta", log(5./2.));
        IDF.put("gamma", log(5./1.));
        IDF.put("zeta", log(5./1.));
        when(indexMock.getIDF()).thenReturn(IDF);

        Set<UUID> ids = new HashSet<>();
        UUID id1 = UUID.randomUUID(); ids.add(id1);
        UUID id2 = UUID.randomUUID(); ids.add(id2);
        UUID id3 = UUID.randomUUID(); ids.add(id3);
        UUID id4 = UUID.randomUUID(); ids.add(id4);
        UUID id5 = UUID.randomUUID(); ids.add(id5);
        when(indexMock.getIDs()).thenReturn(ids);

        HashMap<String, Integer> TF1 = new HashMap<>();
        TF1.put("alpha", 1);
        when(indexMock.getTF(id1)).thenReturn(TF1);
        HashMap<String, Integer> TF2 = new HashMap<>();
        TF2.put("alpha", 1);
        TF2.put("beta", 1);
        when(indexMock.getTF(id2)).thenReturn(TF2);
        HashMap<String, Integer> TF3 = new HashMap<>();
        TF3.put("beta", 1);
        TF3.put("gamma", 1);
        when(indexMock.getTF(id3)).thenReturn(TF3);
        HashMap<String, Integer> TF4 = new HashMap<>();
        TF4.put("alpha", 1);
        when(indexMock.getTF(id4)).thenReturn(TF4);
        HashMap<String, Integer> TF5 = new HashMap<>();
        TF5.put("zeta", 1);
        when(indexMock.getTF(id5)).thenReturn(TF5);

        // Expected result
        // Note that id3 and id5 could be in reverse order too
        List<UUID> expected = new ArrayList<>(); expected.add(id1); expected.add(id2); expected.add(id3); expected.add(id5);
        // Actual result
        List<UUID> actual = finder.getSimilarDocs(id4, 5);
        // If actual has id3 and id5 in reverse order, modify expected accordingly
        if((actual.contains(id3)) && (actual.contains(id5)) && (actual.indexOf(id3) > actual.indexOf(id5))){
            expected.set(2, id5);
            expected.set(3, id3);
        }
        // Test
        assertIterableEquals(expected, actual);
    }

    @Test
    void getSimilarDocsSimple2(){
//        Search by id2
//        String content1 = "alpha";
//        String content2 = "alpha beta";
//        String content3 = "beta gamma";
//        String content4 = "alpha";
//        String content5 = "zeta";

        HashMap<String, Double> IDF = new HashMap<>();
        IDF.put("alpha", log(5./3.));
        IDF.put("beta", log(5./2.));
        IDF.put("gamma", log(5./1.));
        IDF.put("zeta", log(5./1.));
        when(indexMock.getIDF()).thenReturn(IDF);

        Set<UUID> ids = new HashSet<>();
        UUID id1 = UUID.randomUUID(); ids.add(id1);
        UUID id2 = UUID.randomUUID(); ids.add(id2);
        UUID id3 = UUID.randomUUID(); ids.add(id3);
        UUID id4 = UUID.randomUUID(); ids.add(id4);
        UUID id5 = UUID.randomUUID(); ids.add(id5);
        when(indexMock.getIDs()).thenReturn(ids);

        HashMap<String, Integer> TF1 = new HashMap<>();
        TF1.put("alpha", 1);
        when(indexMock.getTF(id1)).thenReturn(TF1);
        HashMap<String, Integer> TF2 = new HashMap<>();
        TF2.put("alpha", 1);
        TF2.put("beta", 1);
        when(indexMock.getTF(id2)).thenReturn(TF2);
        HashMap<String, Integer> TF3 = new HashMap<>();
        TF3.put("beta", 1);
        TF3.put("gamma", 1);
        when(indexMock.getTF(id3)).thenReturn(TF3);
        HashMap<String, Integer> TF4 = new HashMap<>();
        TF4.put("alpha", 1);
        when(indexMock.getTF(id4)).thenReturn(TF4);
        HashMap<String, Integer> TF5 = new HashMap<>();
        TF5.put("zeta", 1);
        when(indexMock.getTF(id5)).thenReturn(TF5);

        // Expected result
        // Note that id1 and id4 could be in reverse order too
        List<UUID> expected = new ArrayList<>(); expected.add(id1); expected.add(id4); expected.add(id3); expected.add(id5);
        // Actual result
        List<UUID> actual = finder.getSimilarDocs(id2, 5);
        // If actual has id1 and id4 in reverse order, modify expected accordingly
        if((actual.contains(id1)) && (actual.contains(id4)) && (actual.indexOf(id1) > actual.indexOf(id4))){
            expected.set(0, id4);
            expected.set(1, id1);
        }
        // Test
        assertIterableEquals(expected, actual);
    }

    public static void main(String[] args) {
        TestRunner.runClassTests(FinderTests.class);
    }

}
