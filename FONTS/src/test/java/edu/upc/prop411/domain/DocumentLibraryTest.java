package edu.upc.prop411.domain;

import edu.upc.prop411.TestRunner;
import edu.upc.prop411.domain.exceptions.critical.NotFoundException;
import edu.upc.prop411.domain.exceptions.recoverable.AuthorTitleAlreadyExistsException;
import edu.upc.prop411.domain.exceptions.recoverable.DocumentException;
import edu.upc.prop411.domain.exceptions.recoverable.NotExistsException;
import org.junit.jupiter.api.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class DocumentLibraryTest {

    @Test
    void createValid(){
        DocumentLibrary documentLibrary = new DocumentLibrary();
    }

    @Test
    void createDocument () throws AuthorTitleAlreadyExistsException, DocumentException {
        DocumentLibrary documentLibrary = new DocumentLibrary();
        UUID doc = documentLibrary.createDocument("The Odyssey", "Homer", "Once upon a time...");
        assertNotNull(doc);
    }

    @Test
    void createDocumentAlreadyExists() throws AuthorTitleAlreadyExistsException, DocumentException {
        DocumentLibrary documentLibrary = new DocumentLibrary();
        UUID doc = documentLibrary.createDocument("The Odyssey", "Homer", "Once upon a time...");
        assertNotNull(doc);
        assertThrows(AuthorTitleAlreadyExistsException.class, () -> {
            UUID doc2 = documentLibrary.createDocument("The Odyssey", "Homer", "Once upon a time...");
        });
    }

    @Test
    void getWithAuthorTitle() throws AuthorTitleAlreadyExistsException, DocumentException, NotExistsException {
        DocumentLibrary documentLibrary = new DocumentLibrary();
        UUID doc = documentLibrary.createDocument("The Odyssey", "Homer", "Once upon a time...");
        assertThrows(NotExistsException.class, () -> {
            UUID doc2 = documentLibrary.getWithAuthorTitle("The Republic", "Plato");
        });
        UUID doc3 = documentLibrary.getWithAuthorTitle("Homer", "The Odyssey");
        assertEquals(doc, doc3);
    }

    @Test
    void getAuthorDocs() throws AuthorTitleAlreadyExistsException, DocumentException, NotExistsException {
        DocumentLibrary documentLibrary = new DocumentLibrary();
        UUID doc1 = documentLibrary.createDocument("The Odyssey", "Homer", "Once upon a time...");
        UUID doc2 = documentLibrary.createDocument("The Iliad", "Homer", "Once upon a time...");
        UUID doc3 = documentLibrary.createDocument("The Republic", "Plato", "Once upon a time...");
        Collection<UUID> docsByHomer = new HashSet<>();
        docsByHomer.add(doc1);
        docsByHomer.add(doc2);
        Collection<UUID> docsByHomerDL = new HashSet<>(documentLibrary.getAuthorDocs("Homer"));
        assertEquals(docsByHomer, docsByHomerDL);
        assertThrows(NotExistsException.class, () -> {
            Collection<UUID> NotExistingAuthorDocs = documentLibrary.getAuthorDocs("Arquimedes");
        });
    }

    @Test
    void ModifyAuthor() throws AuthorTitleAlreadyExistsException, DocumentException {
        DocumentLibrary DL = new DocumentLibrary();
        UUID doc = DL.createDocument("The Republic", "Homer", "Once upon a time...");
        DL.modifyDocAuthor(doc, "Plato");
    }

    @Test
    void ModifyAuthorAlreadyExists() throws AuthorTitleAlreadyExistsException, DocumentException {
        DocumentLibrary DL = new DocumentLibrary();
        UUID doc = DL.createDocument("The Republic", "Homer", "Once upon a time...");
        UUID doc2 = DL.createDocument("The Republic", "Plato", "Once upon a time...");

        assertThrows(AuthorTitleAlreadyExistsException.class, () -> {
            DL.modifyDocAuthor(doc, "Plato");
        });
    }

    @Test
    void ModifyAuthorNotFound() throws AuthorTitleAlreadyExistsException {
        DocumentLibrary DL = new DocumentLibrary();

        assertThrows(NotFoundException.class, () -> {
            UUID doc = UUID.randomUUID();
            DL.modifyDocAuthor(doc, "Plato");
        });
    }

    //////////////////////////

    @Test
    void ModifyTitle() throws AuthorTitleAlreadyExistsException, DocumentException {
        DocumentLibrary DL = new DocumentLibrary();
        UUID doc = DL.createDocument("The Republic", "Homer", "Once upon a time...");
        DL.modifyDocTitle(doc, "The Odyssey");
    }

    @Test
    void ModifyTitleAlreadyExists() throws AuthorTitleAlreadyExistsException, DocumentException {
        DocumentLibrary DL = new DocumentLibrary();
        UUID doc = DL.createDocument("The Republic", "Homer", "Once upon a time...");
        UUID doc2 = DL.createDocument("The Odyssey", "Homer", "Once upon a time...");

        assertThrows(AuthorTitleAlreadyExistsException.class, () -> {
            DL.modifyDocTitle(doc, "The Odyssey");
        });
    }

    @Test
    void ModifyTitleNotFound() throws AuthorTitleAlreadyExistsException {
        DocumentLibrary DL = new DocumentLibrary();

        assertThrows(NotFoundException.class, () -> {
            UUID doc = UUID.randomUUID();
            DL.modifyDocTitle(doc, "Whatever");
        });
    }

    @Test
    void ModifyContent() throws AuthorTitleAlreadyExistsException, DocumentException {
        DocumentLibrary DL = new DocumentLibrary();
        UUID doc = DL.createDocument("The Republic", "Plato", "Once upon a time...");
        DL.modifyDocContent(doc, "Not so long ago..." );
    }

    @Test
    void ModifContentNotFound() throws AuthorTitleAlreadyExistsException {
        DocumentLibrary DL = new DocumentLibrary();

        assertThrows(NotFoundException.class, () -> {
            UUID doc = UUID.randomUUID();
            DL.modifyDocContent(doc, "Not so long ago...");
        });
    }

    @Test
    void deleteDoc() throws AuthorTitleAlreadyExistsException, DocumentException {
        DocumentLibrary DL = new DocumentLibrary();
        UUID doc = DL.createDocument("The Odyssey", "Homer", "Once upon a time...");
        DL.delDoc(doc);
        assertEquals(new HashSet<UUID>(), new HashSet<>(DL.getDocs()));

        assertThrows(NotFoundException.class, () -> {
            UUID doc1 = UUID.randomUUID();
            DL.modifyDocContent(doc1, "Not so long ago...");
        });
    }


    public static void main(String[] args) {
        TestRunner.runClassTests(DocumentLibraryTest.class);
    }
}
