package edu.upc.prop411.domain;

import edu.upc.prop411.frontend.CLI;
import edu.upc.prop411.frontend.FrontendException;
import edu.upc.prop411.frontend.RangeException;
import edu.upc.prop411.frontend.SelectionException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.Set;

public class CLITests {
    CLI cli;

    @BeforeEach
    void setUp() throws FrontendException {
        cli = new CLI();
    }

    // Selection parser tests
    @Test
    void parseValidSelection() {
        Set<Integer> validSelection1 = Set.of(1, 4, 5, 6, 7, 12);
        assertDoesNotThrow(() -> {
            Set<Integer> parsedSelection = cli.parseSelection("1,4-7,12", 20);
            assertEquals(Set.of(1, 4, 5, 6, 7, 12), parsedSelection);
        });

        Set<Integer> validSelection2 = Set.of(3, 4);
        assertDoesNotThrow(() -> {
            Set<Integer> parsedSelection = cli.parseSelection("3-3,3,4,4,4", 20);
            assertEquals(validSelection2, parsedSelection);
        });

        Set<Integer> validSelection3 = Set.of(5, 6, 7, 8, 9);
        assertDoesNotThrow(() -> {
            Set<Integer> parsedSelection = cli.parseSelection("5-8,6-9", 9);
            assertEquals(validSelection3, parsedSelection);
        });
    }

    @Test
    void parseInvalidSelections() {
        assertThrows(SelectionException.class, () -> cli.parseSelection(",4", 20));
        assertThrows(SelectionException.class, () -> cli.parseSelection("4,", 20));
        assertThrows(SelectionException.class, () -> cli.parseSelection(",", 20));
        assertThrows(SelectionException.class, () -> cli.parseSelection(".", 20));
        assertThrows(SelectionException.class, () -> cli.parseSelection("^", 20));
        assertThrows(SelectionException.class, () -> cli.parseSelection("-1", 20));
        assertThrows(SelectionException.class, () -> cli.parseSelection("-", 20));
        assertThrows(SelectionException.class, () -> cli.parseSelection("1--2", 20));
        assertThrows(SelectionException.class, () -> cli.parseSelection("1-3, 5", 20));
        assertThrows(SelectionException.class, () -> cli.parseSelection("1 , 5", 20));
        assertThrows(SelectionException.class, () -> cli.parseSelection("1 , 5", 20));
    }

    @Test
    void parseInvalidRangeSelections() {
        assertThrows(RangeException.class, () -> cli.parseSelection("0", 21));
        assertThrows(RangeException.class, () -> cli.parseSelection("21", 20));
        assertThrows(RangeException.class, () -> cli.parseSelection("19,22", 21));
        assertThrows(RangeException.class, () -> cli.parseSelection("0-11", 21));
    }
}
