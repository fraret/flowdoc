package edu.upc.prop411.domain;

import edu.upc.prop411.TestRunner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TextUtilsTests {

    @Test
    void testSeparator() {
        String notSeparators = "kjflahfheeajeehaf kaf eefa((ḉ+.,.-";
        for (char c: notSeparators.toCharArray()) {
            assertFalse(TextUtils.isControlLineSeparator(c));
        }
        String separators = "\n\r";
        for (char c: separators.toCharArray()) {
            assertTrue(TextUtils.isControlLineSeparator(c));
        }
    }

    @Test
    void testWord() {
        String notSeparators = "affkalj646\u00FA\u00E8"; // Unicode characters: Latin Small Letter U With Acute, Latin Small Letter E With Grave
        for (char c: notSeparators.toCharArray()) {
            assertTrue(TextUtils.isWordElement(c));
        }
        String separators = " ,.{}";
        for (char c: separators.toCharArray()) {
            assertFalse(TextUtils.isWordElement(c));
        }
    }

    @Test
    void testAllCodepoints() {
        assertTrue(TextUtils.allCodepointsTrue("       ", (c) -> c ==  ' '));
        assertTrue(TextUtils.allCodepointsTrue("", (c) -> false));
        assertFalse(TextUtils.allCodepointsTrue("    d   ", (c) -> c ==  ' '));
    }

    public static void main(String[] args) {
        TestRunner.runClassTests(TextUtilsTests.class);
    }

}
