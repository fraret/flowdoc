package edu.upc.prop411.domain;

import edu.upc.prop411.TestRunner;
import edu.upc.prop411.domain.exceptions.recoverable.DocumentException;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class DocumentTests {

    @Test
    void createValid() throws DocumentException {
        Document doc = new Document("El Quijote", "Miquel de Cervantes", "En un lugar de la Mancha");
        assertEquals("El Quijote", doc.getTitle());
        assertEquals("Miquel de Cervantes", doc.getAuthor());
        assertEquals("En un lugar de la Mancha", doc.getContent());;
    }


    @Test
    void createEmptyTitle() {
        assertThrows(DocumentException.class, () -> {
            Document doc = new Document("", "Miquel de Cervantes", "En un lugar de la Mancha");
        });
    }

    @Test
    void createEmptyAuthor() {
        assertThrows(DocumentException.class, () -> {
            Document doc = new Document("El Quijote", "", "En un lugar de la Mancha");
        });
    }

    @Test
    void createEmptyContent() {
        assertThrows(DocumentException.class, () -> {
            Document doc = new Document("El Quijote", "Miquel de Cervantes", "");
        });
    }

    @Test
    void createNewLineTitle() {
        assertThrows(DocumentException.class, () -> {
            Document doc = new Document("El Quijote\nde la Mancha", "Miquel de Cervantes", "potato");
        });
    }

    @Test
    void createNewLineAuthor() {
        assertThrows(DocumentException.class, () -> {
            Document doc = new Document("El Quijotede la Mancha", "Miquel\n de Cervantes", "potato");
        });
    }

    public static void main(String[] args) {
        TestRunner.runClassTests(DocumentTests.class);
    }

}
