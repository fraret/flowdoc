# Documenter 

This project uses gradle as a build system. The only requirement for using gradle is having a java version between 8 and 18. An internet connection will also be needed for gradle to be able to fetch dependencies as well as possibly itself.

Gradle can be run from this directory using the inlcuded wrappers, `gradlew` or `gradlew.bat`. It has many possible targets, the most important of which we will list below.
All gradle output is placed in a subdirectory of the `build` directory, which will be automatically created. All build artifacts can be cleaned by running

    ./gradlew clean

In this file we assume that the user is using gradle in a UNIX-like (probably linux/macOS) environment and all the command are written accordingly. If you're using Windows, replace all mentions of `./gradlew` with `gradlew.bat`.

## Building
### Main application
If you want the full driver, run

    ./gradlew releaseJar

This will create a jar which driver that bundles all libraries and files and place it in the `release` subdirectory of build.
You can run this jar file anywhere, as it bundles all its needed dependencies.

### Unit tests
If you want to build all the unit tests, run

    ./gradlew unitTests

This will create a folder hierarchy in the `tests` subdirectory of build where each folder has the jar file which can be launched to run all tests for the corresponding class.
Be aware that there is an extra `lib` directory with a jar file that bundles needed libraries (not duplicated in each file), so in this case moving files might break them.

### Javadoc documentation
If you want to generate the Javadoc documentation, run

    ./gradlew javadoc

### Code coverage
Run the tests first with

    ./gradlew test    

Then

    ./gradlew jacocoTestReport

The report will be found in `build/reports/jacoco/test/html/index.html`

## Running
You can run the application directly from gradle using:

    ./gradlew run --console=plain -q

This will automatically recompile the application and launch it, avoiding you the steps needed for manually building
the jar file and running it. Be advised that its performance is significantly slower than using the manual method

## Testing

### Unit Testing
To run all unit tests:

    ./gradlew test

You can also create the jar files and run them as explained above
